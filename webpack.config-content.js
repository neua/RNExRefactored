// const path=require("path");
//
// module.exports = {
//   mode:"development",
//   devtool:"none",
//   entry: "./src/background/main.js",
//   output: {
//     filename: "hello.js",
//     path: path.resolve(__dirname, "CODEE")
//   }
// };
//
const path = require('path');

module.exports = {
  entry: ['./src/content/main.tsx'],
  mode: 'development',
  devtool: 'inline-source-map',
  module: {
    rules: [
      // {
      //    test: /\.(js|jsx)$/,
      //    exclude: /node_modules/,
      //    use: {
      //        loader: 'babel-loader'
      //    },
      // },
      {
           test: /\.css/,
           exclude: /node_modules/,
           use: {
               loader: 'css-loader'
           },
      },
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  output: {
    filename: 'content.js',
    path: path.resolve(__dirname, './dist/content'),
  },
};

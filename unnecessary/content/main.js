let rn_navbar_toggle = true;

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    if (request.message === "clicked_browser_action" || request.message === "no_session" || request.message === "refresh_the_page") {
      if (request.message === "refresh_the_page") {
        main_mode.view.refresh_the_page();
      }
      if (request.message !== "no_session") {
        if (!document.querySelector("#RNEx")) {
          main_mode.view.main();
          main_mode.view.clickListener();
          console.log("inside clicked browser");
        }
      } else if (request.message === "no_session") {
        console.log("no_session");
        if (document.querySelector("#RNEx") && document.querySelector("#RNEx").shadowRoot.querySelector(".rn-outer-main-wrap")) {
          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-outer-main-wrap").className = "rn-login-outer-main-wrap";
          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-login-outer-main-wrap").innerHTML = `
           <div class="rn-login-general-content-box">
            <h4>Login RNEx with a Solid account </h4>
           </div>
           <div class="rn-login-general-content-box">
            <input onclick="window.open('https://inrupt.net/register');" type="button" class="rn-general-searchbox-button-second-type"  value="Register" target="_blank"></input>
            <input id="rn-login-button" type="button" class="rn-general-searchbox-button-second-type" id="login" value="Login"></input>
           </div>
         `;
          document.querySelector("#RNEx").shadowRoot.querySelector("#rn-login-button").addEventListener("click", () => {
            const messageType = "rn_login";
            const messageValues = {
              location: location
            };
            chrome.runtime.sendMessage({
              messageType,
              messageValues
            }, messageResponse => {
              console.log("messageResponse", messageResponse);
              const response = messageResponse[0];
              const error = messageResponse[1];
              if (response !== null) {
                console.log("login response: ", response.body);

              } else {
                console.log("login error: ", error);

              }
            });
          });
        }
      }

    } else if (request.message === "cancel_clicked_browser_action") {
      if (document.querySelector("#RNEx")) {
        document.querySelector("#RNEx").remove();
        console.log("cancel_clicked_browser_action");
      }

    }


  }
);

chrome_storage = {
  addListener: function(func) {
    chrome.storage.onChanged.addListener(func);
  },
  rn_get_the_session_info: async function() {
    let current_mode_promise = new Promise((resolve, reject) => {
      chrome.storage.local.get("rn_session_info", function(val) {
        if (val.rn_session_info === undefined || val.rn_session_info === null) {
          reject(null);
        } else {
          resolve(val.rn_session_info);
        }
      });
    });
    return await current_mode_promise;
  },
  rn_current_mode: function(val) {
    chrome.storage.local.set({
      rn_current_mode: val
    });
  },
  rn_remove_except_the_given_mode: function(given_mode) {

    if (given_mode !== 'rn-modes-profile') {
      chrome.storage.local.remove(['profile_mode'], (e) => {});
    }
    if (given_mode !== 'rn-modes-recommending') {
      chrome.storage.local.remove(['recommending_mode'], (e) => {});
    }
    if (given_mode !== 'rn-modes-response') {
      chrome.storage.local.remove(['rn-modes-response'], (e) => {});
    }
    if (given_mode !== 'rn-modes-notification') {
      chrome.storage.local.remove(['notification_mode'], (e) => {});
    }
    if (given_mode !== 'rn-modes-account') {
      chrome.storage.local.remove(['account_mode'], (e) => {});
    }
    if (given_mode !== 'rn-modes-reading') {
      chrome.storage.local.remove(['reading_mode'], (e) => {});
    }
    if (given_mode !== 'rn-modes-writing') {
    }

  },
  rn_navbar_toggle_clicked_and_open: function(val) {
    chrome.storage.local.set({
      rn_navbar_toggle_clicked_and_open: val
    });
  },
  rn_general_store_the_single_renarration_to_apply_the_page: async function(success) {
    let current_mode_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get(["rn_general_apply_the_renarration"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            rn_general_apply_the_renarration: {
              renarration_to_apply: success
            }
          }, () => {
            resolve(success);
          });
        } else {
          let createdNotificationObject = value.rn_general_apply_the_renarration;
          createdNotificationObject.renarration_to_apply = success;
          chrome.storage.local.set({
            rn_general_apply_the_renarration: createdNotificationObject
          }, () => {
            resolve(success);
          });
        }
      });

    });
    return await current_mode_promise;
  },
  rn_general_store_the_single_renarration_store_the_applied_renarration_on_the_page: async function(success) {
    let current_mode_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get(["rn_general_applied_renarration"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            rn_general_applied_renarration: {
              applied_renarration: success
            }
          }, () => {
            resolve(success);
          });
        } else {
          let createdNotificationObject = value.rn_general_applied_renarration;
          createdNotificationObject.applied_renarration = success;
          chrome.storage.local.set({
            rn_general_applied_renarration: createdNotificationObject
          }, () => {
            resolve(success);
          });
        }
      });

    });
    return await current_mode_promise;
  },
  rn_get_the_general_store_the_single_renarration_store_the_applied_renarration_on_the_page: async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("rn_general_applied_renarration", function(val) {
        if (val.rn_general_applied_renarration.applied_renarration) {
          resolve(val.rn_general_applied_renarration.applied_renarration);
        } else {
          reject(null);
        }
      });

    });

    return await a_promise;
  },
  rn_get_the_general_store_single_renarration_to_apply_the_page_values: async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("rn_general_apply_the_renarration", function(val) {
        if (val.rn_general_apply_the_renarration) {
          resolve(val.rn_general_apply_the_renarration);
        } else {
          reject(null);
        }
      });

    });

    return await a_promise;
  },
  rn_account_mode_clicked_row_before_the_go_button: async function(val) {
    let a_promise = new Promise((resolve, reject) => {
      chrome.storage.local.get(["account_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            account_mode: {
              clicked_row_before_the_go_button: val
            }
          }, function() {
            resolve("nice1 rn_account_mode_clicked_row_before_the_go_button");

          });
        } else {
          let createdRenarrationObject = value.account_mode;
          createdRenarrationObject.clicked_row_before_the_go_button = val;

          chrome.storage.local.set({
            account_mode: createdRenarrationObject
          }, function() {
            resolve("nice2 rn_account_mode_clicked_row_before_the_go_button");

          });
        }
      });

    });


    return await a_promise;
  },
  rn_account_mode_clicked_row_unique_id_before_the_go_button: async function(val) {
    let a_promise = new Promise((resolve, reject) => {
      chrome.storage.local.get(["account_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            account_mode: {
              clicked_row_unique_id_before_the_go_button: val
            }
          }, function() {

            resolve("nice1 rn_account_mode_clicked_row_unique_id_before_the_go_button");

          });
        } else {
          let createdRenarrationObject = value.account_mode;
          createdRenarrationObject.clicked_row_unique_id_before_the_go_button = val;

          chrome.storage.local.set({
            account_mode: createdRenarrationObject
          }, function() {
            resolve("nice2 rn_account_mode_clicked_row_unique_id_before_the_go_button");

          });
        }
      });
    });
    return await a_promise;
  },
  rn_reading_mode_clicked_row_before_the_go_button: async function(val) {
    let a_promise = new Promise((resolve, reject) => {
      chrome.storage.local.get(["reading_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            reading_mode: {
              clicked_row_before_the_go_button: val
            }
          }, function() {
            resolve("nice1 rn_reading_mode_clicked_row_before_the_go_button");

          });
        } else {
          let createdRenarrationObject = value.reading_mode;
          createdRenarrationObject.clicked_row_before_the_go_button = val;

          chrome.storage.local.set({
            reading_mode: createdRenarrationObject
          }, function() {
            resolve("nice2 rn_reading_mode_clicked_row_before_the_go_button");

          });
        }
      });

    });


    return await a_promise;
  },
  rn_reading_mode_clicked_row_unique_id_before_the_go_button: async function(val) {
    let a_promise = new Promise((resolve, reject) => {
      chrome.storage.local.get(["reading_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            reading_mode: {
              clicked_row_unique_id_before_the_go_button: val
            }
          }, function() {
            //    console.log('Value is set to ' + reading_mode);
            resolve("nice1 rn_reading_mode_clicked_row_unique_id_before_the_go_button");

          });
        } else {
          let createdRenarrationObject = value.reading_mode;
          createdRenarrationObject.clicked_row_unique_id_before_the_go_button = val;

          chrome.storage.local.set({
            reading_mode: createdRenarrationObject
          }, function() {
            //  console.log('Value is set to ' + reading_mode);
            resolve("nice2 rn_reading_mode_clicked_row_unique_id_before_the_go_button");

          });
        }
      });
    });
    return await a_promise;
  },
  rn_profile_mode_clicked_row_unique_id_before_the_go_button: async function(val) {
    let a_promise = new Promise((resolve, reject) => {
      chrome.storage.local.get(["profile_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            profile_mode: {
              clicked_row_unique_id_before_the_go_button: val
            }
          }, function() {
            //    console.log('Value is set to ' + profile_mode);
            resolve("nice1 rn_profile_mode_clicked_row_unique_id_before_the_go_button");

          });
        } else {
          let createdRenarrationObject = value.profile_mode;
          createdRenarrationObject.clicked_row_unique_id_before_the_go_button = val;

          chrome.storage.local.set({
            profile_mode: createdRenarrationObject
          }, function() {
            //  console.log('Value is set to ' + profile_mode);
            resolve("nice2 rn_profile_mode_clicked_row_unique_id_before_the_go_button");

          });
        }
      });
    });
    return await a_promise;
  },
  rn_profile_mode_clicked_row_before_the_go_button: async function(val) {
    let a_promise = new Promise((resolve, reject) => {
      chrome.storage.local.get(["profile_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            profile_mode: {
              clicked_row_before_the_go_button: val
            }
          }, function() {
            //  console.log('Value is set to ' + profile_mode);
            resolve("nice1 rn_profile_mode_clicked_row_before_the_go_button");

          });
        } else {
          let createdRenarrationObject = value.profile_mode;
          createdRenarrationObject.clicked_row_before_the_go_button = val;

          chrome.storage.local.set({
            profile_mode: createdRenarrationObject
          }, function() {
            //  console.log('Value is set to ' + profile_mode);
            resolve("nice2 rn_profile_mode_clicked_row_before_the_go_button");

          });

        }
      });

    });

    //    chrome_storage.rn_response_mode_renarration_value(val);

    return await a_promise;
  },
  rn_response_mode_comment_text: async function(val) {
    let a_promise = new Promise((resolve, reject) => {
      chrome.storage.local.get(["response_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            response_mode: {
              comment_text: val
            }
          }, function() {
            //  console.log('Value is set to ' + response_mode);
            resolve("nice1 rn_response_mode_comment_text");

          });
        } else {
          let createdRenarrationObject = value.response_mode;
          createdRenarrationObject.comment_text = val;

          chrome.storage.local.set({
            response_mode: createdRenarrationObject
          }, function() {
            //  console.log('Value is set to ' + response_mode);
            resolve("nice2 rn_response_mode_comment_text");

          });
        }
      });

    });
    return await a_promise;

  },
  rn_response_mode_rate_value: async function(val) {
    let a_promise = new Promise((resolve, reject) => {
      chrome.storage.local.get(["response_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            response_mode: {
              rate_value: val
            }
          }, function() {
            //  console.log('Value is set to ' + response_mode);
            resolve("nice1 rn_response_mode_comment_text");

          });
        } else {
          let createdRenarrationObject = value.response_mode;
          createdRenarrationObject.rate_value = val;

          chrome.storage.local.set({
            response_mode: createdRenarrationObject
          }, function() {
            //  console.log('Value is set to ' + response_mode);
            resolve("nice2 rn_response_mode_comment_text");

          });
        }
      });

    });
    return await a_promise;

  },
  rn_response_mode_renarration_value: async function(val) {
    let a_promise = new Promise((resolve, reject) => {
      chrome.storage.local.get(["response_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            response_mode: {
              renarration_value: val
            }
          }, function() {
            //  console.log('Value is set to ' + response_mode);
            resolve("nice1 rn_response_mode_comment_text");

          });
        } else {
          let createdRenarrationObject = value.response_mode;
          createdRenarrationObject.renarration_value = val;

          chrome.storage.local.set({
            response_mode: createdRenarrationObject
          }, function() {
            //  console.log('Value is set to ' + response_mode);
            resolve("nice2 rn_response_mode_comment_text");

          });
        }
      });

    });
    return await a_promise;

  },
  rn_get_the_response_mode_renarration_value: async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("response_mode", function(val) {
        if (val.response_mode) {
          if (val.response_mode.renarration_value === undefined || val.response_mode.renarration_value === null) {
            reject(null);
          } else {
            resolve(val.response_mode.renarration_value);
          }
        } else {
          console.log("chrome.storage.local.get response_mode doesn't exist");
          reject("chrome.storage.local.get response_mode doesn't exist");
        }
      });

    });

    return await a_promise;
  },
  rn_get_the_response_mode_response_documents_array: async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("response_mode", function(val) {
        if (val.response_mode === undefined || val.response_mode === null) {
          reject(null);
        } else {
          resolve(val.response_mode.response_documents_array);
        }
      });

    });

    return await a_promise;
  },
  rn_get_the_account_mode_clicked_row_unique_id_before_the_go_button: async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("account_mode", function(val) {
        if (val.account_mode) {
          if (val.account_mode.clicked_row_unique_id_before_the_go_button === undefined || val.account_mode.clicked_row_unique_id_before_the_go_button === null) {
            reject(null);
          } else {
            resolve(val.account_mode.clicked_row_unique_id_before_the_go_button);
          }
        } else {
          console.log("chrome.storage.local.get account_mode doesn't exist");
          reject("chrome.storage.local.get account_mode doesn't exist");
        }
      });

    });

    return await a_promise;
  },
  rn_get_the_reading_mode_clicked_row_unique_id_before_the_go_button: async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("reading_mode", function(val) {
        if (val.reading_mode) {
          if (val.reading_mode.clicked_row_unique_id_before_the_go_button === undefined || val.reading_mode.clicked_row_unique_id_before_the_go_button === null) {
            reject(null);
          } else {
            resolve(val.reading_mode.clicked_row_unique_id_before_the_go_button);
          }
        } else {
          console.log("chrome.storage.local.get reading_mode doesn't exist");
          reject("chrome.storage.local.get reading_mode doesn't exist");
        }
      });

    });

    return await a_promise;
  },
  rn_get_the_profile_mode_clicked_row_unique_id_before_the_go_button: async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("profile_mode", function(val) {
        if (val.profile_mode) {
          if (val.profile_mode.clicked_row_unique_id_before_the_go_button === undefined || val.profile_mode.clicked_row_unique_id_before_the_go_button === null) {
            reject(null);
          } else {
            resolve(val.profile_mode.clicked_row_unique_id_before_the_go_button);
          }
        } else {
          console.log("chrome.storage.local.get profile_mode doesn't exist");
          reject("chrome.storage.local.get profile_mode doesn't exist");
        }
      });

    });

    return await a_promise;
  },
  rn_get_the_account_mode_clicked_row_renarrations_value_before_the_go_button: async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("account_mode", function(val) {
        if (val.account_mode.clicked_row_before_the_go_button === undefined || val.account_mode.clicked_row_before_the_go_button === null) {
          reject(null);
        } else {
          resolve(val.account_mode.clicked_row_before_the_go_button);
        }
      });

    });

    return await a_promise;
  },
  rn_get_the_reading_mode_clicked_row_renarrations_value_before_the_go_button: async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("reading_mode", function(val) {
        if (val.reading_mode.clicked_row_before_the_go_button === undefined || val.reading_mode.clicked_row_before_the_go_button === null) {
          reject(null);
        } else {
          resolve(val.reading_mode.clicked_row_before_the_go_button);
        }
      });

    });

    return await a_promise;
  },
  rn_get_the_profile_mode_clicked_row_renarrations_value_before_the_go_button: async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("profile_mode", function(val) {
        if (val.profile_mode.clicked_row_before_the_go_button === undefined || val.profile_mode.clicked_row_before_the_go_button === null) {
          reject(null);
        } else {
          resolve(val.profile_mode.clicked_row_before_the_go_button);
        }
      });

    });

    return await a_promise;
  },
  rn_get_the_current_mode: async function() {
    let current_mode_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("rn_current_mode", function(val) {

        if (val.rn_current_mode === undefined || val.rn_current_mode === null) {
          reject(null);
        } else {
          resolve(val.rn_current_mode);
        }
      });

    });

    return await current_mode_promise;
  },
  rn_get_the_navbar_toggle_clicked_and_open: async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("rn_navbar_toggle_clicked_and_open", function(val) {

        if (val.rn_navbar_toggle_clicked_and_open === undefined || val.rn_navbar_toggle_clicked_and_open === null) {
          reject(null);
        } else {
          resolve(val.rn_navbar_toggle_clicked_and_open);
        }
      });

    });

    return await a_promise;
  },
  rn_get_the_account_mode_renarrations_and_the_table: async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("account_mode", function(val) {
        if (val.account_mode) {
          if (val.account_mode.renarrations_and_the_table === undefined || val.account_mode.renarrations_and_the_table === null) {
            reject(null);
          } else {
            resolve(val.account_mode.renarrations_and_the_table);
          }
        } else {
          console.log("chrome.storage.local.get account_mode doesn't exist");
          reject("chrome.storage.local.get account_mode doesn't exist");
        }
      });

    });

    return await a_promise;
  },
  rn_get_the_reading_mode_renarrations_and_the_table: async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("reading_mode", function(val) {
        if (val.reading_mode) {
          if (val.reading_mode.renarrations_and_the_table === undefined || val.reading_mode.renarrations_and_the_table === null) {
            reject(null);
          } else {
            resolve(val.reading_mode.renarrations_and_the_table);
          }
        } else {
          console.log("chrome.storage.local.get reading_mode doesn't exist");
          reject("chrome.storage.local.get reading_mode doesn't exist");
        }
      });

    });

    return await a_promise;
  },
  rn_get_the_profile_mode_renarrations_and_the_table: async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("profile_mode", function(val) {
        if (val.profile_mode) {
          if (val.profile_mode.renarrations_and_the_table === undefined || val.profile_mode.renarrations_and_the_table === null) {
            reject(null);
          } else {
            resolve(val.profile_mode.renarrations_and_the_table);
          }
        } else {
          console.log("chrome.storage.local.get profile_mode doesn't exist");
          reject("chrome.storage.local.get profile_mode doesn't exist");
        }
      });

    });

    return await a_promise;
  },
  rn_get_the_reading_mode_renarration_documents_array: async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("reading_mode", function(val) {
        if (val.reading_mode === undefined || val.reading_mode === null) {
          reject(null);
        } else {
          resolve(val.reading_mode.renarration_documents_array);
        }
      });

    });

    return await a_promise;
  },
  rn_get_the_profile_mode_renarration_documents_array: async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("profile_mode", function(val) {
        if (val.profile_mode === undefined || val.profile_mode === null) {
          reject(null);
        } else {
          resolve(val.profile_mode.renarration_documents_array);
        }
      });

    });

    return await a_promise;
  },
  rn_get_the_current_session_renarrators_info_object: async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("rn_session_renarrators_info", function(val) {
        if (val.rn_session_renarrators_info) {
          resolve(val.rn_session_renarrators_info);
        } else {
          reject(null);
        }
      });

    });

    return await a_promise;
  },
  rn_get_the_profile_mode_renarrators_info_object: async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("profile_mode", function(val) {
        if (val.profile_mode === undefined || val.profile_mode === null) {
          reject(null);
        } else {
          resolve(val.profile_mode.renarrators_profile_info);
        }
      });

    });

    return await a_promise;
  },
  rn_get_the_notification_mode_values: async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("notification_mode", function(val) {
        if (val.notification_mode) {
          resolve(val.notification_mode);
        } else {
          reject("notification_mode doesn't exist");
        }
      });

    });

    return await a_promise;
  },
  rn_get_the_recommending_mode_values: async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("recommending_mode", function(val) {
        if (val.recommending_mode) {
          resolve(val.recommending_mode);
        } else {
          reject("recommending_mode doesn't exist");
        }
      });

    });

    return await a_promise;
  },
  rn_get_the_extension_current_tab_url: async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("rn_extension_current_tab_url", function(val) {
        if (val.rn_extension_current_tab_url === undefined || val.rn_extension_current_tab_url === null) {
          reject(null);
        } else {
          resolve(val.rn_extension_current_tab_url);
        }
      });

    });

    return await a_promise;
  },
  rn_set_recommending_mode_chosen_renarration: async function(success) {
    let current_mode_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get(["recommending_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            recommending_mode: {
              chosen_renarration: success
            }
          }, (success) => {
            resolve(success);
          });
        } else {
          let createdRenarrationObject = value.recommending_mode;
          createdRenarrationObject.chosen_renarration = success;

          chrome.storage.local.set({
            recommending_mode: createdRenarrationObject
          }, (success) => {
            resolve(success);
          });

        }
      });

    });
    return await current_mode_promise;
  },
  rn_get_external_api_analytics_for_the_applied_renarration_on_the_page: async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("rn_external_analytics_result_for_applied_renarration", function(val) {
        if (val.rn_external_analytics_result_for_applied_renarration) {
          resolve(val.rn_external_analytics_result_for_applied_renarration);
        } else {
          reject("rn_external_analytics_result_for_applied_renarration doesn't exist");
        }
      });

    });

    return await a_promise;
  }
};

main_mode = {
  variables: {
    renarration_information: {
      profile_card: null,
      webId: null,
      webId_label: null,
      idp: null,
      role: null,
      email: null,
      name: null,
      picture: null,
      friends: []
    },
    renarration_array_from_the_pod: null,
    reading_mode: {}
  },
  view: {
    main: function() {

      let menu_toggle_icon = chrome.runtime.getURL("img/OwnedIcons/menuicon.svg");
      //  let profile_jpg = chrome.runtime.getURL("img/profile.jpg");
      let profile_mode_icon = chrome.runtime.getURL("img/OwnedIcons/otherprofile.svg");
      let recommending_mode_icon = chrome.runtime.getURL("img/OwnedIcons/search.svg");
      let notification_mode_icon = chrome.runtime.getURL("img/OwnedIcons/notification.svg");
      let searching_mode_icon = chrome.runtime.getURL("img/OwnedIcons/search.svg");
      let account_mode_icon = chrome.runtime.getURL("img/OwnedIcons/myprofile.svg");
      let reading_mode_icon = chrome.runtime.getURL("img/OwnedIcons/readingbook.svg");
      let writing_mode_icon = chrome.runtime.getURL("img/OwnedIcons/pencil.svg");
      //  let response_mode_icon = chrome.runtime.getURL("img/rn-icons/part2/pencil.svg");
      let information_box_icon = chrome.runtime.getURL("img/OwnedIcons/information.svg");
      let response_mode_icon = chrome.runtime.getURL("img/OwnedIcons/response.svg");

      let rn_style_css = document.createElement('link');
      rn_style_css.setAttribute("rel", "stylesheet");
      rn_style_css.setAttribute("href", chrome.runtime.getURL('rn-design.css'));

      let shadow = document.createElement('div');
      shadow.setAttribute("id", "RNEx");
      document.body.appendChild(shadow);
      shadow.attachShadow({
        mode: "open"
      });

      let rn_outer_main_wrap = document.createElement("div");
      rn_outer_main_wrap.setAttribute("class", "rn-outer-main-wrap");

      let rn_whole_window_wrap = document.createElement("div");
      rn_whole_window_wrap.setAttribute("id", "rn_whole_window_wrap");

      let rn_navbar = document.createElement("div");
      rn_navbar.setAttribute("class", "rn-navbar");
      rn_navbar.innerHTML = `<input type="image" src=${menu_toggle_icon} class="rn-navbar-toggle"></input>
        <!-- <input type="image" src=${profile_mode_icon} class="rn-modes-profile"></input>-->
        <input type="image" src=${notification_mode_icon} class="rn-modes-notification"></input>
        <!-- <i id="rn-response-mode-icon" class="rn-modes-response"></i>-->
        <input type="image" src=${account_mode_icon} class="rn-modes-account"></input>
        <input type="image" src=${reading_mode_icon} class="rn-modes-reading"></input>
        <input type="image" src=${writing_mode_icon} class="rn-modes-writing"></input>
        <input type="image" src=${recommending_mode_icon} class="rn-modes-recommending"></input>`;

      let rn_navbar_vertical = document.createElement("div");
      rn_navbar_vertical.setAttribute("class", "rn-navbar-vertical");
      rn_navbar_vertical.innerHTML = `<input type="image" src=${information_box_icon} class="rn-information-of-current-renarration">
      <input type="image" src=${profile_mode_icon} class="rn-modes-profile"></input>
      <input type="image" src=${response_mode_icon} class="rn-modes-response"></input>`;
      let rn_main_wrap = document.createElement("div");
      rn_main_wrap.setAttribute("class", "rn-main-wrap");

      chrome_storage.rn_get_the_navbar_toggle_clicked_and_open().then(success => {
        main_mode.controller.creating_the_inside_of_main_mode_depending_on_the_chrome_storage(success, "rn-navbar-toggle");
        //  rn_main_wrap.appendChild(writing_mode.view.main());
      }).catch(e => {
        //rn_main_wrap.appendChild(reading_mode.view.main());

      });

      chrome_storage.rn_get_the_current_mode().then(s => {
        main_mode.controller.creating_the_inside_of_main_mode_depending_on_the_chrome_storage(null, s);
        //  rn_main_wrap.appendChild(writing_mode.view.main());
      }).catch(e => {
        main_mode.controller.creating_the_inside_of_main_mode_depending_on_the_chrome_storage(null, "rn-modes-account");
        //  rn_main_wrap.appendChild(account_mode.view.main());

      });

      rn_outer_main_wrap.appendChild(rn_navbar);
      rn_outer_main_wrap.appendChild(rn_navbar_vertical);
      rn_outer_main_wrap.appendChild(rn_main_wrap);

      shadow.shadowRoot.appendChild(rn_style_css);
      shadow.shadowRoot.appendChild(rn_whole_window_wrap);
      shadow.shadowRoot.appendChild(rn_outer_main_wrap);

    },
    clickListener: function() {
      document.querySelector("#RNEx").shadowRoot.querySelector(".rn-navbar").addEventListener("click", e => {
        console.log("event:", e.target);
        let chosen_mode_class_name = e.target.className;



        if (chosen_mode_class_name !== "rn-navbar-toggle" && chosen_mode_class_name !== "rn-navbar") {

          main_mode.view.remove_the_selection_sign_from_menu(e.target);

        }

        main_mode.controller.creating_the_inside_of_main_mode_depending_on_the_chrome_storage(!rn_navbar_toggle, e.target.className);
      });
      document.querySelector("#RNEx").shadowRoot.querySelector(".rn-navbar-vertical").addEventListener("click", e => {
        console.log(".rn-navbar-vertical event:", e.target);
        main_mode.controller.rn_navbar_vertical.main_operation(e.target);

      });


    },
    remove: function() {
      if (document.querySelector("#RNEx") !== null) {
        let RNEx = document.querySelector("#RNEx");
        RNEx.remove();
      }
    },
    remove_inside_rn_main_wrap: function() {
      if (document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-wrap").firstChild !== null) {
        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-wrap").firstChild.remove();
      }
    },
    remove_body_listener: function() {
      if (writing_mode.view.variables.current_chosen_target_html_node !== null) {
        writing_mode.view.variables.current_chosen_target_html_node.removeAttribute("rn-class", "rn-chosen-html-node");
      }

      window.removeEventListener('beforeunload', writing_mode.view.variables.window_beforeunload_func);

      if (document.querySelector("[rnex_body=rn_writing_mode]") !== null) {
        document.querySelector("[rnex_body=rn_writing_mode]").removeEventListener('click',
          writing_mode.controller.writing_mode_body_remove_and_choose
        );

        if (writing_mode.view.variables.hovered_target_html_node) {
          writing_mode.view.variables.hovered_target_html_node.removeAttribute("rn-hovered-class", "rn-hovered-html-node");
        }
        document.querySelector("[rnex_body=rn_writing_mode]").removeEventListener('mouseover',
          writing_mode.controller.mouseover_operations
        );
      }
    },
    choose_inside_rn_main_wrap: function(rn_mode_selection_addition) {
      if (rn_mode_selection_addition !== null) {
        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-wrap").appendChild(rn_mode_selection_addition);
      }
    },
    refresh_the_page: function() {
      location.reload();
    },
    remove_the_selection_sign_from_menu: function(eventTarget) {
      if (eventTarget.className === "rn-information-of-current-renarration") {
        let information_sign = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-navbar-vertical .rn-information-of-current-renarration");
        if (information_sign.hasAttribute("rn-navbar-mode-chosen")) {
          information_sign.removeAttribute("rn-navbar-mode-chosen", 'rn-navbar-mode-selection');
        } else {
          information_sign.setAttribute("rn-navbar-mode-chosen", 'rn-navbar-mode-selection');
        }
      } else {
        let horizontal_menu = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-navbar");
        if (horizontal_menu) {
          let children = Array.from(horizontal_menu.children);
          children.forEach(child => {
            child.removeAttribute("rn-navbar-mode-chosen", 'rn-navbar-mode-selection');
          });
        }
        let vertical_menu = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-navbar-vertical");
        if (vertical_menu) {
          let children = Array.from(vertical_menu.children);
          children.forEach(child => {
            if (child.className !== "rn-information-of-current-renarration") {
              child.removeAttribute("rn-navbar-mode-chosen", 'rn-navbar-mode-selection');
            }
          });
        }
        eventTarget.setAttribute("rn-navbar-mode-chosen", "rn-navbar-mode-selection");
      }
    },
    popup: {
      main: function(messageType, messageText = "") {

        if (messageType === "success") {
          main_mode.view.popup.success(messageText);
          setTimeout(main_mode.view.popup.remove, 2000);
        }
        else if (messageType === "error") {
          main_mode.view.popup.error(messageText);
          setTimeout(main_mode.view.popup.remove, 2000);
        }
      },
      success: function(text) {

        let succesMessage = `<div class="rn-alert-box">
          <h2 class="rn-alert-box-text">Success</h2>
          <p class="rn-alert-box-text">${text}</p>
          <input type="button" class="rn-alert-box-button" value="OK" onclick="parentNode.parentNode.removeChild(parentNode)">
          </div>`;

        document.querySelector("#RNEx").shadowRoot.querySelector("#rn_whole_window_wrap").innerHTML = succesMessage;

      },
      error: function(text) {

        let succesMessage = `<div class="rn-alert-box">
          <h2 class="rn-alert-box-text">Error</h2>
          <p class="rn-alert-box-text">${text}</p>
          <input type="button" class="rn-alert-box-button" value="OK" onclick="parentNode.parentNode.removeChild(parentNode)">
          </div>`;

        document.querySelector("#RNEx").shadowRoot.querySelector("#rn_whole_window_wrap").innerHTML = succesMessage;

      },
      remove: function() {
        document.querySelector("#RNEx").shadowRoot.querySelector("#rn_whole_window_wrap").innerHTML = "";
      }
    }
  },
  model: {
    main: function() {},
    login: function() {
      const messageType = "rn_login";
      const messageValues = {
        location: location
      };
      chrome.runtime.sendMessage({
        messageType,
        messageValues
      }, messageResponse => {
        const response = messageResponse[0];
        const error = messageResponse[1];
        if (response !== null) {
          console.log("login response: ", response.body);

        } else {
          console.log("login error: ", error);

        }
      });
    },
    logout: function() {
      const messageType = "rn_logout";
      const messageValues = {
        location: location
      };
      chrome.runtime.sendMessage({
        messageType,
        messageValues
      }, messageResponse => {
        const response = messageResponse[0];
        const error = messageResponse[1];
        if (response !== null) {
          console.log("logout response: ", response.body);

        } else {
          console.log("logout error: ", error);

        }
      });
    },
    track_session: function() {
      const messageType = "rn_tracksession";
      const messageValues = {
        location: location
      };
      chrome.runtime.sendMessage({
        messageType,
        messageValues
      }, messageResponse => {
        const response = messageResponse[0];
        const error = messageResponse[1];
        if (response !== null) {
          console.log("track_session response: ", response.body);

          main_mode.variables.renarration_information.profile_card = response.body.webId;
          return response.body;
        } else {
          console.log("track_session error: ", error);
          return error;
        }
      });
    },
    get_the_profile_info_with_track_session: function() {
      const messageType = "rn_tracksession";
      const messageValues = {
        location: location
      };
      chrome.runtime.sendMessage({
        messageType,
        messageValues
      }, messageResponse => {
        const response = messageResponse[0];
        const error = messageResponse[1];
        if (response !== null) {
          console.log("track_session response: ", response.body);

          main_mode.variables.renarration_information.idp = response.body.idp;

          main_mode.variables.renarration_information.profile_card = response.body.webId;

          return response.body;
        } else {
          console.log("rn_tracksession error: ", error);
          return error;
        }
      });
    },
    get_the_renarrations_from_the_renarrators_pod: function(table_action = {
      action: null,
      go_page: 1,
      current_page: 1
    }) {
      const messageType = "rn_get_the_renarrations_from_the_renarrators_pod";
      chrome.runtime.sendMessage({
        messageType,
        messageValues: table_action
      }, messageResponse => {
        let response;
        let error;
        if (messageResponse) {
          response = messageResponse[0];
          error = messageResponse[1];
        }

        if (response) {
          console.log("get_renarrations_from_the_renarrators_pod response: ", response.renarrationArray);
          main_mode.variables.renarration_array_from_the_pod = response.renarrationArray;
          return response.body;
        } else {
          console.log("rn_get_the_renarrations_from_the_renarrators_pod error: ", error);
          return error;
        }
      });
    },
    get_the_renarrations_from_the_rnex_then_from_the_renarrators_pod: function(desiredOnSourceDocument = undefined, table_action = {
      action: null,
      go_page: 1,
      current_page: 1
    }, desiredRenarratedBy = undefined, currentMode = undefined) {
      let desired_renarrations = {
        desiredOnSourceDocument: desiredOnSourceDocument,
        tableAction: table_action,
        desiredRenarratedBy: desiredRenarratedBy,
        currentMode: currentMode
      };
      const messageType = "rn_get_the_renarrations_from_the_rnex_then_from_the_renarrators_pod";
      const messageValues = desired_renarrations;
      chrome.runtime.sendMessage({
        messageType,
        messageValues
      }, messageResponse => {
        const response = messageResponse[0];
        const error = messageResponse[1];
        if (response !== null) {
          console.log("rn_get_the_renarrations_from_the_rnex_then_from_the_renarrators_pod response: ", response.renarrationArray);

          main_mode.variables.reading_mode.renarration_array_from_the_pod = response.renarrationArray;

          return response.body;
        } else {
          // console.log("track_session error: ", error);
          return error;
        }
      });
    },
    get_the_renarrations_from_the_other_renarrators_pod: function(renarration_unique_id_with_pod_location) {
      let desired_number_and_renarrationUniqueIdWithPodLocation = {
        renarration_unique_id_with_pod_location: renarration_unique_id_with_pod_location
      };
      const messageType = "rn_get_the_renarrations_from_the_other_renarrators_pod";
      const messageValues = desired_number_and_renarrationUniqueIdWithPodLocation;
      chrome.runtime.sendMessage({
        messageType,
        messageValues
      }, messageResponse => {
        const response = messageResponse[0];
        const error = messageResponse[1];
        if (response !== null) {
          console.log("rn_get_the_renarrations_from_the_other_renarrators_pod response: ", response.renarrationArray);
          return response.body;
        } else {
          // console.log("track_session error: ", error);
          return error;
        }
      });
    },
    get_the_part_of_webId: function(renarrator_session) {

      let renarrator = renarrator_session;

      let regex = /(?<=\:\/\/)(.*?)(?=\.)/g;
      renarrator_edited_firstpart = renarrator.match(regex);

      console.log("first", renarrator_edited_firstpart);
      console.log(renarrator_edited_firstpart[0]);

      return renarrator_edited_firstpart[0];
    },
    create_the_webId_via_concatenation: function(webId_label, idp, profile_card) {
      let regex = /(?<=\:\/\/)(.*)/g;

      let idp_removal_of_https = idp.match(regex);


      let regex2 = /^.*(?=:\/\/)/g;
      let first_http_or_https_part = profile_card.match(regex2);

      let webId = first_http_or_https_part + "://" + webId_label + "." + idp_removal_of_https;

      return webId;
    },
    get_the_notifications_from_the_rnex_pod: function() {
      const messageType = "rn_get_the_notifications_from_the_rnex_pod";
      chrome.runtime.sendMessage({
        messageType
      }, messageResponse => {
        const response = messageResponse[0];
        const error = messageResponse[1];
        if (response !== null) {
          console.log("rn_get_the_notifications_from_the_rnex_pod response: ", response.renarrationArray);
          return response;
        } else {
          return error;
        }
      });
    },
    get_the_recommendations_from_the_rnex_pod: function() {
      const messageType = "rn_get_the_recommendations_from_the_rnex_pod";
      chrome.runtime.sendMessage({
        messageType
      });

    },
    get_the_search_result_from_the_rnex_pod: function() {
      const messageType = "rn_get_the_search_result_from_the_rnex_pod";
      chrome.runtime.sendMessage({
        messageType
      }, messageResponse => {
        const response = messageResponse[0];
        const error = messageResponse[1];
        if (response !== null) {
          console.log("rn_get_the_search_result_from_the_rnex_pod response: ", response.renarrationArray);
          return response;
        } else {

          return error;
        }
      });
    },
    get_the_searched_renarration_result_from_the_rnex_pod: function(operation_type, text_value) {
      const messageType = "rn_get_the_searched_renarration_result_from_the_rnex_pod";
      const messageValues = {
        operation_type: operation_type,
        text_value: text_value
      };
      chrome.runtime.sendMessage({
        messageType,
        messageValues
      });

    },
    create: function() {},
    read: function(location) {
      const messageType = "rn_read";
      const messageValues = {
        location: location
      };
      chrome.runtime.sendMessage({
        messageType,
        messageValues
      }, messageResponse => {
        const response = messageResponse[0];
        const error = messageResponse[1];
        if (response !== null) {
          console.log("response: ", response.body);
          return response.body;

        } else {
          console.log("error: ", error);
          return error;
        }
      });
    },
    update: function(location, additions, deletions) {
      const messageType = "rn_update";
      const messageValues = {
        location: location,
        additions: additions,
        deletions: deletions
      };
      chrome.runtime.sendMessage({
        messageType,
        location,
        additions,
        deletions
      }, messageResponse => {
        const response = messageResponse[0];
        const error = messageResponse[1];
        if (response !== null) {
          console.log("response: ", response.body);
          return response.body;

        } else {
          console.log("error: ", error);
          return error;
        }
      });

    },
    update_renarration_to_renarrators_and_rnex_pod: function(renarration_unique_id, renarration_information_send_object, renarration_transformation_send_object) {
      let renarration_values = {};
      renarration_values.renarration_unique_id = renarration_unique_id;
      renarration_values.renarration_information = renarration_information_send_object;
      renarration_values.renarration_transformation = renarration_transformation_send_object;


      chrome.storage.local.get("pod_information", function(pod_information) {
        if (typeof(pod_information.renarration_values_array) !== 'undefined' && pod_information.renarration_values_array instanceof Array) {
          pod_information.renarration_values_array.push(renarration_values);
        } else {
          pod_information.renarration_values_array = [renarration_values];
        }
        chrome.storage.local.set(pod_information);
      });


      const messageType = "rn_update_renarration_to_renarrators_and_rnex_pod";
      const messageValues = {
        renarration_unique_id: renarration_unique_id,
        renarration_information_send_object: renarration_information_send_object,
        renarration_transformation_send_object: renarration_transformation_send_object
      };
      chrome.runtime.sendMessage({
        messageType,
        renarration_unique_id,
        renarration_information_send_object,
        renarration_transformation_send_object
      }, messageResponse => {
        const response = messageResponse[0];
        const error = messageResponse[1];
        if (response !== null) {
          console.log("response: ", response.body);

          main_mode.view.popup.main("success", "Renarration is created!");
          return response.body;

        } else {
          console.log("error: ", error);
          return error;
        }
      });

    },
    delete_the_renarration: function(url_of_renarration_for_deletion) {
      const messageType = "rn_delete_the_renarration";
      chrome.runtime.sendMessage({
        messageType,
        messageValues: {
          url_of_renarration_for_deletion: url_of_renarration_for_deletion
        }
      }, messageResponse => {
        let response;
        let error;
        if (messageResponse) {
          response = messageResponse[0];
          error = messageResponse[1];
        }

        if (response) {
          console.log("rn_delete_the_renarration-response: ", response);
        } else {
          console.log("rn_delete_the_renarration-error: ", error);
          return error;
        }
      });
    },
    get_the_renarration_from_the_url: function(renarrationUniqueUrl) {
      const messageType = "rn_get_the_renarration_from_the_url";
      let messageValues = {
        renarrationUniqueUrl: renarrationUniqueUrl
      };
      chrome.runtime.sendMessage({
        messageType,
        messageValues
      }, res => {
        console.log("get_the_renarration_from_the_url res", res);
      });
    },
    follow_the_renarrator: function(renarratorWebId) {
      const messageType = "rn_follow_the_renarrator";
      let messageValues = {
        renarratorWebId: renarratorWebId
      };
      chrome.runtime.sendMessage({
        messageType,
        messageValues
      }, res => {
        console.log("rn_follow_the_renarrator", res);
      });
    },
    response: {
      create_response_comment_file_on_the_renarrators_pod: function(renarration_location, comment, comment_information) {
        const messageType = "rn_response_mode_update_comment_response_to_renarrators_pod";
        const messageValues = {
          renarration_location: renarration_location,
          comment: comment,
          comment_information: comment_information
        };
        chrome.runtime.sendMessage({
          messageType,
          messageValues
        }, messageResponse => {
          const response = messageResponse[0];
          const error = messageResponse[1];
          if (response !== null) {
            console.log("response: ", response.body);
            return response.body;

          } else {
            console.log("error: ", error);
            return error;
          }
        });

      },
      create_response_rate_file_on_the_renarrators_pod: function(renarration_location, rate, rate_information) {

        const messageType = "rn_response_mode_update_rate_response_to_renarrators_pod";
        const messageValues = {
          renarration_location: renarration_location,
          rate: rate,
          rate_information: rate_information,
        };
        chrome.runtime.sendMessage({
          messageType,
          messageValues
        }, messageResponse => {
          const response = messageResponse[0];
          const error = messageResponse[1];
          if (response !== null) {
            console.log("response: ", response.body);
            return response.body;

          } else {
            console.log("error: ", error);
            return error;
          }
        });

      },
      get_the_responses_from_the_rnex_and_then_from_the_renarrators_pod: function(desiredOnSourceDocument) {
        let desired_onsourcedocument = {
          desiredOnSourceDocument: desiredOnSourceDocument
        };
        const messageType = "rn_get_the_responses_from_the_rnex_and_then_from_the_renarrators_pod";
        const messageValues = desired_onsourcedocument;
        chrome.runtime.sendMessage({
          messageType,
          messageValues
        }, messageResponse => {
          const response = messageResponse[0];
          const error = messageResponse[1];
          if (response !== null) {
            console.log("rn_get_the_responses_from_the_rnex_and_then_from_the_renarrators_pod response: ", response.responseArray);

            return response.responseArray;
          } else {
            return error;
          }
        });
      }

    },
    send_renarration_or_response_to_text_analytics: function(text_to_analyze) {
      const messageType = "rn_send_renarration_or_response_to_text_analytics";
      let messageValues = {
        textToAnalyze: text_to_analyze
      };
      chrome.runtime.sendMessage({
        messageType,
        messageValues
      }, res => {
        console.log("rn_send_renarration_or_response_to_text_analytics", res);
      });
    }
  },
  controller: {
    creating_the_inside_of_main_mode_depending_on_the_chrome_storage: function(toggle_val, val) {
      clicked_mode_node = val;

      if (clicked_mode_node === "rn-navbar-toggle") {
        console.log("you have clicked rn-navbar-toggle");

        if (!toggle_val) {
          chrome_storage.rn_navbar_toggle_clicked_and_open(toggle_val);


          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-outer-main-wrap").classList.add("rn-outer-main-wrap-toggle");

          document.querySelector("body").classList.add("rn-body-margin-change");

          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-wrap").classList.add("rn-display-none");

          rn_navbar_toggle = false;
        } else {
          chrome_storage.rn_navbar_toggle_clicked_and_open(toggle_val);

          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-outer-main-wrap").classList.remove("rn-outer-main-wrap-toggle");

          document.querySelector("body").classList.remove("rn-body-margin-change");

          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-wrap").classList.remove("rn-display-none");

          rn_navbar_toggle = true;
        }
      } else if (clicked_mode_node === "rn-modes-profile") {
        chrome_storage.rn_current_mode("rn-modes-profile");
        chrome_storage.rn_remove_except_the_given_mode("rn-modes-profile");

        let renarration_onTargetDocument = document.querySelector("body").getAttribute("rnex-renarration-status-data-onTargetDocument");
        chrome_storage.rn_get_the_general_store_the_single_renarration_store_the_applied_renarration_on_the_page().then(res => {
          let webIdOfVisitedRenarrator = res.renarratedBy;
          let desiredOnSourceDocument;
          let currentMode = "profile_mode";
          let table_action;
          main_mode.model.get_the_renarrations_from_the_rnex_then_from_the_renarrators_pod(desiredOnSourceDocument, table_action, webIdOfVisitedRenarrator, currentMode);
        });


        main_mode.view.remove_body_listener();
        main_mode.view.remove_inside_rn_main_wrap();
        main_mode.view.choose_inside_rn_main_wrap(profile_mode.view.main());

        profile_mode.controller.main();
        main_mode.controller.go_and_apply_the_renarration_to_the_current_page_with_the_value_from_chrome_storage_profile_mode();
      } else if (clicked_mode_node === "rn-modes-recommending") {
        chrome_storage.rn_current_mode("rn-modes-recommending");
        chrome_storage.rn_remove_except_the_given_mode("rn-modes-recommending");
        main_mode.model.get_the_recommendations_from_the_rnex_pod();

        main_mode.view.remove_body_listener();
        main_mode.view.remove_inside_rn_main_wrap();
        main_mode.view.choose_inside_rn_main_wrap(recommending_mode.view.main());
        recommending_mode.controller.main();
        main_mode.controller.rn_general_check_the_store_to_apply_the_renarration();
      } else if (clicked_mode_node === "rn-modes-notification") {
        chrome_storage.rn_current_mode("rn-modes-notification");
        chrome_storage.rn_remove_except_the_given_mode("rn-modes-notification");
        main_mode.model.get_the_notifications_from_the_rnex_pod();

        main_mode.view.remove_body_listener();
        main_mode.view.remove_inside_rn_main_wrap();
        main_mode.view.choose_inside_rn_main_wrap(notification_mode.view.main());

        notification_mode.controller.main();
      } else if (clicked_mode_node === "rn-modes-response") {
        chrome_storage.rn_current_mode("rn-modes-response");
        chrome_storage.rn_remove_except_the_given_mode("rn-modes-response");


        let renarration_onTargetDocument = document.querySelector("body").getAttribute("rnex-renarration-status-data-onTargetDocument");
        main_mode.model.response.get_the_responses_from_the_rnex_and_then_from_the_renarrators_pod(renarration_onTargetDocument);

        main_mode.view.remove_body_listener();
        main_mode.view.remove_inside_rn_main_wrap();
        main_mode.view.choose_inside_rn_main_wrap(response_mode.view.main());

        response_mode.controller.main();
      } else if (clicked_mode_node === "rn-modes-account") {
        chrome_storage.rn_current_mode("rn-modes-account");
        chrome_storage.rn_remove_except_the_given_mode("rn-modes-account");

        let table_action;
        main_mode.model.get_the_renarrations_from_the_renarrators_pod(table_action);

        main_mode.view.remove_body_listener();
        main_mode.view.remove_inside_rn_main_wrap();
        main_mode.view.choose_inside_rn_main_wrap(account_mode.view.main());
        account_mode.controller.main();

        main_mode.controller.go_and_apply_the_renarration_to_the_current_page_with_the_value_from_chrome_storage();
      } else if (clicked_mode_node === "rn-modes-reading") {
        chrome_storage.rn_current_mode("rn-modes-reading");
        chrome_storage.rn_remove_except_the_given_mode("rn-modes-reading");
        chrome_storage.rn_get_the_extension_current_tab_url().then(s => {
          let webIdOfVisitedRenarrator;
          let desiredOnSourceDocument = s;
          let currentMode = "reading_mode";
          let table_action;
          main_mode.model.get_the_renarrations_from_the_rnex_then_from_the_renarrators_pod(desiredOnSourceDocument, table_action, webIdOfVisitedRenarrator, currentMode);
        }).catch(e => {
          let webIdOfVisitedRenarrator;
          let desiredOnSourceDocument = window.location.href;
          let currentMode = "reading_mode";
          let table_action;
          main_mode.model.get_the_renarrations_from_the_rnex_then_from_the_renarrators_pod(desiredOnSourceDocument, table_action, webIdOfVisitedRenarrator, currentMode);
          console.log("some error related with get_the_renarrations_from_the_rnex_then_from_the_renarrators_pod has occured");
        });

        main_mode.view.remove_body_listener();
        main_mode.view.remove_inside_rn_main_wrap();
        main_mode.view.choose_inside_rn_main_wrap(reading_mode.view.main());
        reading_mode.controller.main();
        main_mode.controller.go_and_apply_the_renarration_to_the_current_page_with_the_value_from_chrome_storage_reading_mode();
      } else if (clicked_mode_node === "rn-modes-writing") {
        if (document.body.getAttribute("rnex-renarration-status") === "renarration-is-applied") {
          document.body.removeAttribute("rnex-renarration-status");
          main_mode.view.refresh_the_page();
        }


        chrome_storage.rn_current_mode("rn-modes-writing");
        chrome_storage.rn_remove_except_the_given_mode("rn-modes-writing");
        main_mode.model.get_the_profile_info_with_track_session();

        document.querySelector("body").setAttribute("rnex_body", "rn_writing_mode");
        main_mode.view.remove_inside_rn_main_wrap();
        main_mode.view.choose_inside_rn_main_wrap(writing_mode.view.main());
        writing_mode.controller.main();

      }

    },
    go_and_apply_the_renarration_to_the_current_page_with_the_value_from_chrome_storage: function() {

      chrome_storage.rn_get_the_account_mode_clicked_row_unique_id_before_the_go_button().then(unique_id_url => {

        document.querySelector("#RNEx").shadowRoot.querySelectorAll(".rn-general-table-tbody tr").forEach(renarration_row => {
          console.log(renarration_row.getAttribute("rnuniqueid"));
          if (renarration_row.getAttribute("rnuniqueid") === unique_id_url) {
            renarration_row.click();
            console.log("this is it", renarration_row);

          }
        });

        return chrome_storage.rn_get_the_account_mode_clicked_row_renarrations_value_before_the_go_button();

      }).then(renarration => {
        console.log("this is it 2", renarration);
        if (document.body.getAttribute("rnex-renarration-status") === "renarration-is-applied") {
          document.body.removeAttribute("rnex-renarration-status");
          window.location.href = window.location.href;

        } else {
          if (renarration.onSourceDocument === window.location.href) {
            main_mode.controller.renarration_xpath_evaluation(renarration);
          }
        }
      }).catch(e => {
        console.log("error in go_and_apply_the_renarration_to_the_current_page_with_the_value_from_chrome_storage function and in chrome_storage.rn_get_the_account_mode_clicked_row_unique_id_before_the_go_button", e);
      });

    },
    go_and_apply_the_renarration_to_the_current_page_with_the_value_from_chrome_storage_reading_mode: function() {

      chrome_storage.rn_get_the_reading_mode_clicked_row_unique_id_before_the_go_button().then(unique_id_url => {

        document.querySelector("#RNEx").shadowRoot.querySelectorAll(".rn-general-table-tbody tr").forEach(renarration_row => {
          console.log(renarration_row.getAttribute("rnuniqueid"));
          if (renarration_row.getAttribute("rnuniqueid") === unique_id_url) {
            renarration_row.click();
            console.log("this is it", renarration_row);

          }
        });

        return chrome_storage.rn_get_the_reading_mode_clicked_row_renarrations_value_before_the_go_button();

      }).then(renarration => {
        console.log("this is it 2", renarration);
        if (document.body.getAttribute("rnex-renarration-status") === "renarration-is-applied") {
          document.body.removeAttribute("rnex-renarration-status");
          window.location.href = window.location.href;

        } else {
          if (renarration.onSourceDocument === window.location.href) {
            main_mode.controller.renarration_xpath_evaluation(renarration);

          }
        }
      }).catch(e => {
        console.log("error in go_and_apply_the_renarration_to_the_current_page_with_the_value_from_chrome_storage function and in chrome_storage.rn_get_the_reading_mode_clicked_row_unique_id_before_the_go_button", e);
      });

    },
    go_and_apply_the_renarration_to_the_current_page_with_the_value_from_chrome_storage_profile_mode: function() {

      chrome_storage.rn_get_the_profile_mode_clicked_row_unique_id_before_the_go_button().then(unique_id_url => {

        document.querySelector("#RNEx").shadowRoot.querySelectorAll(".rn-general-table-tbody tr").forEach(renarration_row => {
          console.log(renarration_row.getAttribute("rnuniqueid"));
          if (renarration_row.getAttribute("rnuniqueid") === unique_id_url) {
            renarration_row.click();

            console.log("this is it", renarration_row);

          }
        });

        return chrome_storage.rn_get_the_profile_mode_clicked_row_renarrations_value_before_the_go_button();

      }).then(renarration => {
        console.log("this is it 2", renarration);
        if (document.body.getAttribute("rnex-renarration-status") === "renarration-is-applied") {
          document.body.removeAttribute("rnex-renarration-status");
          window.location.href = window.location.href;

        } else {
          if (renarration.onSourceDocument === window.location.href) {
            main_mode.controller.renarration_xpath_evaluation(renarration);

          }
        }
      }).catch(e => {
        console.log("error in go_and_apply_the_renarration_to_the_current_page_with_the_value_from_chrome_storage function and in chrome_storage.rn_get_the_profile_mode_clicked_row_unique_id_before_the_go_button", e);
      });

    },
    go_and_apply_the_renarration_to_the_current_page_with_the_value_from_chrome_storage_recommending_mode: function() {
      chrome_storage.rn_get_the_profile_mode_clicked_row_unique_id_before_the_go_button().then(unique_id_url => {

        document.querySelector("#RNEx").shadowRoot.querySelectorAll(".rn-general-table-tbody tr").forEach(renarration_row => {
          console.log(renarration_row.getAttribute("rnuniqueid"));
          if (renarration_row.getAttribute("rnuniqueid") === unique_id_url) {
            renarration_row.click();
            console.log("this is it", renarration_row);

          }
        });

        return chrome_storage.rn_get_the_profile_mode_clicked_row_renarrations_value_before_the_go_button();

      }).then(renarration => {
        console.log("this is it 2", renarration);
        if (document.body.getAttribute("rnex-renarration-status") === "renarration-is-applied") {
          document.body.removeAttribute("rnex-renarration-status");
          window.location.href = window.location.href;

        } else {
          if (renarration.onSourceDocument === window.location.href) {
            main_mode.controller.renarration_xpath_evaluation(renarration);

          }
        }
      }).catch(e => {
        console.log("error in go_and_apply_the_renarration_to_the_current_page_with_the_value_from_chrome_storage function and in chrome_storage.rn_get_the_profile_mode_clicked_row_unique_id_before_the_go_button", e);
      });

    },
    renarration_xpath_evaluation: function(renarration) {
      console.log("renarration not ordered", renarration);
      chrome_storage.rn_general_store_the_single_renarration_store_the_applied_renarration_on_the_page(renarration);

      document.querySelector("#RNEx").shadowRoot.querySelector("#rn_whole_window_wrap").classList.add("gray-border-of-renarration-applied-sign");

      document.body.setAttribute("rnex-renarration-status", "renarration-is-applied");
      document.body.setAttribute("rnex-renarration-status-data-renarratedBy", renarration.renarratedBy);
      document.body.setAttribute("rnex-renarration-status-data-renarratedAt", renarration.renarratedAt);
      document.body.setAttribute("rnex-renarration-status-data-onSourceDocument", renarration.onSourceDocument);
      document.body.setAttribute("rnex-renarration-status-data-motivation", renarration.motivation);
      document.body.setAttribute("rnex-renarration-status-data-onTargetDocument", renarration.targetFileSourceUrlandUniqueId);

      renarration.renarrationTransformation.sort((a, b) => new Date(a.createdAt) - new Date(b.createdAt));
      console.log("orderedRenarration  ordered", renarration);

      renarration.renarrationTransformation.forEach((renarrationTransformationItem) => {
        main_mode.controller.xpath_evaluation(renarrationTransformationItem);
      });
    },
    xpath_evaluation: function(renarrationTransformation) {
      let action = renarrationTransformation.hasAction;

      function documentEvaluateXpath(xpath_string) {
        let node_of_xpath_target = null;
        let xpath_evaluation = document.evaluate(xpath_string, document, null, XPathResult.ANY_TYPE, null);

        let xpath_iteration_this_node = xpath_evaluation.iterateNext();
        while (xpath_iteration_this_node) {

          node_of_xpath_target = xpath_iteration_this_node;

          xpath_iteration_this_node = xpath_evaluation.iterateNext();
        }

        return node_of_xpath_target;
      }

      if (action === "insert") {
        let content = renarrationTransformation.content;
        let rangeSelectorXpathStartSelector = renarrationTransformation.RangeSelector.startSelectorXpathValue;
        let rangeSelectorXpathEndSelector = renarrationTransformation.RangeSelector.endSelectorXpathValue;

        if (rangeSelectorXpathStartSelector !== null && rangeSelectorXpathStartSelector !== undefined) {
          let refCurrentChosenNode = documentEvaluateXpath(rangeSelectorXpathStartSelector);
          if (refCurrentChosenNode) {
            let refCurrentChosenNodeOuterHTML = refCurrentChosenNode.outerHTML;
            refCurrentChosenNode.outerHTML = refCurrentChosenNodeOuterHTML + content;
          } else {
            console.log("error in documentEvaluateXpath for the hasAction insert rangeSelectorXpathStartSelector xpath string:", xpath_string, "renarrationTransformation", renarrationTransformation);
          }

        } else if (rangeSelectorXpathEndSelector !== null && rangeSelectorXpathEndSelector !== undefined) {
          let refCurrentChosenNode = documentEvaluateXpath(rangeSelectorXpathEndSelector);

          if (refCurrentChosenNode) {
            let refCurrentChosenNodeOuterHTML = refCurrentChosenNode.outerHTML;
            refCurrentChosenNode.outerHTML = content + refCurrentChosenNodeOuterHTML;
          } else {
            console.log("error in documentEvaluateXpath for the hasAction insert rangeSelectorXpathEndSelector xpath string:", xpath_string, "renarrationTransformation", renarrationTransformation);
          }

        }

      } else if (action === "replace") {
        let xpath = renarrationTransformation.xpathValue;
        let content = renarrationTransformation.content;

        let xpath_string = xpath;
        let xpath_content = content;

        if (documentEvaluateXpath(xpath_string)) {
          documentEvaluateXpath(xpath_string).outerHTML = xpath_content;

        } else {
          console.log("error in documentEvaluateXpath for the hasAction replace xpath string:", xpath_string, "renarrationTransformation", renarrationTransformation);
        }

      } else if (action === "remove") {
        let xpath = renarrationTransformation.xpathValue;
        let xpath_string = xpath;

        if (documentEvaluateXpath(xpath_string)) {
          documentEvaluateXpath(xpath_string).remove();
        } else {
          console.log("error in documentEvaluateXpath for the hasAction remove xpath string:", xpath_string, "renarrationTransformation", renarrationTransformation);
        }
      }

    },
    rn_navbar_vertical: {
      main_operation: function(eventTarget) {
        let renarration_applied = document.querySelector("body").getAttribute("rnex-renarration-status");

        if (renarration_applied) {
          main_mode.view.remove_the_selection_sign_from_menu(eventTarget);

          if (eventTarget.className === "rn-information-of-current-renarration") {
            main_mode.controller.rn_navbar_vertical.information_box_operations.main();
          } else if (eventTarget.className === "rn-modes-profile") {
            main_mode.controller.creating_the_inside_of_main_mode_depending_on_the_chrome_storage(!rn_navbar_toggle, "rn-modes-profile");
          } else if (eventTarget.className === "rn-modes-response") {
            main_mode.controller.creating_the_inside_of_main_mode_depending_on_the_chrome_storage(!rn_navbar_toggle, "rn-modes-response");
          }
        }
      },
      information_box_operations: {
        main: function() {

          let information_box_node = document.querySelector("#RNEx").shadowRoot.querySelector("#information-of-current-renarration-box");
          let more_information_box = document.querySelector("#RNEx").shadowRoot.querySelector("#more-information-for-current-renarration-box");
          if (more_information_box) {
            more_information_box.remove();
          }
          if (information_box_node) {
            information_box_node.remove();
          } else {
            main_mode.controller.rn_navbar_vertical.information_box_operations.active_operation();
            main_mode.controller.rn_navbar_vertical.information_box_operations.text_analytics.main();
          }

        },
        active_operation: function() {

          let renarration_renarratedBy = document.querySelector("body").getAttribute("rnex-renarration-status-data-renarratedBy");
          let renarration_renarratedAt = document.querySelector("body").getAttribute("rnex-renarration-status-data-renarratedAt");
          let renarration_onSourceDocument = document.querySelector("body").getAttribute("rnex-renarration-status-data-onSourceDocument");
          let renarration_motivation = document.querySelector("body").getAttribute("rnex-renarration-status-data-motivation");
          let regex = /[^#]*/;
          let onTargetUrl = document.querySelector("body").getAttribute("rnex-renarration-status-data-onTargetDocument");
          let onTargetUrlUntilTHeHashTag = onTargetUrl.match(regex);

          let renarration_onTargetDocument = onTargetUrlUntilTHeHashTag[0];

          let information_box = document.createElement("div");
          information_box.setAttribute("id", "information-of-current-renarration-box");
          information_box.setAttribute("class", "rn-renarration-information-box");

          information_box.innerHTML = `
             <div class="rn-general-content-label-value-group">
                <div class="rn-general-content-label-name">
                   <p >Renarrator</p>
                </div>
                <div class="rn-general-content-value-name">
                    <a href="${renarration_renarratedBy}" target="_blank" class="general-clickable-url"><p  class="slide-to-right-hover">${renarration_renarratedBy}</p></a>
                </div>
             </div>
             <div class="rn-general-content-label-value-group">
                <div class="rn-general-content-label-name">
                   <p >Date</p>
                </div>
                <div class="rn-general-content-value-name">
                   <p class="slide-to-right-hover">${renarration_renarratedAt}</p>
                </div>
             </div>
             <div class="rn-general-content-label-value-group">
                <div class="rn-general-content-label-name">
                   <p >Source</p>
                </div>
                <div class="rn-general-content-value-name">
                   <a href="${renarration_onSourceDocument}" target="_blank" class="general-clickable-url"><p class="slide-to-right-hover">${renarration_onSourceDocument}</p></a>
                </div>
             </div>
             <div class="rn-general-content-label-value-group">
                <div class="rn-general-content-label-name">
                   <p >Motivation</p>
                </div>
                <div class="rn-general-content-value-name">
                   <p class="slide-to-right-hover">${renarration_motivation}</p>
                </div>
             </div>
             <div class="rn-general-content-label-value-group">
                <div class="rn-general-content-label-name">
                   <p>Target</p>
                </div>
                <div class="rn-general-content-value-name">
                    <a href="${renarration_onTargetDocument}" target="_blank" class="general-clickable-url"><p  class="slide-to-right-hover">${renarration_onTargetDocument}</p></a>
                </div>
             </div>
             <input type="button" class="rn-general-searchbox-button" id="show_more_information" value="More Information">
             `;

          let rn_main_wrap_innerHTML = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-wrap").innerHTML;

          document.querySelector("#RNEx").shadowRoot.appendChild(information_box);

        },
        text_analytics: {
          main: function() {

            document.querySelector("#RNEx").shadowRoot.querySelector("#information-of-current-renarration-box").addEventListener("click", event => {
              if (event.target.id === "show_more_information") {
                chrome_storage.rn_get_the_general_store_the_single_renarration_store_the_applied_renarration_on_the_page().then(res => {
                  main_mode.controller.rn_navbar_vertical.information_box_operations.text_analytics.analyze_the_renarration(res);
                });

                chrome_storage.addListener(main_mode.controller.rn_navbar_vertical.information_box_operations.text_analytics.storage_listener);

              }

            });

          },
          analyze_the_renarration: function(renarration) {
            let renarrationTransformations = renarration.renarrationTransformation;
            console.log(renarration);
            let renarration_content = [];
            renarrationTransformations.forEach((rt) => {
              renarration_content.push(rt.content);
            });
            let text_to_analyze = renarration_content.join();

            main_mode.model.send_renarration_or_response_to_text_analytics(text_to_analyze);
          },
          apply_the_text_analytics_to_ui: function() {
            let more_information_box = document.querySelector("#RNEx").shadowRoot.querySelector("#more-information-for-current-renarration-box");

            function validURL(str) {
              var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
                '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
                '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
                '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
                '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
                '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
              return !!pattern.test(str);
            }

            if (more_information_box) {
              more_information_box.remove();
            } else {
              chrome_storage.rn_get_external_api_analytics_for_the_applied_renarration_on_the_page().then((res) => {

                let text_analytics = res.topics_extraction;
                console.log("apply_the_text_analytics_to_ui");
                console.log(text_analytics);
                let more_information_box = document.createElement("div");
                more_information_box.setAttribute("id", "more-information-for-current-renarration-box");
                more_information_box.setAttribute("class", "rn-renarration-more-information-box");

                let concept_list_html = `  <div class="rn-general-centered-content-label-name">
                   <p>Concepts</p>
                </div>`;
                let entity_list_html = `  <div class="rn-general-centered-content-label-name">
                   <p>Entities</p>
                </div>`;
                text_analytics.concept_list.forEach((item) => {
                  // item.form
                  // item.sementity.type;
                  concept_list_html += `<div class="rn-general-content-label-value-group">
                  <div class="rn-general-content-label-name">
                     <p>${item.sementity.type}</p>
                  </div>
                  <div class="rn-general-content-value-name">
                        <a ${(item.semld_list&&validURL(item.semld_list[0])) ? "href="+item.semld_list[0] :href=""}  target="_blank" class="general-clickable-url"><p>${item.form}</p></a>
                  </div>
                 </div>`;
                });

                text_analytics.entity_list.forEach((item) => {


                  entity_list_html += `<div class="rn-general-content-label-value-group">
                      <div class="rn-general-content-label-name">
                         <p >${item.sementity.type}</p>
                      </div>
                      <div class="rn-general-content-value-name">
                          <a ${(item.semld_list&&validURL(item.semld_list[0])) ? "href="+item.semld_list[0] :href=""} target="_blank" class="general-clickable-url"><p  >${item.form}</p></a>
                      </div>
                   </div>`;
                });

                more_information_box.innerHTML = concept_list_html + entity_list_html;

                document.querySelector("#RNEx").shadowRoot.appendChild(more_information_box);
              });
            }
          },
          storage_listener: function(changes, namespace) {
            console.log("changes", changes);
            console.log("namespace", namespace);
            for (let key in changes) {
              let storageChange = changes[key];
              if (key == "rn_external_analytics_result_for_applied_renarration") {
                main_mode.controller.rn_navbar_vertical.information_box_operations.text_analytics.apply_the_text_analytics_to_ui();
              }
            }
          }
        }
      }
    },
    rn_general_store_the_renarration_to_apply_the_page: function(renarration) {
      chrome_storage.rn_general_store_the_single_renarration_to_apply_the_page(renarration).then(success => {
        if (renarration.onSourceDocument === window.location.href) {

          window.location.href = renarration.onSourceDocument;
        } else {
          window.location.href = renarration.onSourceDocument;
        }
      });

    },
    rn_general_check_the_store_to_apply_the_renarration: function() {
      chrome_storage.rn_get_the_general_store_single_renarration_to_apply_the_page_values().then(success => {
        let renarration = success.renarration_to_apply;
        if (renarration) {
          main_mode.controller.renarration_xpath_evaluation(renarration);

          chrome_storage.rn_general_store_the_single_renarration_to_apply_the_page(null);
        }
      });

    },
    rn_general_create_the_renarration_general_table_outside_html: function(current_mode = undefined) {
      let renarratedAtOrOnSourceDocument = "";
      if (current_mode === "reading_mode") {
        renarratedAtOrOnSourceDocument = "Renarrator";
      } else if (current_mode === "profile_mode") {
        renarratedAtOrOnSourceDocument = "Source";
      } else if (current_mode === "account_mode") {
        renarratedAtOrOnSourceDocument = "Source";
      }
      let rn_table_tfoot = `<tfoot class="rn-general-table-tfooter"> <tr> <td><input type="button" class="rn-general-table-tfooter-buttons" id="go-to-previous-table-contents" value="<"></td><td><input type="button" class="rn-general-table-tfooter-buttons" id="go-to-next-table-contents" value=">"></td><td><input type="button" class="rn-general-table-tfooter-apply-button" id="go-and-apply-the-new-renarration" value="Apply"></td>

      <td> <input type="text" id="table-current-page" value="1" class="rn-general-table-tfooter-number-box" > <input disabled="" type="text" id="table-total-page" value="1" class="rn-general-table-tfooter-number-box" ></td></tr></tfoot>`;

      let rn_account_mode_renarrations_items = `<div class="rn-general-table">
      <table class="rn-general-table-overflow rn-general-table-scroll"><thead class="rn-general-table-thead rn-table-column-size"><tr class="rn-renarration-list-items-info"><th><p>Language</p></th><th><p>Motivation</p></th><th><p>Date</p></th><th><p>${renarratedAtOrOnSourceDocument}</p></th></tr></thead><tbody id="account-table" class="rn-general-table-tbody rn-table-column-size">` + `</tbody>${rn_table_tfoot}</table>` + `<div id="loading-animation-table" class="table-loading-animation" class=""><div class="circle-loader"></div></div>` + `</div>`;

      return rn_account_mode_renarrations_items;
    },
    rn_general_create_the_renarration_table_html: function(renarrations_and_the_table, current_mode = undefined) {
      let rn_profile_mode_renarrations_items_tbody = "";
      let renarratedAtOrOnSourceDocument = "";

      let renarration_documents_array = renarrations_and_the_table.desired_renarrations;
      let rn_profile_renarrations_title = "Null";
      let rn_profile_renarrations_language = "Null";
      let rn_profile_renarrations_motivation = "Null";
      let rn_profile_renarrations_date = "Null";
      let rn_profile_renarrations_url = "Null";
      let renarration_documents_array_date_sorted = renarration_documents_array.slice();
      renarration_documents_array_date_sorted.sort(function(a, b) {
        return new Date(b.renarratedAt) - new Date(a.renarratedAt);
      });
      for (let i = 0; i < renarration_documents_array_date_sorted.length; i++) {

        let langstring = [];

        if (renarration_documents_array_date_sorted[i].renarrationTransformation !== undefined && renarration_documents_array_date_sorted[i].renarrationTransformation !== null && renarration_documents_array_date_sorted[i].renarrationTransformation.length > 0) {
          let allLangItems = [];

          for (let j = 0; j < renarration_documents_array_date_sorted[i].renarrationTransformation.length; j++) {
            let rt = renarration_documents_array_date_sorted[i].renarrationTransformation[j];
            if (rt !== null && rt !== undefined && rt.language !== null && rt.language !== undefined) {
              allLangItems.push(rt.language);
            }
          }

          let uniqueLanguageItems = Array.from(new Set(allLangItems));
          langstring = uniqueLanguageItems.join();
        }

        if (current_mode === "reading_mode") {
          renarratedAtOrOnSourceDocument = "renarratedBy";
        } else if (current_mode === "profile_mode") {
          renarratedAtOrOnSourceDocument = "onSourceDocument";
        } else if (current_mode === "account_mode") {
          renarratedAtOrOnSourceDocument = "onSourceDocument";
        }

        let motivation_text = [];
        if (renarration_documents_array_date_sorted[i].motivation.length > 1) {
          renarration_documents_array_date_sorted[i].motivation.forEach((element) => {
            let edited_motivation_text_element = element.substr(0, 3) + ".";
            motivation_text.push(edited_motivation_text_element);
          });
        } else {
          motivation_text = renarration_documents_array_date_sorted[i].motivation;
        }


        let rn_target_url_and_unique_id_attribute = `rnuniqueid="${renarration_documents_array_date_sorted[i].targetFileSourceUrlandUniqueId}"`;
        rn_profile_mode_renarrations_items_tbody += `<tr ${rn_target_url_and_unique_id_attribute}><td><p>${langstring}</p></td><td><p>${motivation_text.join()}</p></td><td><p>${renarration_documents_array_date_sorted[i].renarratedAt}</p></td><td><p>${renarration_documents_array_date_sorted[i][renarratedAtOrOnSourceDocument]}</p></td></tr>` + `
        <div id="" class="rn-general-content-tooltip-box rn-general-tooltip">
        <div class="rn-general-content-tooltip-label-value-group"><div class="rn-general-content-tooltip-label-name"><p id="label-name">Language</p></div><div class="rn-general-content-tooltip-value-name"><p id="value-name" >${langstring}</p></div></div>

        <div class="rn-general-content-tooltip-label-value-group"><div class="rn-general-content-tooltip-label-name"><p id="label-name">Motivation</p></div><div class="rn-general-content-tooltip-value-name"><p id="value-name" class="slide-to-right">${renarration_documents_array_date_sorted[i].motivation}</p></div></div>

        <div class="rn-general-content-tooltip-label-value-group"><div class="rn-general-content-tooltip-label-name"><p id="label-name">Date</p></div><div class="rn-general-content-tooltip-value-name"><p id="value-name">${renarration_documents_array_date_sorted[i].renarratedAt}</p></div></div>

        <div class="rn-general-content-tooltip-label-value-group"><div class="rn-general-content-tooltip-label-name"><p id="label-name">Source</p></div><div class="rn-general-content-tooltip-value-name"><p id="value-name" class="slide-to-right">${renarration_documents_array_date_sorted[i].onSourceDocument}</p></div></div>

        <div class="rn-general-content-tooltip-label-value-group"><div class="rn-general-content-tooltip-label-name"><p id="label-name">Renarrator</p></div><div class="rn-general-content-tooltip-value-name"><p id="value-name" >${renarration_documents_array_date_sorted[i].renarratedBy}</p>
        </div></div></div>
        `;
      }
      document.querySelector("#RNEx").shadowRoot.querySelector("#loading-animation-table").classList.add("table-loading-animation-display-none");
      document.querySelector("#RNEx").shadowRoot.querySelector("#account-table").innerHTML = rn_profile_mode_renarrations_items_tbody;
      document.querySelector("#RNEx").shadowRoot.querySelector("#table-total-page").value = renarrations_and_the_table.total_number_of_page_in_the_table;

    },
    rn_general_create_the_renarration_table_html_footer_actions: function(e_target, mode) {
      let currentLocation = window.location.href;
      let current_page_string = document.querySelector("#RNEx").shadowRoot.querySelector("#table-current-page").value;
      let current_page = parseInt(current_page_string);
      if (e_target.id === "go-to-previous-table-contents") {
        console.log("previous", e_target);

        let page_val;
        if (current_page > 1) {
          page_val = (--current_page).toString();

        } else {
          page_val = "1";
        }
        document.querySelector("#RNEx").shadowRoot.querySelector("#table-current-page").value = page_val;

        let footer_action = {
          action: "previous",
          go_page: null,
          current_page: current_page
        };

        if (mode === "account_mode") {
          main_mode.model.get_the_renarrations_from_the_renarrators_pod(footer_action);
        } else if (mode === "reading_mode") {
          let webIdOfVisitedRenarrator;
          let desiredOnSourceDocument = currentLocation;
          let currentMode = "reading_mode";
          main_mode.model.get_the_renarrations_from_the_rnex_then_from_the_renarrators_pod(desiredOnSourceDocument, footer_action, webIdOfVisitedRenarrator, currentMode);
        } else if (mode === "profile_mode") {
          chrome_storage.rn_get_the_general_store_the_single_renarration_store_the_applied_renarration_on_the_page().then(res => {
            let webIdOfVisitedRenarrator = res.renarratedBy;
            let desiredOnSourceDocument;
            let currentMode = "profile_mode";
            main_mode.model.get_the_renarrations_from_the_rnex_then_from_the_renarrators_pod(desiredOnSourceDocument, footer_action, webIdOfVisitedRenarrator, currentMode);
          });
        }

        document.querySelector("#RNEx").shadowRoot.querySelector("#account-table").innerHTML = "";
        document.querySelector("#RNEx").shadowRoot.querySelector("#loading-animation-table").classList.remove("table-loading-animation-display-none");
      } else if (e_target.id === "go-to-next-table-contents") {
        console.log("next", e_target);


        let page_val;
        let number_of_last_page = parseInt(document.querySelector("#RNEx").shadowRoot.querySelector("#table-total-page").value);
        if (current_page < number_of_last_page) {
          page_val = (++current_page).toString();

        } else {
          page_val = number_of_last_page.toString();
        }

        document.querySelector("#RNEx").shadowRoot.querySelector("#table-current-page").value = page_val;

        let footer_action = {
          action: "next",
          go_page: null,
          current_page: current_page
        };

        if (mode === "account_mode") {
          main_mode.model.get_the_renarrations_from_the_renarrators_pod(footer_action);
        } else if (mode === "reading_mode") {
          let webIdOfVisitedRenarrator;
          let desiredOnSourceDocument = currentLocation;
          let currentMode = "reading_mode";
          main_mode.model.get_the_renarrations_from_the_rnex_then_from_the_renarrators_pod(desiredOnSourceDocument, footer_action, webIdOfVisitedRenarrator, currentMode);
        } else if (mode === "profile_mode") {
          chrome_storage.rn_get_the_general_store_the_single_renarration_store_the_applied_renarration_on_the_page().then(res => {
            let webIdOfVisitedRenarrator = res.renarratedBy;
            let desiredOnSourceDocument;
            let currentMode = "profile_mode";
            main_mode.model.get_the_renarrations_from_the_rnex_then_from_the_renarrators_pod(desiredOnSourceDocument, footer_action, webIdOfVisitedRenarrator, currentMode);
          });
        }

        document.querySelector("#RNEx").shadowRoot.querySelector("#account-table").innerHTML = "";
        document.querySelector("#RNEx").shadowRoot.querySelector("#loading-animation-table").classList.remove("table-loading-animation-display-none");
      }


    }
  }
};

reading_mode = {
  variables: {
    clicked_old_table_row: null,
    clicked_current_table_row: null
  },
  view: {
    variables: {
      clicked_item_nodes_array: []
    },
    main: function(clicked_item_nodes_array) {

      let rn_mode_header = `<div class="rn-current-mode-content-box"><div><p>Reading Mode</p></div></div>`;
      let rn_reading_mode_renarrations_header = `<div class="rn-general-content-box"><div class="rn-general-header"><h4>Renarrations</h4></div></div>`;

      let rn_reading_mode_renarrations_filter = `<div class="rn-general-content-box"><div class=""><table class="rn-general-searchbox-table"><tbody><tr><td><div class="rn-general-searchbox-header"><p>Filter</p></div></td><td><input type="text" class="rn-general-searchbox-textbox" placeholder="Type here..."></td><td><input type="button" class="rn-general-searchbox-button" value="Submit"></td></tr></tbody></table></div></div>`;

      let rn_reading_mode_renarrations_buttons = `<div class="rn-general-content-box">
      <input type="button" id="go-and-apply-the-new-renarration" class="rn-general-searchbox-button-second-type" value="Go">
    </div>`;

      let rn_main_mode_reading = document.createElement("div");
      rn_main_mode_reading.setAttribute("class", "rn-main-mode-reading");

      rn_main_mode_reading.innerHTML = rn_mode_header + rn_reading_mode_renarrations_header + main_mode.controller.rn_general_create_the_renarration_general_table_outside_html("reading_mode");

      reading_mode.model.get_renarration_items();

      return rn_main_mode_reading;
    }
  },
  model: {
    main: function() {

    },
    get_renarration_items: function() {


      chrome_storage.rn_get_the_reading_mode_renarrations_and_the_table().then(renarration_documents_array => {

        main_mode.controller.rn_general_create_the_renarration_table_html(renarration_documents_array, "reading_mode");

      }).catch(e => {
        console.log("some error occured getting the rn_get_the_reading_mode_renarration_documents_array from  the chrome storage", e);
      });

    }
  },
  controller: {
    main: function() {

      chrome_storage.addListener(reading_mode.controller.storage_listener.main);


      document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-reading").addEventListener("click", reading_mode.controller.reading_mode_main_operation_chain);

    },
    reading_mode_main_operation_chain: function(e) {
      console.log("event:", e.target);
      reading_mode.controller.highlighting_the_clicked_renarration_row(e);

      if (e.target.id === "go-and-apply-the-new-renarration") {

        reading_mode.controller.apply_the_renarration_to_the_body.main();

      }
      if (e.target.closest(".rn-general-table") && e.target.closest(".rn-general-table-tfooter")) {

        main_mode.controller.rn_general_create_the_renarration_table_html_footer_actions(e.target, "reading_mode");
      }
    },
    highlighting_the_clicked_renarration_row: function(e) {
      if (e.target.closest(".rn-general-table") && e.target.closest(".rn-general-table-tbody") && e.target.closest("tr")) {

        if (reading_mode.variables.clicked_old_table_row !== null) {
          reading_mode.variables.clicked_old_table_row.setAttribute("class", "");
        }
        reading_mode.variables.clicked_current_table_row = e.target.closest("tr");
        reading_mode.variables.clicked_current_table_row.setAttribute("class", "rn-general-table-tbody-tr-clicked");
        reading_mode.variables.clicked_old_table_row = reading_mode.variables.clicked_current_table_row;

      }
    },
    apply_the_renarration_to_the_body: {
      main: function() {

        chrome_storage.rn_get_the_reading_mode_renarrations_and_the_table().then(renarrations_and_the_table => {
          let renarration_documents_array = renarrations_and_the_table.desired_renarrations;
          reading_mode.controller.apply_the_renarration_to_the_body.url_unique_id_check(renarration_documents_array);

        }).catch(e => {
          console.log("some error occured getting the rn_get_the_reading_mode_renarration_documents_array from  the chrome storage inside the reading_mode.apply_the_renarration_to_the_body.main", e);
        });



      },
      html_attribute_get_renarration_id: function(renarration_documents_array) {

        if (reading_mode.variables.clicked_current_table_row !== null) {


          let html_attribute_get_renarration_unique_id = reading_mode.variables.clicked_current_table_row.getAttribute("rnuniqueid");

          console.log("html_attribute_get_renarration_id ", html_attribute_get_renarration_unique_id);

          console.log("clicked_current_table_row", reading_mode.variables.clicked_current_table_row);




          for (let i = 0; i < renarration_documents_array.length; i++) {
            if (html_attribute_get_renarration_unique_id === renarration_documents_array[i].targetFileSourceUrlandUniqueId) {
              console.log("return main_mode.variables.renarration_array_from_the_pod[i]", renarration_documents_array[i]);
              return renarration_documents_array[i];
            }
          }

          console.log("Inside reading_mode.controller.apply_the_renarration_to_the_body.html_attribute_get_renarration_id couldn't find the chosen document");
          return null;

        } else {

          console.log("Inside reading_mode.controller.apply_the_renarration_to_the_body.html_attribute_get_renarration_id a problem has occurred");

          return null;
        }

      },
      url_unique_id_check: function(renarration_documents_array) {
        let current_location_href = window.location.href;
        let chosen_renarration_document_from_the_array = reading_mode.controller.apply_the_renarration_to_the_body.html_attribute_get_renarration_id(renarration_documents_array);

        if (chosen_renarration_document_from_the_array !== null) {

          if (chosen_renarration_document_from_the_array.onSourceDocument === current_location_href) {
            if (document.body.getAttribute("rnex-renarration-status") === "renarration-is-applied") {
              reading_mode.controller.apply_the_renarration_to_the_body.setting_The_chrome_storage_for_the_clicked_renarration_and_redirecting_the_page(chosen_renarration_document_from_the_array);

            } else {
              main_mode.controller.renarration_xpath_evaluation(chosen_renarration_document_from_the_array);
            }

          } else {
            reading_mode.controller.apply_the_renarration_to_the_body.setting_The_chrome_storage_for_the_clicked_renarration_and_redirecting_the_page(chosen_renarration_document_from_the_array);
          }

        } else {
          console.log("chosen_renarration_document_from_the_array is null, in other words reading_mode.controller.apply_the_renarration_to_the_body.html_attribute_get_renarration_id() returns null value");
        }

      },
      setting_The_chrome_storage_for_the_clicked_renarration_and_redirecting_the_page: function(chosen_renarration_document_from_the_array) {
        chrome_storage.rn_reading_mode_clicked_row_before_the_go_button(chosen_renarration_document_from_the_array).then((s) => {
          console.log("1", s);
          return chrome_storage.rn_reading_mode_clicked_row_unique_id_before_the_go_button(chosen_renarration_document_from_the_array.targetFileSourceUrlandUniqueId);

        }).then((s) => {
          console.log("2", s);
          window.location.href = chosen_renarration_document_from_the_array.onSourceDocument;
        });
      }
    },
    table_item_click: function() {

    },
    storage_listener: {
      main: function(changes, namespace) {
        console.log("changes", changes);
        for (let key in changes) {
          let storageChange = changes[key];
          if (key == "reading_mode") {
            reading_mode.model.get_renarration_items();
          }
        }

      }
    }
  }
};

writing_mode = {
  view: {
    variables: {
      window_beforeunload_func: function(e) {
        e.preventDefault();
        e.returnValue = '';
      },
      clicked_item_nodes_array: [],
      last_chosen_target_html_node: null,
      current_chosen_target_html_node: null,
      hovered_target_html_node: null,
      renarration_information: {
        profile_card: null,
        webId: null,
        webId_label: null,
        idp: null,
        role: null,
        email: null,
        name: null,
        motivation: {
          translation: null,
          elaboration: null,
          simplification: null,
          alteration: null,
          correction: null
        },
        creation_time: null,
        creation_time_toString36: null,
        unique_id: null,
        source_url: null
      },
      renarration_transformation_contents: {
        text_contents: {
          count: 0,
          date: [],
          xpath: [],
          action: [],
          insert_value: [],
          insert_prev_xpath: [],
          insert_next_xpath: [],
          language: [],
          old: [],
          old_html_node: [],
          new: [],
          media_type: []
        }
      }
    },
    main: function() {
      let copy_paste_icon = chrome.runtime.getURL("img/OwnedIcons/copy.svg");

      let rn_main_mode_writing = document.createElement("div");
      rn_main_mode_writing.setAttribute("class", "rn-main-mode-writing");

      let rn_mode_header = `<div class="rn-current-mode-content-box"><div><p>Create Renarration</p></div></div>`;
      let rn_writing_mode_renarration_transformations_action_current_chosen_node_box = `<div class="rn-general-sliding-content-area-box"> <div class="rn-general-sliding-content-area-box-header">
      <input type="image" src=${copy_paste_icon} class="copy-the-content-icon" id="rn-copy-content-icon" rn-class-visibility="rn-visibility-hidden">

      <p class="rn-general-sliding-content-area-box-header-overflow">Selected HTML Part</p><div class="rn-updown-icon-group"> <i class="rn-icon-down"></i> <i class="rn-icon-up"></i> </div></div><div class="rn-general-sliding-content-area-box-body"> <p class="rn-general-sliding-content-area-text-place"> </p></div></div>`;

      let rn_writing_mode_renarration_transformations_actions = `<div class="rn-general-content-box"><div class="rn-general-header"><h4>Renarration Transformation</h4></div><div class="rn-general-content-header"><p>Action</p></div><div class="rn-general-content-radiobuttons-group"><div class="rn-general-content-radiobutton"><input type="radio" name="action-radiobuttons" id="remove" value="Remove" /><p>Remove</p></div><div class="rn-general-content-radiobutton"><input type="radio" name="action-radiobuttons" id="replace" value="Replace" checked/><p>Replace</p></div></div><div class="rn-general-content-radiobuttons-group"><div class="rn-general-content-radiobutton"><input type="radio" name="action-radiobuttons" id="insert" value="Insert" /><p>Insert</p></div></div><div class="rn-general-content-radiobuttons-group" id="rn-insert-radiobuttons-group" rn-class-display="rn-display-none" ><div class="rn-general-content-radiobutton"><input type="radio" name="insert-radiobuttons" id="insertbefore"><p>Insert Before</p></div><div class="rn-general-content-radiobutton"><input type="radio" name="insert-radiobuttons" id="insertafter" checked><p>Insert After</p></div></div>${rn_writing_mode_renarration_transformations_action_current_chosen_node_box}<div class="rich-textarea-toggle-group" ><p class="rich-textarea-toggle-text">Type HTML</p><label class="switch" id="richtext-area-toggle-switch"><input type="checkbox"  id="toggle-richtext-area">  <span class="slider round"></span></label></div><div class="richtext-and-advancedtext-area-toggle"><textarea id="rn_transformation_text" class="rn-general-textarea rn-display-none" placeholder="Type Html..."></textarea></div><div class="rn-general-content-label-value-group" id="rn_language_group"><div class="rn-general-searchbox-header"><p>Language</p></div><input type="text" class="rn-general-searchbox-textbox" id="rn_transformation_language" placeholder="Type here..." /></div><input type="button" class="rn-general-searchbox-longer-button" id="save_the_rt" value="Save Renarration Transformation" /></div>`;

      let rn_writing_mode_renarration_motivations = `<div class="rn-general-content-box"><div class="rn-general-header"> <h4>RENARRATION</h4> </div> <div class="rn-general-content-header"> <p>Motivation</p> </div> <div class="rn-general-content-radiobuttons-group"> <div class="rn-general-content-radiobutton"> <input type="radio" name="motivation-radiobuttons-1" id="motivation-radiobutton-simplification" value="Simplification"> <p>Simplification</p> </div> <div class="rn-general-content-radiobutton"> <input type="radio" name="motivation-radiobuttons-2" id="motivation-radiobutton-alteration" value="Alteration"> <p>Alteration</p> </div> </div> <div class="rn-general-content-radiobuttons-group"> <div class="rn-general-content-radiobutton"> <input type="radio" name="motivation-radiobuttons-3" id="motivation-radiobutton-correction" value="Correction"> <p>Correction</p> </div> <div class="rn-general-content-radiobutton"> <input type="radio" name="motivation-radiobuttons-4"  id="motivation-radiobutton-elaboration" value="Elaboration"> <p>Elaboration</p> </div> </div> <div class="rn-general-content-radiobuttons-group"> <div class="rn-general-content-radiobutton"> <input type="radio" name="motivation-radiobuttons-5"  id="motivation-radiobutton-translation"  value="Translation"> <p>Translation</p> </div> </div> <input type="button" class="rn-general-searchbox-button" id="save_the_rn" value="Save Renarration"></div>`;

      rn_main_mode_writing.innerHTML = rn_mode_header + rn_writing_mode_renarration_transformations_actions + rn_writing_mode_renarration_motivations;

      return rn_main_mode_writing;
    }
  },
  model: {
    main: function() {

    },
    track_session: function() {
      const messageType = "rn_tracksession";
      const messageValues = {
        location: location
      };
      chrome.runtime.sendMessage({
        messageType,
        messageValues
      }, messageResponse => {
        const response = messageResponse[0];
        const error = messageResponse[1];
        if (response !== null) {
          console.log("track_session response: ", response.body);
          return response.body;
        } else {
          console.log("track_session error: ", error);
          return error;
        }
      });
    }
  },
  controller: {
    main: function() {

      window.addEventListener('beforeunload', writing_mode.view.variables.window_beforeunload_func);

      writing_mode.controller.richTextAreaListener();
      writing_mode.controller.copyTheChosenNodeListener();



      document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").addEventListener("click", writing_mode.controller.writing_mode_remove_and_choose);

      document.querySelector("[rnex_body=rn_writing_mode]").addEventListener("mouseover", writing_mode.controller.mouseover_operations);
      document.querySelector("[rnex_body=rn_writing_mode]").addEventListener("click",
        writing_mode.controller.writing_mode_body_remove_and_choose);

    },
    copyTheChosenNodeListener: function() {
      document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#rn-copy-content-icon").addEventListener("click", (e) => {
        let htmlInputToggleOn = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#toggle-richtext-area").checked;

        if (htmlInputToggleOn) {
          let copiedHTML = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector(".rn-general-sliding-content-area-text-place").innerText;

          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#rn_transformation_text").value = copiedHTML;
        }

      });

    },
    richTextAreaListener: function() {

      function mediaTypeTextListeners() {

        function textRn(cssValue, textValue) {
          let textOfTextArea = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").value;
          let cssTextOfTextArea = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").style.cssText;
          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#rn_transformation_text").value = `<p style="${cssTextOfTextArea}">${textOfTextArea}</p>`;

          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").placeholder = `Type something`;
        }

        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-color-input").addEventListener("input", (e) => {
          if (e.target.value) {
            document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").style.color = e.target.value;
          } else {
            document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").style.color = "";
          }
          textRn();
        });

        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-font-size-input").addEventListener("input", (e) => {
          if (e.target.value) {
            document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").style.fontSize = e.target.value + "px";
          } else {
            document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").style.fontSize = "";
          }
          textRn();
        });

        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-font-prop-bold").addEventListener("input", (e) => {
          if (e.target.checked) {
            document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").style.fontWeight = "bold";
          } else {
            document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").style.fontWeight = "";
          }
          textRn();
        });

        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-font-prop-italic").addEventListener("input", (e) => {
          if (e.target.checked) {
            document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").style.fontStyle = "italic";
          } else {
            document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").style.fontStyle = "";
          }
          textRn();
        });

        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-font-prop-underline").addEventListener("input", (e) => {
          if (e.target.checked) {
            document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").style.textDecorationLine = "underline";
          } else {
            document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").style.textDecorationLine = "";
          }
          textRn();
        });
        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").addEventListener("input", (e) => {
          textRn();
        });
        textRn();
      }

      function mediaTypeAudioListeners() {
        function audioRn() {
          let textOfTextArea = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").value;
          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").style.cssText = "";
          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#rn_transformation_text").value = `<audio controls><source src=${textOfTextArea} type="audio/ogg"><source src=${textOfTextArea} type="audio/mpeg">Your browser does not support the audio element.</audio>`;

          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").placeholder = `Type an audio url`;
        }
        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").addEventListener("input", (e) => {
          audioRn();
        });
        audioRn();
      }

      function mediaTypeVideoListeners() {
        let cssOfTextAreaWidth = "";
        let cssOfTextAreaHeigth = "";

        function videoRn(cssWidth = cssOfTextAreaWidth, cssHeight = cssOfTextAreaHeigth) {
          let textOfTextArea = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").value;
          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").style.cssText = "";
          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#rn_transformation_text").value = `
            <iframe  src=${textOfTextArea} width="${cssWidth}" height="${cssHeight}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;

          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").placeholder = `Type a video url`;
        }

        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-media-video-width-input").addEventListener("input", (e) => {
          if (e.target.value) {
            cssOfTextAreaWidth = e.target.value;
          } else {
            cssOfTextAreaWidth = "";
          }
          videoRn();

        });

        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-media-video-height-input").addEventListener("input", (e) => {
          if (e.target.value) {
            cssOfTextAreaHeigth = e.target.value;
          } else {
            cssOfTextAreaHeigth = "";
          }
          videoRn();
        });

        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").addEventListener("input", (e) => {
          videoRn();
        });
        videoRn();
      }

      function mediaTypeImageListeners() {
        let cssOfTextAreaWidth = "";
        let cssOfTextAreaHeigth = "";

        function imageRn(cssWidth = cssOfTextAreaWidth, cssHeight = cssOfTextAreaHeigth) {
          let textOfTextArea = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").value;
          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").style.cssText = "";
          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#rn_transformation_text").value = `
          <img  src=${textOfTextArea} width="${cssWidth}" height="${cssHeight}">
            `;

          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").placeholder = `Type a image url`;
        }


        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-media-image-width-input").addEventListener("input", (e) => {
          if (e.target.value) {
            cssOfTextAreaWidth = e.target.value;
          } else {
            cssOfTextAreaWidth = "";
          }
          imageRn();

        });

        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-media-image-height-input").addEventListener("input", (e) => {
          if (e.target.value) {
            cssOfTextAreaHeigth = e.target.value;
          } else {
            cssOfTextAreaHeigth = "";
          }
          imageRn();
        });

        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").addEventListener("input", (e) => {
          imageRn();
        });
        imageRn();
      }

      function controlTheTextAreaAndSavingRenarrationTransformation(value = true) {
        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").disabled = value;
        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#save_the_rt").disabled = value;
      }

      function richTextAddListeners() {

        let rich_textarea_box = document.createElement("div");
        rich_textarea_box.setAttribute("class", "rich-textarea-box");

        let rich_textarea_header_expansion = document.createElement("div");
        rich_textarea_header_expansion.setAttribute("class", "rich-textarea-header-expansion");

        let richtext_area_header_html = `<div class="rich-textarea-header" ><p class="rich-textarea-label" style="">Type</p><label class="rich-textarea-header-media-radiobutton-group"> <input type="radio" name="media-radiobuttons" id="media-radiobutton-text" value="Text"> <span class="rich-textarea-header-media-radiobutton-group-text">Text</span> </label><label class="rich-textarea-header-media-radiobutton-group"> <input type="radio" name="media-radiobuttons" id="media-radiobutton-image" value="Image"> <span class="rich-textarea-header-media-radiobutton-group-text">Image</span> </label><label class="rich-textarea-header-media-radiobutton-group"> <input type="radio" name="media-radiobuttons" id="media-radiobutton-audio" value="Audio"> <span class="rich-textarea-header-media-radiobutton-group-text">Audio</span> </label><label class="rich-textarea-header-media-radiobutton-group"> <input type="radio" name="media-radiobuttons" id="media-radiobutton-video" value="Video"> <span class="rich-textarea-header-media-radiobutton-group-text">Video</span> </label></div>`;

        let richtext_area_header_expansion_html_media_audio = ``;

        let richtext_area_header_expansion_html_media_video = `
        <p class="rich-textarea-label" style="">Width</p><input type="number" min="256" max="1024" class="rich-textarea-image-size-input" id="richtext-media-video-width-input">
        <p class="rich-textarea-label" style="">Height</p><input type="number" min="256" max="1024" class="rich-textarea-image-size-input" id="richtext-media-video-height-input">`;
        let richtext_area_header_expansion_html_media_image = `
      <p class="rich-textarea-label" style="">Width</p><input type="number" min="256" max="1024" class="rich-textarea-image-size-input" id="richtext-media-image-width-input">
      <p class="rich-textarea-label" style="">Height</p><input type="number" min="256" max="1024" class="rich-textarea-image-size-input" id="richtext-media-image-height-input">`;


        let richtext_area_header_expansion_html_media_text = `<p class="rich-textarea-label" style="">Size</p><input type="number" min="8" max="96" class="rich-textarea-font-size-input" id="richtext-font-size-input"><p class="rich-textarea-label" style="">Color</p><input type="color" class="rich-textarea-color-input" id="richtext-color-input"><label class="rn-checkbox-container"> <input type="checkbox" id="richtext-font-prop-bold"> <span class="rn-checkbox-checkmark" style="font-weight: bold;">B</span></label><label class="rn-checkbox-container"> <input type="checkbox" id="richtext-font-prop-italic"> <span class="rn-checkbox-checkmark" style="font-style: italic;">I</span></label><label class="rn-checkbox-container"> <input type="checkbox" id="richtext-font-prop-underline"> <span class="rn-checkbox-checkmark" style="text-decoration-line: underline;">U</span></label>`;

        rich_textarea_box.innerHTML = `${richtext_area_header_html}<div class="rich-textarea-header-expansion-group"></div><textarea id="richtext-textarea" class="rich-textarea-text"></textarea>`;

        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector(".richtext-and-advancedtext-area-toggle").prepend(rich_textarea_box);

        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#rn_transformation_text").classList.add("rn-display-none");

        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").placeholder = `Choose a media type to create renarration transformation`;


        controlTheTextAreaAndSavingRenarrationTransformation(true);




        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#media-radiobutton-text").addEventListener("input", (e) => {
          if (e.target.value) {
            controlTheTextAreaAndSavingRenarrationTransformation(false);

            rich_textarea_header_expansion.innerHTML = richtext_area_header_expansion_html_media_text;
            document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector(".rich-textarea-header-expansion-group").prepend(rich_textarea_header_expansion);

            mediaTypeTextListeners();
          } else {

          }
        });

        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#media-radiobutton-audio").addEventListener("input", (e) => {
          if (e.target.value) {
            controlTheTextAreaAndSavingRenarrationTransformation(false);

            document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector(".rich-textarea-header-expansion-group").innerHTML = "";

            mediaTypeAudioListeners();
          } else {

          }
        });
        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#media-radiobutton-video").addEventListener("input", (e) => {
          if (e.target.value) {
            controlTheTextAreaAndSavingRenarrationTransformation(false);

            rich_textarea_header_expansion.innerHTML = richtext_area_header_expansion_html_media_video;
            document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector(".rich-textarea-header-expansion-group").prepend(rich_textarea_header_expansion);

            mediaTypeVideoListeners();
          } else {

          }
        });

        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#media-radiobutton-image").addEventListener("input", (e) => {
          if (e.target.value) {
            controlTheTextAreaAndSavingRenarrationTransformation(false);
            rich_textarea_header_expansion.innerHTML = richtext_area_header_expansion_html_media_image;
            document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector(".rich-textarea-header-expansion-group").prepend(rich_textarea_header_expansion);

            mediaTypeImageListeners();
          } else {

          }
        });

      }

      richTextAddListeners();

      document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#toggle-richtext-area").addEventListener("input", (e) => {
        if (!e.target.checked) {
          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector(".copy-the-content-icon").setAttribute("rn-class-visibility", "rn-visibility-hidden");
          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#rn_transformation_text").value = "";
          richTextAddListeners();
        } else {
          controlTheTextAreaAndSavingRenarrationTransformation(false);

          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector(".copy-the-content-icon").removeAttribute("rn-class-visibility", "rn-visibility-hidden");
          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#rn_transformation_text").value = "";
          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#rn_transformation_text").classList.remove("rn-display-none");
          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector(".rich-textarea-box").remove();

        }
      });


    },
    mouseover_operations: function(e) {
      if (e.target.id !== "RNEx") {
        if (writing_mode.view.variables.hovered_target_html_node) {
          writing_mode.view.variables.hovered_target_html_node.removeAttribute("rn-hovered-class", "rn-hovered-html-node");
        }
        writing_mode.view.variables.hovered_target_html_node = e.target;
        writing_mode.view.variables.hovered_target_html_node.setAttribute("rn-hovered-class", "rn-hovered-html-node");
      }
    },
    writing_mode_remove_and_choose: function(e) {
      e.stopPropagation();
      console.log("event:", e.target);

      let replace_value = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#replace").checked;
      let remove_value = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#remove").checked;
      let insert_value = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#insert").checked;

      if (e.target.id === "save_the_rt") {
        if (replace_value || remove_value || insert_value) {
          writing_mode.controller.replace_the_text_inside_the_body(e);
        } else {
          alert("You should choose an action, before creating a renarration transformation");
        }
      } else if (e.target.id === "save_the_rn") {
        writing_mode.controller.create_the_renarration(e);
      }


      if (e.target.parentNode.className === "rn-updown-icon-group") {
        writing_mode.controller.toggle_the_general_sliding_content_area_box(e);
      }

      writing_mode.controller.actionRadioButtonClickOperations(e.target.id);

    },
    actionRadioButtonClickOperations: function(e_target_id) {
      function controlTheMediaButtonsTextAreaAndSavingRenarrationTransformation(value = true) {
        let richtext_textarea_node = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea");

        let save_the_rt_node = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#save_the_rt");

        let media_radiobutton_image_node = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#media-radiobutton-image");

        let media_radiobutton_text_node = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#media-radiobutton-text");

        let media_radiobutton_audio_node = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#media-radiobutton-audio");

        let media_radiobutton_video_node = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#media-radiobutton-video");

        let htmlInputToggleOn = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#toggle-richtext-area").checked;

        if (richtext_textarea_node) {
          richtext_textarea_node.disabled = value;
        }
        if (save_the_rt_node) {
          save_the_rt_node.disabled = value;
          if (htmlInputToggleOn) {
            save_the_rt_node.disabled = false;
          }
        }
        if (media_radiobutton_image_node) {
          media_radiobutton_image_node.checked = false;
        }
        if (media_radiobutton_text_node) {
          media_radiobutton_text_node.checked = false;
        }
        if (media_radiobutton_audio_node) {
          media_radiobutton_audio_node.checked = false;
        }
        if (media_radiobutton_video_node) {
          media_radiobutton_video_node.checked = false;
        }

      }

      if (e_target_id === "replace" || e_target_id === "remove" || e_target_id === "insert") {
        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#rn_transformation_text").value = "";

        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#rn_transformation_text").removeAttribute("rn-class-display", "rn-display-none");

        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector(".richtext-and-advancedtext-area-toggle").removeAttribute("rn-class-display", "rn-display-none");

        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#rn_language_group").removeAttribute("rn-class-display", "rn-display-none");

        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#rn-insert-radiobuttons-group").setAttribute("rn-class-display", "rn-display-none");

        try {
          controlTheMediaButtonsTextAreaAndSavingRenarrationTransformation(true);
        } catch (err) {
          console.log(err);
        }

        if (e_target_id === "replace") {

        } else if (e_target_id === "remove") {

          controlTheMediaButtonsTextAreaAndSavingRenarrationTransformation(false);

          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#rn_transformation_text").setAttribute("rn-class-display", "rn-display-none");
          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#rn_language_group").setAttribute("rn-class-display", "rn-display-none");

          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector(".richtext-and-advancedtext-area-toggle").setAttribute("rn-class-display", "rn-display-none");

        } else if (e_target_id === "insert") {
          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#rn-insert-radiobuttons-group").removeAttribute("rn-class-display", "rn-display-none");
        }

      }
    },
    toggle_the_general_sliding_content_area_box: function(e) {
      e.target.closest(".rn-general-sliding-content-area-box").classList.toggle("rn-general-sliding-content-area-display-none");
      e.target.closest(".rn-general-sliding-content-area-box").querySelector(".rn-general-sliding-content-area-box-body").classList.toggle("rn-general-sliding-content-area-display-slide-to-none");
    },
    writing_mode_body_remove_and_choose: function(e) {
      console.log("body event:", e.target);
      if (e.target.id !== "RNEx") {
        if (writing_mode.view.variables.last_chosen_target_html_node) {
          writing_mode.view.variables.last_chosen_target_html_node.removeAttribute("rn-class", "rn-chosen-html-node");
        }
        writing_mode.view.variables.current_chosen_target_html_node = e.target;
        writing_mode.view.variables.last_chosen_target_html_node = writing_mode.view.variables.current_chosen_target_html_node;
        writing_mode.view.variables.current_chosen_target_html_node.setAttribute("rn-class", "rn-chosen-html-node");

        if (e.target.hasAttribute("href")) {
          console.log("e target href", e.target.getAttribute("href"));
          e.preventDefault();
        }

        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector(".rn-general-sliding-content-area-text-place").innerText = writing_mode.controller.clone_the_target_node_and_delete_rn_hovered_and_chosen_target_css(writing_mode.view.variables.current_chosen_target_html_node);
      }
    },
    clone_the_target_node_and_delete_rn_hovered_and_chosen_target_css: function(current_chosen_target) {
      let clonedCurrentChosenTargetNode = current_chosen_target.cloneNode();
      let clonedCurrentInnerHTML = current_chosen_target.innerHTML;
      clonedCurrentChosenTargetNode.innerHTML = clonedCurrentInnerHTML;
      clonedCurrentChosenTargetNode.removeAttribute("rn-class", "rn-chosen-html-node");
      clonedCurrentChosenTargetNode.removeAttribute("rn-hovered-class", "rn-hovered-html-node");
      let clonedCurrentChosenTargetNodeOuterHTML = clonedCurrentChosenTargetNode.outerHTML;
      return clonedCurrentChosenTargetNodeOuterHTML;
    },
    replace_the_text_inside_the_body: function(e) {

      let replace_value = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#replace").checked;
      let remove_value = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#remove").checked;
      let insert_value = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#insert").checked;

      if (replace_value || remove_value || insert_value) {


        if (replace_value) {
          writing_mode.controller.renarration_action_operations.replace();
        } else if (remove_value) {
          writing_mode.controller.renarration_action_operations.remove();
        } else if (insert_value) {
          writing_mode.controller.renarration_action_operations.insert();
        }
      }
    },
    renarration_action_operations: {
      remove: function() {
        let action_string = "remove";
        let i_count = writing_mode.view.variables.renarration_transformation_contents.text_contents.count;

        //xpath of the content to an array
        writing_mode.view.variables.renarration_transformation_contents.text_contents.xpath[i_count] = writing_mode.controller.xpath_generator_renarration_content(writing_mode.view.variables.current_chosen_target_html_node);

        //saving the action value to an array
        writing_mode.view.variables.renarration_transformation_contents.text_contents.action[i_count] = action_string;

        //saving the new content value to an array
        writing_mode.view.variables.renarration_transformation_contents.text_contents.new[i_count] = "";

        //saving the date value to an array
        let renarration_transformation_date = new Date();
        writing_mode.view.variables.renarration_transformation_contents.text_contents.date[i_count] = renarration_transformation_date.toISOString();

        //saving the language value to an array
        writing_mode.view.variables.renarration_transformation_contents.text_contents.language[i_count] = "";

        //saving the old content value to an array
        writing_mode.view.variables.renarration_transformation_contents.text_contents.old[i_count] = writing_mode.view.variables.current_chosen_target_html_node.innerText;

        //saving the old html node element value to an array
        writing_mode.view.variables.renarration_transformation_contents.text_contents.old_html_node[i_count] = writing_mode.view.variables.last_chosen_target_html_node.outerHTML;

        //changing the body of html document with new content value from an array
        writing_mode.view.variables.current_chosen_target_html_node.remove();

        //insert value of the renarration transformation
        writing_mode.view.variables.renarration_transformation_contents.text_contents.insert_value[i_count] = null;

        //incrementing the value of the text_contents counter
        writing_mode.view.variables.renarration_transformation_contents.text_contents.count++;
      },
      replace: function() {
        let action_string = "replace";
        let i_count = writing_mode.view.variables.renarration_transformation_contents.text_contents.count;

        //xpath of the content to an array
        writing_mode.view.variables.renarration_transformation_contents.text_contents.xpath[i_count] = writing_mode.controller.xpath_generator_renarration_content(writing_mode.view.variables.current_chosen_target_html_node);

        //saving the action value to an array
        writing_mode.view.variables.renarration_transformation_contents.text_contents.action[i_count] = action_string;

        //saving the new content value to an array

        writing_mode.view.variables.renarration_transformation_contents.text_contents.new[i_count] = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#rn_transformation_text").value;
        //emptying the textarea value
        if (document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea")) {
          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").value = "";
        }
        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#rn_transformation_text").value = "";
        //saving the date value to an array
        let renarration_transformation_date = new Date();
        writing_mode.view.variables.renarration_transformation_contents.text_contents.date[i_count] = renarration_transformation_date.toISOString();

        //saving the language value to an array
        writing_mode.view.variables.renarration_transformation_contents.text_contents.language[i_count] = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#rn_transformation_language").value;

        //saving the old content value to an array
        writing_mode.view.variables.renarration_transformation_contents.text_contents.old[i_count] = writing_mode.view.variables.current_chosen_target_html_node.innerText;

        //saving the old html node element value to an array
        writing_mode.view.variables.renarration_transformation_contents.text_contents.old_html_node[i_count] = writing_mode.view.variables.last_chosen_target_html_node.outerHTML;

        //changing the body of html document with new content value from an array
        writing_mode.view.variables.current_chosen_target_html_node.outerHTML = writing_mode.view.variables.renarration_transformation_contents.text_contents.new[i_count];

        //insert value of the renarration transformation
        writing_mode.view.variables.renarration_transformation_contents.text_contents.insert_value[i_count] = null;

        //media type of the renarration transformation
        let imageMediaType = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#media-radiobutton-image");
        let textMediaType = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#media-radiobutton-text");
        let audioMediaType = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#media-radiobutton-audio");
        let videoMediaType = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#media-radiobutton-video");

        let imageMediaTypeIsChecked = null;
        let textMediaTypeIsChecked = null;
        let audioMediaTypeIsChecked = null;
        let videoMediaTypeIsChecked = null;
        try {
          if (imageMediaType) {
            imageMediaTypeIsChecked = imageMediaType.checked;
          }
          if (textMediaType) {
            textMediaTypeIsChecked = textMediaType.checked;
          }
          if (audioMediaType) {
            audioMediaTypeIsChecked = audioMediaType.checked;
          }
          if (videoMediaType) {
            videoMediaTypeIsChecked = videoMediaType.checked;
          }
        } catch (err) {
          console.log(err);
        }

        if (textMediaTypeIsChecked) {
          writing_mode.view.variables.renarration_transformation_contents.text_contents.media_type[i_count] = "Text";
        } else if (imageMediaTypeIsChecked) {
          writing_mode.view.variables.renarration_transformation_contents.text_contents.media_type[i_count] = "Image";
        } else if (audioMediaTypeIsChecked) {
          writing_mode.view.variables.renarration_transformation_contents.text_contents.media_type[i_count] = "Audio";
        } else if (videoMediaTypeIsChecked) {
          writing_mode.view.variables.renarration_transformation_contents.text_contents.media_type[i_count] = "Video";
        }

        //incrementing the value of the text_contents counter
        writing_mode.view.variables.renarration_transformation_contents.text_contents.count++;
      },
      insert: function() {
        let action_string = "insert";
        let i_count = writing_mode.view.variables.renarration_transformation_contents.text_contents.count;

        //xpath of the content to an array
        writing_mode.view.variables.renarration_transformation_contents.text_contents.xpath[i_count] = writing_mode.controller.xpath_generator_renarration_content(writing_mode.view.variables.current_chosen_target_html_node);

        //saving the action value to an array
        writing_mode.view.variables.renarration_transformation_contents.text_contents.action[i_count] = action_string;

        //saving the new content value to an array
        writing_mode.view.variables.renarration_transformation_contents.text_contents.new[i_count] = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#rn_transformation_text").value;
        //emptying the textarea value
        if (document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea")) {
          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#richtext-textarea").value = "";
        }
        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#rn_transformation_text").value = "";
        //saving the date value to an array
        let renarration_transformation_date = new Date();
        writing_mode.view.variables.renarration_transformation_contents.text_contents.date[i_count] = renarration_transformation_date.toISOString();

        //saving the language value to an array
        writing_mode.view.variables.renarration_transformation_contents.text_contents.language[i_count] = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#rn_transformation_language").value;

        //saving the old content value to an array
        writing_mode.view.variables.renarration_transformation_contents.text_contents.old[i_count] = writing_mode.view.variables.current_chosen_target_html_node.innerText;

        //saving the old html node element value to an array
        writing_mode.view.variables.renarration_transformation_contents.text_contents.old_html_node[i_count] = writing_mode.view.variables.last_chosen_target_html_node.outerHTML;

        //changing the body of html document with new content value from an array !!Important!!
        let insertbefore_checked = document.querySelector("#RNEx").shadowRoot.querySelector("#insertbefore").checked;
        let insertafter_checked = document.querySelector("#RNEx").shadowRoot.querySelector("#insertafter").checked;

        //insert before and after operations
        if (insertbefore_checked) {
          // previous sibling of current node
          if (writing_mode.view.variables.current_chosen_target_html_node.previousElementSibling) {
            writing_mode.view.variables.renarration_transformation_contents.text_contents.insert_prev_xpath[i_count] =
              writing_mode.controller.xpath_generator_renarration_content(writing_mode.view.variables.current_chosen_target_html_node.previousElementSibling);
          } else {
            writing_mode.view.variables.renarration_transformation_contents.text_contents.insert_prev_xpath[i_count] = null;
          }

          //insert before operations
          writing_mode.controller.insertBeforeAfterOperations(writing_mode.view.variables.current_chosen_target_html_node, i_count, "before");
          writing_mode.view.variables.renarration_transformation_contents.text_contents.insert_value[i_count] = "before";
        } else if (insertafter_checked) {

          // next sibling of current node
          if (writing_mode.view.variables.current_chosen_target_html_node.nextElementSibling) {
            writing_mode.view.variables.renarration_transformation_contents.text_contents.insert_next_xpath[i_count] =
              writing_mode.controller.xpath_generator_renarration_content(writing_mode.view.variables.current_chosen_target_html_node.nextElementSibling);
          } else {
            writing_mode.view.variables.renarration_transformation_contents.text_contents.insert_next_xpath[i_count] = null;
          }

          //insert after operations
          writing_mode.controller.insertBeforeAfterOperations(writing_mode.view.variables.current_chosen_target_html_node, i_count, "after");
          writing_mode.view.variables.renarration_transformation_contents.text_contents.insert_value[i_count] = "after";

        }

        //media type of the renarration transformation
        let imageMediaType = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#media-radiobutton-image");
        let textMediaType = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#media-radiobutton-text");
        let audioMediaType = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#media-radiobutton-audio");
        let videoMediaType = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#media-radiobutton-video");

        let imageMediaTypeIsChecked = null;
        let textMediaTypeIsChecked = null;
        let audioMediaTypeIsChecked = null;
        let videoMediaTypeIsChecked = null;
        try {
          if (imageMediaType) {
            imageMediaTypeIsChecked = imageMediaType.checked;
          }
          if (textMediaType) {
            textMediaTypeIsChecked = textMediaType.checked;
          }
          if (audioMediaType) {
            audioMediaTypeIsChecked = audioMediaType.checked;
          }
          if (videoMediaType) {
            videoMediaTypeIsChecked = videoMediaType.checked;
          }
        } catch (err) {
          console.log(err);
        }
        if (textMediaTypeIsChecked) {
          writing_mode.view.variables.renarration_transformation_contents.text_contents.media_type[i_count] = "Text";
        } else if (imageMediaTypeIsChecked) {
          writing_mode.view.variables.renarration_transformation_contents.text_contents.media_type[i_count] = "Image";
        } else if (audioMediaTypeIsChecked) {
          writing_mode.view.variables.renarration_transformation_contents.text_contents.media_type[i_count] = "Audio";
        } else if (videoMediaTypeIsChecked) {
          writing_mode.view.variables.renarration_transformation_contents.text_contents.media_type[i_count] = "Video";
        }

        //incrementing the value of the text_contents counter
        writing_mode.view.variables.renarration_transformation_contents.text_contents.count++;
      }
    },
    insertBeforeAfterOperations: function(node, i_count, beforeaftervalue) {
      if (beforeaftervalue === "after") {
        let refCurrentChosenNode = node;
        refCurrentChosenNode.removeAttribute("rn-class", "rn-chosen-html-node");
        let refCurrentChosenNodeOuterHTML = refCurrentChosenNode.outerHTML;
        refCurrentChosenNode.outerHTML = refCurrentChosenNodeOuterHTML +
          writing_mode.view.variables.renarration_transformation_contents.text_contents.new[i_count];
      } else if (beforeaftervalue === "before") {
        let refCurrentChosenNode = node;
        refCurrentChosenNode.removeAttribute("rn-class", "rn-chosen-html-node");
        let refCurrentChosenNodeOuterHTML = refCurrentChosenNode.outerHTML;
        refCurrentChosenNode.outerHTML =
          writing_mode.view.variables.renarration_transformation_contents.text_contents.new[i_count] + refCurrentChosenNodeOuterHTML;
      } else {
        console.log("error in writing_mode.controller.insertBeforeAfterOperations functions ");
      }
    },
    replace_remove_variable_operations: function(replace_or_remove) {
      let i_count = writing_mode.view.variables.renarration_transformation_contents.text_contents.count;

      //xpath of the content to an array
      writing_mode.view.variables.renarration_transformation_contents.text_contents.xpath[i_count] = writing_mode.controller.xpath_generator_renarration_content(writing_mode.view.variables.current_chosen_target_html_node);

      //saving the action value to an array
      writing_mode.view.variables.renarration_transformation_contents.text_contents.action[i_count] = replace_or_remove;

      //saving the new content value to an array
      if (replace_or_remove === "replace") {
        writing_mode.view.variables.renarration_transformation_contents.text_contents.new[i_count] = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#rn_transformation_text").value;
      } else if (replace_or_remove === "remove") {
        writing_mode.view.variables.renarration_transformation_contents.text_contents.new[i_count] = "";
      }


      //saving the date value to an array
      let renarration_transformation_date = new Date();
      writing_mode.view.variables.renarration_transformation_contents.text_contents.date[i_count] = renarration_transformation_date.toISOString();

      //saving the language value to an array
      if (replace_or_remove === "replace") {

        writing_mode.view.variables.renarration_transformation_contents.text_contents.language[i_count] = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#rn_transformation_language").value;
      } else if (replace_or_remove === "remove") {
        writing_mode.view.variables.renarration_transformation_contents.text_contents.language[i_count] = "";
      }

      //saving the old content value to an array
      writing_mode.view.variables.renarration_transformation_contents.text_contents.old[i_count] = writing_mode.view.variables.current_chosen_target_html_node.innerText;

      //saving the old html node element value to an array
      writing_mode.view.variables.renarration_transformation_contents.text_contents.old_html_node[i_count] = writing_mode.view.variables.last_chosen_target_html_node.outerHTML;



      //changing the body of html document with new content value from an array
      if (replace_or_remove === "replace") {
        writing_mode.view.variables.current_chosen_target_html_node.innerText = writing_mode.view.variables.renarration_transformation_contents.text_contents.new[i_count];
      } else if (replace_or_remove === "remove") {
        writing_mode.view.variables.current_chosen_target_html_node.setAttribute("rn-class-display", "rn-chosen-html-node-display-none");
      }




      console.log("---Starts---", writing_mode.view.variables.renarration_transformation_contents.text_contents.count);
      console.log("old_html_node", writing_mode.view.variables.renarration_transformation_contents.text_contents.old_html_node[i_count]);
      console.log("old", writing_mode.view.variables.renarration_transformation_contents.text_contents.old[i_count]);
      console.log("new", writing_mode.view.variables.renarration_transformation_contents.text_contents.new[i_count]);
      console.log("xpath", writing_mode.view.variables.renarration_transformation_contents.text_contents.xpath[i_count]);
      console.log("action", writing_mode.view.variables.renarration_transformation_contents.text_contents.action[i_count]);
      console.log("language", writing_mode.view.variables.renarration_transformation_contents.text_contents.language[i_count]);


      console.log("last", writing_mode.view.variables.last_chosen_target_html_node);
      console.log("current", writing_mode.view.variables.current_chosen_target_html_node);

      writing_mode.view.variables.renarration_transformation_contents.text_contents.count++;
    },
    get_motivation_value_of_renarration: function(e) {
      let motivation_value = {
        translation: false,
        elaboration: false,
        simplification: false,
        alteration: false,
        correction: false
      };

      motivation_value.translation = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#motivation-radiobutton-translation").checked;
      motivation_value.elaboration = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#motivation-radiobutton-elaboration").checked;
      motivation_value.simplification = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#motivation-radiobutton-simplification").checked;
      motivation_value.alteration = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#motivation-radiobutton-alteration").checked;
      motivation_value.correction = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-writing").querySelector("#motivation-radiobutton-correction").checked;

      writing_mode.view.variables.renarration_information.motivation = motivation_value;
    },
    creation_time_of_renarration: function() {
      //saving the date value to an array
      let renarration_creation_date = new Date();
      writing_mode.view.variables.renarration_information.creation_time = renarration_creation_date.toISOString();

      //for the renarration_unique_id_generator function it is converted to toString(36) base
      writing_mode.view.variables.renarration_information.creation_time_toString36 = renarration_creation_date.getTime().toString(36);
    },
    source_url_of_renarration: function() {
      writing_mode.view.variables.renarration_information.source_url = window.location.href;
    },
    renarration_unique_id_generator: function() {
      let time_to_string36 = writing_mode.view.variables.renarration_information.creation_time_toString36;
      let random_no_to_string36_1 = Math.round((Math.random() * 36 ** 8)).toString(36);
      let random_no_to_string36_2 = Math.round((Math.random() * 36 ** 8)).toString(36);
      let unique_id_string36 = time_to_string36 + "-" + random_no_to_string36_1 + "-" + random_no_to_string36_2;
      writing_mode.view.variables.renarration_information.unique_id = unique_id_string36;
    },
    track_session_and_pass_values_from_main_mode_to_write_mode_variables: function() {
      writing_mode.view.variables.renarration_information.name = main_mode.variables.renarration_information.name;
      writing_mode.view.variables.renarration_information.webId = main_mode.variables.renarration_information.webId;
      writing_mode.view.variables.renarration_information.role = main_mode.variables.renarration_information.role;
      writing_mode.view.variables.renarration_information.email = main_mode.variables.renarration_information.email;
      writing_mode.view.variables.renarration_information.profile_card = main_mode.variables.renarration_information.profile_card;
      writing_mode.view.variables.renarration_information.webId_label = main_mode.variables.renarration_information.webId_label;
      writing_mode.view.variables.renarration_information.webId = main_mode.variables.renarration_information.webId;
      writing_mode.view.variables.renarration_information.idp = main_mode.variables.renarration_information.idp;
    },
    create_the_renarration: function(e) {

      writing_mode.controller.track_session_and_pass_values_from_main_mode_to_write_mode_variables();
      writing_mode.controller.get_motivation_value_of_renarration(e);
      writing_mode.controller.source_url_of_renarration();
      writing_mode.controller.creation_time_of_renarration();
      writing_mode.controller.renarration_unique_id_generator();

      writing_mode.controller.renarration_turtle_operations();


    },
    renarration_turtle_operations: function() {
      let renarration_unique_id = writing_mode.view.variables.renarration_information.unique_id;
      let renarration_information_send_object = writing_mode.view.variables.renarration_information;

      let renarration_transformation_send_object = writing_mode.view.variables.renarration_transformation_contents;

      console.log(renarration_information_send_object, renarration_transformation_send_object);

      main_mode.model.update_renarration_to_renarrators_and_rnex_pod(renarration_unique_id, renarration_information_send_object, renarration_transformation_send_object);
    },
    xpath_generator_renarration_content: function(element) {
      if (element === document.body)
        return '/html[1]/' + element.tagName.toLowerCase() + '[1]';

      var ix = 0;
      var siblings = element.parentNode.childNodes;

      for (var i = 0; i < siblings.length; i++) {
        var sibling = siblings[i];
        if (sibling === element)
          return writing_mode.controller.xpath_generator_renarration_content(element.parentNode) + '/' + element.tagName.toLowerCase() + '[' + (ix + 1) + ']';
        if (sibling.nodeType === 1 && sibling.tagName === element.tagName)
          ix++;
      }
    }
  }
};

account_mode = {
  variables: {
    renarration_information: {
      profile_card: null,
      webId: null,
      webId_label: null,
      idp: null,
      role: null,
      email: null,
      name: null,
      picture: null,
      friends: []
    },
    clicked_old_table_row: null,
    clicked_current_table_row: null
  },
  view: {
    main: function() {
      let rn_main_mode_profile = document.createElement("div");
      rn_main_mode_profile.setAttribute("class", "rn-main-mode-account");

      let profile_jpg = chrome.runtime.getURL("img/defaultprofile.svg");
      let rn_profile_picture = `<input type="image" src=${profile_jpg} class="rn-general-picture"></input>`;

      let rn_mode_header = `<div class="rn-current-mode-content-box"><div><p>My Profile</p></div></div>`;
      let rn_profile_mode_renarrations_header = `<div class="rn-general-content-box"><div class="rn-general-header"><h4>Renarrations</h4></div></div>`;

      let rn_profile_mode_renarrations_filter = `<div class="rn-general-content-box"><div class=""><table class="rn-general-searchbox-table"><tbody><tr><td><div class="rn-general-searchbox-header"><p>Filter</p></div></td><td><input type="text" class="rn-general-searchbox-textbox" placeholder="Type here..."></td><td><input type="button" class="rn-general-searchbox-button" value="Submit"></td></tr></tbody></table></div></div>`;

      let rn_profile_mode_renarrations_buttons = `<div class="rn-general-content-box">
        <input type="button" class="rn-general-searchbox-button-second-type" id="back-and-return-to-the-old-renarration" value="Back">
        <input type="button" class="rn-general-searchbox-button-second-type" id="go-and-apply-the-new-renarration" value="Go">
      </div>`;

      let rn_account_mode_login_logout_buttons = `<div class="rn-general-content-box">
        <input type="button" class="rn-general-searchbox-button-second-type" id="logout" value="Logout">
      </div>`;

      let rn_account_mode_personal_items_general_html = `<div id="account-profile"class="rn-general-content-box">` + `</div>`;

      rn_main_mode_profile.innerHTML = rn_mode_header + rn_profile_picture + rn_account_mode_personal_items_general_html + rn_profile_mode_renarrations_header + main_mode.controller.rn_general_create_the_renarration_general_table_outside_html("account_mode") + rn_account_mode_login_logout_buttons;

      account_mode.model.get_profile();
      account_mode.model.get_renarration_items();

      return rn_main_mode_profile;
    }
  },
  model: {
    main: function() {

    },
    get_friends: function() {

    },
    get_profile: function() {
      chrome_storage.rn_get_the_current_session_renarrators_info_object().then(renarrators_profile_info => {
        let rn_profile_mode_personal_items_html = "";

        let rn_profile_mode_personal_item_label = "label";
        let rn_profile_mode_personal_item_value = "value";
        let label_id_array = ["label-name", "label-webid", "label-role", "label-email"];
        let value_id_array = ["value-name", "value-webid", "value-role", "value-email"];
        let rn_profile_mode_personal_item_label_array = ["Name", "WebId", "Role", "Email"];
        let rn_profile_mode_personal_item_value_array = [renarrators_profile_info.name, renarrators_profile_info.webId, renarrators_profile_info.role, renarrators_profile_info.email];

        for (let i = 0; i < rn_profile_mode_personal_item_value_array.length; i++) {
          if (rn_profile_mode_personal_item_value_array[i] !== null || rn_profile_mode_personal_item_value_array[i] !== undefined) {

            rn_profile_mode_personal_items_html += `<div class="rn-general-content-label-value-group"><div class="rn-general-content-label-name">


            <p id=${label_id_array[i]} >${rn_profile_mode_personal_item_value_array[i]!==null ? rn_profile_mode_personal_item_label_array[i] : rn_profile_mode_personal_item_label}</p>

            </div><div class="rn-general-content-value-name">

            ${i===1 ? `<a href="${rn_profile_mode_personal_item_value_array[i]}" target="_blank" class="general-clickable-url"><p class="slide-to-right-hover" id=${value_id_array[i]}>${rn_profile_mode_personal_item_value_array[i]!==null ? rn_profile_mode_personal_item_value_array[i] : rn_profile_mode_personal_item_value}</p></a>` :  `<p class="slide-to-right-hover" id=${value_id_array[i]}>${rn_profile_mode_personal_item_value_array[i]!==null ? rn_profile_mode_personal_item_value_array[i] : rn_profile_mode_personal_item_value}</p>`    }

            </div></div>`;

          }


        }

        let rn_account_mode_personal_items_general_html = `<div id="account-profile"class="rn-general-content-box">` + rn_profile_mode_personal_items_html + `</div>`;

        document.querySelector("#RNEx").shadowRoot.querySelector("#account-profile").innerHTML = rn_profile_mode_personal_items_html;

        if (renarrators_profile_info.picture) {
          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-general-picture").setAttribute("src", renarrators_profile_info.picture);
        }

        return rn_account_mode_personal_items_general_html;

      }).catch(e => {
        main_mode.model.track_session();
        console.log("some error occured getting the rn_get_the_current_session_renarrators_info_object from  the chrome storage", e);
      });
    },
    get_renarration_items: function() {

      chrome_storage.rn_get_the_account_mode_renarrations_and_the_table().then(renarrations_and_the_table => {
        main_mode.controller.rn_general_create_the_renarration_table_html(renarrations_and_the_table, "account_mode");
      }).catch(e => {

        setTimeout(()=>{   document.querySelector("#RNEx").shadowRoot.querySelector("#loading-animation-table").classList.add("table-loading-animation-display-none");
      }, 2000);


        console.log("some error occured getting the rn_get_the_account_mode_renarration_documents_array from  the chrome storage", e);
      });

    }
  },
  controller: {
    main: function() {
      chrome_storage.addListener(account_mode.controller.storage_listener.main);

      document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-account").addEventListener("click", account_mode.controller.account_mode_remove_and_choose);

    },
    account_mode_remove_and_choose: function(e) {
      console.log("event:", e.target);

      if (e.target.closest(".rn-general-table") && e.target.closest(".rn-general-table-tbody") && e.target.closest("tr")) {

        if (account_mode.variables.clicked_old_table_row !== null) {
          account_mode.variables.clicked_old_table_row.setAttribute("class", "");
        }
        account_mode.variables.clicked_current_table_row = e.target.closest("tr");
        account_mode.variables.clicked_current_table_row.setAttribute("class", "rn-general-table-tbody-tr-clicked");
        account_mode.variables.clicked_old_table_row = account_mode.variables.clicked_current_table_row;
      }

      if (e.target.closest(".rn-general-table") && e.target.closest(".rn-general-table-tfooter")) {
        main_mode.controller.rn_general_create_the_renarration_table_html_footer_actions(e.target, "account_mode");
      }

      if (e.target.className === "rn-general-searchbox-button-second-type" && e.target.id === "login") {
        main_mode.model.login();

      } else if (e.target.className === "rn-general-searchbox-button-second-type" && e.target.id === "logout") {
        main_mode.model.logout();
        main_mode.view.refresh_the_page();
      } else if (e.target.id === "go-and-apply-the-new-renarration") {
        account_mode.controller.apply_the_renarration_to_the_body.main();
      } else if (e.target.id === "delete-the-chosen-renarration") {
        account_mode.controller.delete_the_renarration();
      }
    },
    delete_the_renarration: function() {
      if (account_mode.variables.clicked_current_table_row !== null) {

        let html_attribute_get_renarration_unique_id = account_mode.variables.clicked_current_table_row.getAttribute("rnuniqueid");

        console.log("html_attribute_get_renarration_id ", html_attribute_get_renarration_unique_id);

        main_mode.model.delete_the_renarration(html_attribute_get_renarration_unique_id);

      }

    },
    apply_the_renarration_to_the_body: {
      main: function() {
        chrome_storage.rn_get_the_account_mode_renarrations_and_the_table().then(renarrations_and_the_table => {
          let renarration_documents_array = renarrations_and_the_table.desired_renarrations;
          account_mode.controller.apply_the_renarration_to_the_body.url_unique_id_check(renarration_documents_array);
        }).catch(e => {

          console.log("some error occured getting the rn_get_the_account_mode_renarration_documents_array from  the chrome storage inside the account_mode.apply_the_renarration_to_the_body.main", e);
        });
      },
      html_attribute_get_renarration_id: function(renarration_documents_array) {

        if (account_mode.variables.clicked_current_table_row !== null) {

          let html_attribute_get_renarration_unique_id = account_mode.variables.clicked_current_table_row.getAttribute("rnuniqueid");

          console.log("html_attribute_get_renarration_id ", html_attribute_get_renarration_unique_id);

          console.log("clicked_current_table_row", account_mode.variables.clicked_current_table_row);

          for (let i = 0; i < renarration_documents_array.length; i++) {
            if (html_attribute_get_renarration_unique_id === renarration_documents_array[i].targetFileSourceUrlandUniqueId) {
              console.log("return main_mode.variables.renarration_array_from_the_pod[i]", renarration_documents_array[i]);
              return renarration_documents_array[i];
            }
          }

          console.log("Inside account_mode.controller.apply_the_renarration_to_the_body.html_attribute_get_renarration_id couldn't find the chosen document");
          return null;

        } else {

          console.log("Inside account_mode.controller.apply_the_renarration_to_the_body.html_attribute_get_renarration_id a problem has occurred");

          return null;
        }

      },
      url_unique_id_check: function(renarration_documents_array) {
        let current_location_href = window.location.href;
        let chosen_renarration_document_from_the_array = account_mode.controller.apply_the_renarration_to_the_body.html_attribute_get_renarration_id(renarration_documents_array);

        if (chosen_renarration_document_from_the_array !== null) {

          if (chosen_renarration_document_from_the_array.onSourceDocument === current_location_href) {
            if (document.body.getAttribute("rnex-renarration-status") === "renarration-is-applied") {
              account_mode.controller.apply_the_renarration_to_the_body.setting_The_chrome_storage_for_the_clicked_renarration_and_redirecting_the_page(chosen_renarration_document_from_the_array);

            } else {
              main_mode.controller.renarration_xpath_evaluation(chosen_renarration_document_from_the_array);
            }

          } else {
            account_mode.controller.apply_the_renarration_to_the_body.setting_The_chrome_storage_for_the_clicked_renarration_and_redirecting_the_page(chosen_renarration_document_from_the_array);
          }

        } else {
          console.log("chosen_renarration_document_from_the_array is null, in other words account_mode.controller.apply_the_renarration_to_the_body.html_attribute_get_renarration_id() returns null value");
        }

      },
      setting_The_chrome_storage_for_the_clicked_renarration_and_redirecting_the_page: function(chosen_renarration_document_from_the_array) {
        chrome_storage.rn_account_mode_clicked_row_before_the_go_button(chosen_renarration_document_from_the_array).then((s) => {
          console.log("1", s);
          return chrome_storage.rn_account_mode_clicked_row_unique_id_before_the_go_button(chosen_renarration_document_from_the_array.targetFileSourceUrlandUniqueId);

        }).then((s) => {
          console.log("2", s);
          window.location.href = chosen_renarration_document_from_the_array.onSourceDocument;
        });
      }
    },
    table_footer_actions: {
      main: function(e_target) {
        let current_page_string = document.querySelector("#RNEx").shadowRoot.querySelector("#table-current-page").value;
        let current_page = parseInt(current_page_string);
        if (e_target.id === "go-to-previous-table-contents") {
          console.log("previous", e_target);

          let page_val;
          if (current_page > 1) {
            page_val = (--current_page).toString();

          } else {
            page_val = "1";
          }
          document.querySelector("#RNEx").shadowRoot.querySelector("#table-current-page").value = page_val;

          let footer_action = {
            action: "previous",
            go_page: null,
            current_page: current_page
          };
          main_mode.model.get_the_renarrations_from_the_renarrators_pod(footer_action);

          document.querySelector("#RNEx").shadowRoot.querySelector("#account-table").innerHTML = "";
          document.querySelector("#RNEx").shadowRoot.querySelector("#loading-animation-table").classList.remove("table-loading-animation-display-none");
        } else if (e_target.id === "go-to-next-table-contents") {
          console.log("next", e_target);


          let page_val;
          let number_of_last_page = parseInt(document.querySelector("#RNEx").shadowRoot.querySelector("#table-total-page").value);
          if (current_page < number_of_last_page) {
            page_val = (++current_page).toString();

          } else {
            page_val = number_of_last_page.toString();
          }

          document.querySelector("#RNEx").shadowRoot.querySelector("#table-current-page").value = page_val;

          let footer_action = {
            action: "next",
            go_page: null,
            current_page: current_page
          };
          main_mode.model.get_the_renarrations_from_the_renarrators_pod(footer_action);

          document.querySelector("#RNEx").shadowRoot.querySelector("#account-table").innerHTML = "";
          document.querySelector("#RNEx").shadowRoot.querySelector("#loading-animation-table").classList.remove("table-loading-animation-display-none");
        }


      }
    },
    remove_inside_rn_main_mode_account: function() {
      if (document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-wrap").firstChild !== null) {
        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-account").innerHTML = "";
      }
    },
    choose_inside_rn_main_mode_account: function(rn_read_mode_selection_addition) {
      if (rn_read_mode_selection_addition !== null) {
        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-account").innerHTML = rn_read_mode_selection_addition.innerHTML;
      }
    },
    storage_listener: {
      main: function(changes, namespace) {
        console.log("changes", changes);
        for (let key in changes) {
          let storageChange = changes[key];
          if (key == "account_mode") {
            account_mode.model.get_renarration_items();
          }
        }

      }
    }
  }
};

profile_mode = {
  variables: {
    clicked_old_table_row: null,
    clicked_current_table_row: null
  },
  view: {
    main: function() {
      let rn_main_mode_profile = document.createElement("div");
      rn_main_mode_profile.setAttribute("class", "rn-main-mode-profile");

      let profile_jpg = chrome.runtime.getURL("img/defaultprofile.svg");
      let rn_profile_picture = `<input type="image" src=${profile_jpg} class="rn-general-picture"></input>`;

      let rn_profile_mode_follow_button = `<div class="rn-follow-button-box"><input type="button" class="rn-follow-button" id="follow-the-profile" value="Follow"></div>`;

      let rn_mode_header = `<div class="rn-current-mode-content-box"><div><p>Visited Profile</p></div></div>`;

      let rn_profile_mode_renarrations_header = `<div class="rn-general-content-box"><div class="rn-general-header"><h4>Renarrations</h4></div></div>`;

      let rn_profile_mode_renarrations_filter = `<div class="rn-general-content-box"><div class=""><table class="rn-general-searchbox-table"><tbody><tr><td><div class="rn-general-searchbox-header"><p>Filter</p></div></td><td><input type="text" class="rn-general-searchbox-textbox" placeholder="Type here..."></td><td><input type="button" class="rn-general-searchbox-button" value="Submit"></td></tr></tbody></table></div></div>`;


      let rn_profile_mode_personal_items_general_html = `<div id="profile-profile"class="rn-general-content-box">` + `</div>`;

      rn_main_mode_profile.innerHTML = rn_mode_header + rn_profile_picture + rn_profile_mode_follow_button + rn_profile_mode_personal_items_general_html + rn_profile_mode_renarrations_header + main_mode.controller.rn_general_create_the_renarration_general_table_outside_html("profile_mode");

      profile_mode.model.get_profile_items();
      profile_mode.model.get_renarration_items();

      return rn_main_mode_profile;
    }
  },
  model: {
    main: function() {

    },
    get_renarration_items: function() {

      let rn_profile_mode_renarrations_items_tbody = "";

      chrome_storage.rn_get_the_profile_mode_renarrations_and_the_table().then(renarration_documents_array => {
        main_mode.controller.rn_general_create_the_renarration_table_html(renarration_documents_array, "profile_mode");

      }).catch(e => {
        console.log("some error occured getting the rn_get_the_reading_mode_renarration_documents_array from  the chrome storage", e);
      });

    },
    get_profile_items: function() {
      chrome_storage.rn_get_the_profile_mode_renarrators_info_object().then(renarrators_profile_info => {
        let rn_profile_mode_personal_items_html = "";

        let rn_profile_mode_personal_item_label = "label";
        let rn_profile_mode_personal_item_value = "value";
        let label_id_array = ["label-name", "label-webid", "label-role", "label-email"];
        let value_id_array = ["value-name", "value-webid", "value-role", "value-email"];
        let rn_profile_mode_personal_item_label_array = ["Name", "WebId", "Role", "Email"];
        let rn_profile_mode_personal_item_value_array = [renarrators_profile_info.name, renarrators_profile_info.webId, renarrators_profile_info.role, renarrators_profile_info.email];

        for (let i = 0; i < rn_profile_mode_personal_item_value_array.length; i++) {
          if (rn_profile_mode_personal_item_value_array[i] !== null || rn_profile_mode_personal_item_value_array[i] !== undefined) {

            rn_profile_mode_personal_items_html += `<div class="rn-general-content-label-value-group"><div class="rn-general-content-label-name">


            <p id=${label_id_array[i]} >${rn_profile_mode_personal_item_value_array[i]!==null ? rn_profile_mode_personal_item_label_array[i] : rn_profile_mode_personal_item_label}</p>

            </div><div class="rn-general-content-value-name">

            ${i===1 ? `<a href="${rn_profile_mode_personal_item_value_array[i]}" target="_blank" class="general-clickable-url"><p class="slide-to-right-hover" id=${value_id_array[i]}>${rn_profile_mode_personal_item_value_array[i]!==null ? rn_profile_mode_personal_item_value_array[i] : rn_profile_mode_personal_item_value}</p></a>` :  `<p class="slide-to-right-hover" id=${value_id_array[i]}>${rn_profile_mode_personal_item_value_array[i]!==null ? rn_profile_mode_personal_item_value_array[i] : rn_profile_mode_personal_item_value}</p>`    }

            </div></div>`;

          }


        }

        let rn_account_mode_personal_items_general_html = `<div id="profile-profile"class="rn-general-content-box">` + rn_profile_mode_personal_items_html + `</div>`;

        document.querySelector("#RNEx").shadowRoot.querySelector("#profile-profile").innerHTML = rn_profile_mode_personal_items_html;

        if (renarrators_profile_info.picture) {
          document.querySelector("#RNEx").shadowRoot.querySelector(".rn-general-picture").setAttribute("src", renarrators_profile_info.picture);
        }

        return rn_account_mode_personal_items_general_html;

      }).catch(e => {
        console.log("some error occured getting the rn_get_the_current_session_renarrators_info_object from  the chrome storage", e);
      });
    }
  },
  controller: {
    main: function() {

      chrome_storage.addListener(profile_mode.controller.storage_listener.main);


      document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-profile").addEventListener("click", profile_mode.controller.profile_mode_main_operation_chain);

    },
    profile_mode_main_operation_chain: function(e) {
      console.log("event:", e.target);
      profile_mode.controller.highlighting_the_clicked_renarration_row(e);

      if (e.target.closest(".rn-general-table") && e.target.closest(".rn-general-table-tfooter")) {
        main_mode.controller.rn_general_create_the_renarration_table_html_footer_actions(e.target, "profile_mode");
      }

      if (e.target.id === "go-and-apply-the-new-renarration") {

        profile_mode.controller.apply_the_renarration_to_the_body.main();

      } else if (e.target.id === "follow-the-profile") {
        profile_mode.controller.follow_operations.main();
      }

    },
    follow_operations: {
      main: function() {
        chrome_storage.rn_get_the_profile_mode_renarrators_info_object().then(
          renarrators_profile => {
            main_mode.model.follow_the_renarrator(renarrators_profile.webId);
          }
        );
      }
    },
    highlighting_the_clicked_renarration_row: function(e) {
      if (e.target.closest(".rn-general-table") && e.target.closest(".rn-general-table-tbody") && e.target.closest("tr")) {

        if (profile_mode.variables.clicked_old_table_row !== null) {
          profile_mode.variables.clicked_old_table_row.setAttribute("class", "");
        }
        profile_mode.variables.clicked_current_table_row = e.target.closest("tr");
        profile_mode.variables.clicked_current_table_row.setAttribute("class", "rn-general-table-tbody-tr-clicked");
        profile_mode.variables.clicked_old_table_row = profile_mode.variables.clicked_current_table_row;

      }
    },
    apply_the_renarration_to_the_body: {
      main: function() {

        chrome_storage.rn_get_the_profile_mode_renarrations_and_the_table().then(renarrations_and_the_table => {
          let renarration_documents_array = renarrations_and_the_table.desired_renarrations;
          profile_mode.controller.apply_the_renarration_to_the_body.url_unique_id_check(renarration_documents_array);
        }).catch(e => {
          console.log("some error occured getting the rn_get_the_profile_mode_renarrations_and_the_table from  the chrome storage inside the profile_mode.apply_the_renarration_to_the_body.main", e);
        });



      },
      url_unique_id_check: function(renarration_documents_array) {
        let current_location_href = window.location.href;
        let chosen_renarration_document_from_the_array = profile_mode.controller.apply_the_renarration_to_the_body.html_attribute_get_renarration_id(renarration_documents_array);

        if (chosen_renarration_document_from_the_array !== null) {

          if (chosen_renarration_document_from_the_array.onSourceDocument === current_location_href) {
            if (document.body.getAttribute("rnex-renarration-status") === "renarration-is-applied") {
              profile_mode.controller.apply_the_renarration_to_the_body.setting_The_chrome_storage_for_the_clicked_renarration_and_redirecting_the_page(chosen_renarration_document_from_the_array);

            } else {
              main_mode.controller.renarration_xpath_evaluation(chosen_renarration_document_from_the_array);
            }

          } else {
            profile_mode.controller.apply_the_renarration_to_the_body.setting_The_chrome_storage_for_the_clicked_renarration_and_redirecting_the_page(chosen_renarration_document_from_the_array);
          }

        } else {
          console.log("chosen_renarration_document_from_the_array is null, in other words profile_mode.controller.apply_the_renarration_to_the_body.html_attribute_get_renarration_id() returns null value");
        }

      },
      html_attribute_get_renarration_id: function(renarration_documents_array) {

        if (profile_mode.variables.clicked_current_table_row !== null) {


          let html_attribute_get_renarration_unique_id = profile_mode.variables.clicked_current_table_row.getAttribute("rnuniqueid");

          console.log("html_attribute_get_renarration_id ", html_attribute_get_renarration_unique_id);

          console.log("clicked_current_table_row", profile_mode.variables.clicked_current_table_row);




          for (let i = 0; i < renarration_documents_array.length; i++) {
            if (html_attribute_get_renarration_unique_id === renarration_documents_array[i].targetFileSourceUrlandUniqueId) {
              console.log("return main_mode.variables.renarration_array_from_the_pod[i]", renarration_documents_array[i]);
              return renarration_documents_array[i];
            }
          }

          console.log("Inside profile_mode.controller.apply_the_renarration_to_the_body.html_attribute_get_renarration_id couldn't find the chosen document");
          return null;

        } else {

          console.log("Inside profile_mode.controller.apply_the_renarration_to_the_body.html_attribute_get_renarration_id a problem has occurred");

          return null;
        }

      },
      setting_The_chrome_storage_for_the_clicked_renarration_and_redirecting_the_page: function(chosen_renarration_document_from_the_array) {
        chrome_storage.rn_profile_mode_clicked_row_before_the_go_button(chosen_renarration_document_from_the_array).then((s) => {
          console.log("1", s);
          return chrome_storage.rn_profile_mode_clicked_row_unique_id_before_the_go_button(chosen_renarration_document_from_the_array.targetFileSourceUrlandUniqueId);

        }).then((s) => {
          console.log("2", s);
          window.location.href = chosen_renarration_document_from_the_array.onSourceDocument;
        });
      }
    },
    storage_listener: {
      main: function(changes, namespace) {
        console.log("changes", changes);
        for (let key in changes) {
          let storageChange = changes[key];
          if (key == "profile_mode") {
            profile_mode.model.get_renarration_items();
            profile_mode.model.get_profile_items();
          }
        }

      }
    }
  }
};

response_mode = {
  variables: {},
  view: {
    main: function(clicked_item_nodes_array) {

      let rn_general_response_box = ``;

      let rn_reading_mode_renarrations_items_func = () => {
        let rn_response_webId = "";
        let rn_response_content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam vel dolor sed lectus fermentum vestibulum ut sit amet dui. Integer rhoncus viverra turpis eu aliquam. Nulla varius auctor dui in tempus. Pellentesque libero nibh, ultricies eu lectus a, eleifend ultrices nisl. Maecenas maximus efficitur consequat. Nunc at nisi ut augue mattis porta. Fusce pharetra vitae nisl ut iaculis. Ut volutpat gravida ligula egestas tempus. Quisque eget ultrices nibh.";

        let rn_selected_class = "rn-rate-icon-selected";
        let rn_response_icons = ``;
        let rn_response_rate_value = 3;
        for (let i = 1; i <= 5; i++) {
          if (i <= rn_response_rate_value) {
            rn_response_icons += `<i class="rn-rate-icon ${rn_selected_class}" id="rn-icon-${i}"></i>`;
          } else {
            rn_response_icons += `<i class="rn-rate-icon" id="rn-icon-${i}"></i>`;
          }
        }

        let rn_response_icon_group = `<div class="rn-rate-icon-group">${rn_response_icons}</div>`;

        rn_general_response_box = `<div class="rn-general-response-box"> <div class="rn-general-response-box-header"> <p class="rn-general-response-box-header-overflow">${rn_response_webId}</p>${rn_response_icon_group}<div class="rn-updown-icon-group"> <i class="rn-icon-down"></i> <i class="rn-icon-up"></i> </div></div><div class="rn-general-response-box-body"> <p>${rn_response_content}</p></div></div>`;
      };
      rn_reading_mode_renarrations_items_func();

      let rn_mode_header = `<div class="rn-current-mode-content-box"><div><p>Response Mode</p></div></div>`;

      let rn_general_response_rate_box =
        `<div class="rn-general-response-rate-box">
         <div class="rn-general-response-rate-box-header">
           <p class="rn-general-response-rate-box-header-title">Rate</p>
            <p class="rn-general-response-rate-box-header-overflow rn-general-response-rate-box-header-dark-color-text" >Amount</p>
            <p class="rn-general-response-rate-box-header-overflow rn-general-response-rate-box-header-light-color-text" id="number_of_created_rates"></p>
            <p class="rn-general-response-rate-box-header-overflow rn-general-response-rate-box-header-dark-color-text">Mean</p>
            <p class="rn-general-response-rate-box-header-overflow rn-general-response-rate-box-header-light-color-text" id="rate_mean_value"></p>
            <div class="rn-updown-rate-icon-group"> <i class="rn-rate-icon-down"></i> <i class="rn-rate-icon-up"></i> </div>
         </div>
         <div class="rn-general-response-rate-box-body">
          <div class="rn-general-response-rate-box-body-text-group">
         <p class="rn-general-response-rate-box-header-overflow rn-general-response-rate-box-body-dark-color-text"></p>
         <p class="rn-general-response-rate-box-header-overflow rn-general-response-rate-box-body-light-color-text"></p>
          </div>
         </div>
      </div>`;

      let rn_general_response_container = `<div class="rn-general-response-container"><div class="rn-general-response-container-header"><h4>Response</h4></div>` + rn_general_response_rate_box + `<div class="rn-general-response-container-body">` + `<div class="rn-general-response-container-body-comments"></div>` + `</div></div>`;


      let rn_response_rating_container = `<div class="rn-general-content-box">  <div class="rn-rate-icon-group rn-rate-icon-normal-size " id="rn-rate-icon-group-enabled"> <i class="rn-rate-icon enabled" id="rn-icon-1"></i> <i class="rn-rate-icon enabled" id="rn-icon-2"></i> <i class="rn-rate-icon enabled" id="rn-icon-3"></i> <i class="rn-rate-icon enabled" id="rn-icon-4"></i> <i class="rn-rate-icon enabled" id="rn-icon-5"></i> </div><input type="button" class="rn-general-searchbox-button" id="save_the_rate" value="Rate"> </div>`;

      let rn_response_writing_container = `<div class="rn-general-content-box"> <div class="rn-general-header"> <h4>Response</h4> </div><textarea id="rn_transformation_text" class="rn-general-textarea"></textarea> <input type="button" class="rn-general-searchbox-button" id="save_the_comment" value="Comment"></div>`;

      let rn_main_mode_response = document.createElement("div");
      rn_main_mode_response.setAttribute("class", "rn-main-mode-response");

      rn_main_mode_response.innerHTML = rn_mode_header + rn_general_response_container + rn_response_rating_container + rn_response_writing_container;

      response_mode.model.get_response_items();

      return rn_main_mode_response;
    }
  },
  model: {
    get_response_items: function() {

      let rn_response_mode_response_items_body = "";
      let rn_response_mode_response_rate_items_body = "";
      let rn_response_mode_response_rate_items_header_number_of_created_rates = 0;
      let rn_response_mode_response_rate_items_header_total_rates = 0;
      let rn_response_mode_response_rate_items_header_mean = 0;
      chrome_storage.rn_get_the_response_mode_response_documents_array().then(response_documents_not_sorted_array => {
        let response_documents_array = response_documents_not_sorted_array.slice();
        response_documents_array.sort(function(a, b) {
          return new Date(b.respondedAt) - new Date(a.respondedAt);
        });

        for (let i = 0; i < response_documents_array.length; i++) {
          if (response_documents_array[i].type === "Comment") {
            let rn_response_webId = response_documents_array[i].isCreatedBy;
            let rn_response_content = response_documents_array[i].hasCommentValue;
            let rn_response_respondedAt = response_documents_array[i].respondedAt;


            let rn_general_response_box = `<div class="rn-general-response-box"> <div class="rn-general-response-box-header"> <p class="rn-general-response-box-header-overflow">${rn_response_webId}</p><p class="rn-general-response-box-header-overflow rn-general-response-rate-box-body-dark-color-time-text">${rn_response_respondedAt}</p><div class="rn-updown-icon-group"> <i class="rn-icon-down"></i> <i class="rn-icon-up"></i> </div></div><div class="rn-general-response-box-body"> <p>${rn_response_content}</p></div></div>`;

            rn_response_mode_response_items_body += rn_general_response_box;
          } else if (response_documents_array[i].type === "Rate") {
            let rn_response_webId = response_documents_array[i].isCreatedBy;
            let rn_response_rate_content = response_documents_array[i].hasRateValue;


            let rn_general_response_rate_box = `
            <div class="rn-general-response-rate-box-body-text-group">
           <p class="rn-general-response-rate-box-header-overflow rn-general-response-rate-box-body-dark-color-text">${rn_response_webId}</p>
           <p class="rn-general-response-rate-box-header-overflow rn-general-response-rate-box-body-light-color-text">${rn_response_rate_content}/5</p>
            </div>`;

            let isNumeric = (str) => {
              if (typeof str != "string") return false;
              return !isNaN(str) && !isNaN(parseFloat(str));
            };
            rn_response_mode_response_rate_items_header_number_of_created_rates++;
            if (isNumeric(rn_response_rate_content)) {
              rn_response_mode_response_rate_items_header_total_rates += parseInt(rn_response_rate_content);
            }

            rn_response_mode_response_rate_items_body += rn_general_response_rate_box;
          }
        }
        if (rn_response_mode_response_rate_items_header_number_of_created_rates !== 0) {
          rn_response_mode_response_rate_items_header_mean = Number(rn_response_mode_response_rate_items_header_total_rates / rn_response_mode_response_rate_items_header_number_of_created_rates).toFixed(2);
        }


        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-general-response-container-body-comments").innerHTML = rn_response_mode_response_items_body;
        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-general-response-rate-box-body").innerHTML = rn_response_mode_response_rate_items_body;

        document.querySelector("#RNEx").shadowRoot.querySelector("#number_of_created_rates").innerHTML = rn_response_mode_response_rate_items_header_number_of_created_rates;
        document.querySelector("#RNEx").shadowRoot.querySelector("#rate_mean_value").innerHTML = rn_response_mode_response_rate_items_header_mean;

      }).catch(e => {
        console.log("some error occured getting the rn_get_the_response_mode_response_documents_array from  the chrome storage", e);
      });

    }
  },
  controller: {
    main: function() {
      chrome_storage.addListener(response_mode.controller.storage_listener.main);

      document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-response").addEventListener("click", response_mode.controller.main_operation_chain);

      document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-response").querySelector("#rn-rate-icon-group-enabled").addEventListener("mouseleave", response_mode.controller.main_rate_chain.complete_mouse_leave);

      document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-response").querySelector("#rn-rate-icon-group-enabled").querySelectorAll(".rn-rate-icon").forEach(response_mode.controller.main_rate_chain.click);

      document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-response").querySelector("#rn-rate-icon-group-enabled").querySelectorAll(".rn-rate-icon").forEach(response_mode.controller.main_rate_chain.mouseenter);

      document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-response").querySelector("#rn-rate-icon-group-enabled").querySelectorAll(".rn-rate-icon").forEach(response_mode.controller.main_rate_chain.mouseleave);

    },
    main_operation_chain: function(e) {
      console.log("event:", e.target);

      if (e.target.parentNode.className === "rn-updown-icon-group") {

        response_mode.controller.toggle_the_response_box(e);

      }

      if (e.target.id === "save_the_rate") {
        response_mode.controller.send_rate_value();
      }

      if (e.target.id === "save_the_comment") {
        response_mode.controller.send_comment_value();
      }

    },
    toggle_the_response_box: function(e) {

      e.target.closest(".rn-general-response-box").classList.toggle("rn-response-display-none");
      e.target.closest(".rn-general-response-box").querySelector(".rn-general-response-box-body").classList.toggle("rn-response-display-slide-to-none");

    },
    send_comment_value: function() {
      let comment_information = {};
      let creation_time = response_mode.controller.creation_time_of_response();
      comment_information.creation_time = creation_time.ISOString;
      comment_information.unique_id = response_mode.controller.response_unique_id_generator(creation_time.String36);

      let comment_text = document.querySelector("#RNEx").shadowRoot.querySelector("#rn_transformation_text").value;

      if (comment_text !== "") {
        console.log("comment_text", comment_text);
        chrome_storage.rn_response_mode_comment_text(comment_text);


        chrome_storage.rn_get_the_general_store_the_single_renarration_store_the_applied_renarration_on_the_page().then(res => {

          main_mode.model.response.create_response_comment_file_on_the_renarrators_pod(res.targetFileSourceUrlandUniqueId, comment_text, comment_information);

          document.querySelector("#RNEx").shadowRoot.querySelector("#rn_transformation_text").value = "";

        });


      }

    },
    send_rate_value: function() {

      let rate_value = 0;
      let rate_information = {};
      let creation_time = response_mode.controller.creation_time_of_response();
      rate_information.creation_time = creation_time.ISOString;
      rate_information.unique_id = response_mode.controller.response_unique_id_generator(creation_time.String36);

      document.querySelector("#RNEx").shadowRoot.querySelectorAll("#rn-rate-icon-group-enabled i").forEach(a => {

        if (a.classList.contains("rn-rate-icon-selected")) {
          rate_value++;
        }

      });

      if (rate_value > 0) {
        console.log("rate_value", rate_value);
        chrome_storage.rn_response_mode_rate_value(rate_value);

        chrome_storage.rn_get_the_general_store_the_single_renarration_store_the_applied_renarration_on_the_page().then(res => {

          main_mode.model.response.create_response_rate_file_on_the_renarrators_pod(res.targetFileSourceUrlandUniqueId, rate_value, rate_information);

        });

      }

    },
    main_rate_chain: {
      click: function(node) {
        if (node !== undefined) {
          node.addEventListener("click", (a) => {

            let target_id = a.target.id;
            if (target_id !== null) {

            }
            let icon_number = target_id[target_id.length - 1];
            for (let i = 0; i < 5; i++) {
              if (i < icon_number) {
                a.target.parentNode.querySelectorAll(".rn-rate-icon")[i].classList.add("rn-rate-icon-selected");
              } else {
                a.target.parentNode.querySelectorAll(".rn-rate-icon")[i].classList.remove("rn-rate-icon-selected");
              }

            }

            a.target.classList.add("rn-rate-icon-active");

          });
        }
      },
      mouseenter: function(node) {
        if (node !== undefined) {
          node.addEventListener("mouseenter", (a) => {
            let target_id = a.target.id;
            if (target_id !== null) {

            }
            let icon_number = target_id[target_id.length - 1];
            for (let i = 0; i < icon_number - 1; i++) {
              a.target.parentNode.querySelectorAll(".rn-rate-icon")[i].classList.add("rn-rate-icon-active");
            }

            a.target.classList.add("rn-rate-icon-active");
          });
        }
      },
      complete_mouse_leave: function(e) {

        e.target.querySelectorAll(".rn-rate-icon").forEach(node => {
          node.classList.remove("rn-rate-icon-active");
        });


      },
      mouseleave: function(node) {
        if (node !== undefined) {
          node.addEventListener("mouseleave", (a) => {


            let target_id = a.target.id;
            let icon_number = target_id[target_id.length - 1];
            for (let i = 4; icon_number <= i; i--) {
              a.target.parentNode.querySelectorAll(".rn-rate-icon")[i].classList.remove("rn-rate-icon-active");
            }
            a.target.classList.remove("rn-rate-icon-active");
          });
        }
      }
    },
    creation_time_of_response: function() {
      let date = {};
      let renarration_creation_date = new Date();
      date.ISOString = renarration_creation_date.toISOString();
      date.String36 = renarration_creation_date.getTime().toString(36);

      return date;
    },
    response_unique_id_generator: function(time_to_string36) {
      let random_no_to_string36_1 = Math.round((Math.random() * 36 ** 8)).toString(36);
      let random_no_to_string36_2 = Math.round((Math.random() * 36 ** 8)).toString(36);
      let unique_id_string36 = time_to_string36 + "-" + random_no_to_string36_1 + "-" + random_no_to_string36_2;
      return unique_id_string36;
    },
    get_the_renarrator_file_url_and_return_response_file_url_ending: function(renarrator_profile_card) {

      let regex = /(http[s]?:\/\/)?([^\/\s]+)/g;
      let val = renarrator_profile_card.match(regex);
      let response_file = "RSPCNTTO" + val[3].substr(5);


      let regex2 = /^.*?(?=\.ttl#)/g;
      let val2 = response_file.match(regex2);
      let response_file2 = val2 + ".ttl";

      let response_file_ending = "/" + val[1] + "/" + "ResponseFiles" + "/" + response_file2;


      return response_file_ending;
    },
    storage_listener: {
      main: function(changes, namespace) {
        console.log("changes", changes);
        for (let key in changes) {
          let storageChange = changes[key];
          if (key == "response_mode") {
            response_mode.model.get_response_items();
          }
        }

      }
    }
  }
};

notification_mode = {
  view: {
    main: function(clicked_item_nodes_array) {

      let rn_mode_header = `<div class="rn-current-mode-content-box"><div><p>Notification Mode</p></div></div>`;
      let rn_general_response_container = `<div class="rn-general-response-container"><div class="rn-general-response-container-header"><h4>Notifications</h4></div><div class="rn-general-response-container-body rn-notification-container-body">` + `</div></div>`;


      let rn_main_mode_response = document.createElement("div");
      rn_main_mode_response.setAttribute("class", "rn-main-mode-notification");

      rn_main_mode_response.innerHTML = rn_mode_header + rn_general_response_container;

      notification_mode.model.get_notification_items();

      return rn_main_mode_response;
    }
  },
  model: {
    get_notification_items: function() {

      let rn_response_mode_response_items_body = "";

      chrome_storage.rn_get_the_notification_mode_values().then(notification_documents_array => {
        let renarrationList = notification_documents_array.notification_renarrations_array;
        let responseList = notification_documents_array.notification_response_array;

        let allTheNotifications = "";

        function onTargetDocumentBeforeTheLastHashSign(onTargetUrl) {
          let regex = /[^#]*/;
          let onTargetUrlUntilTHeHashTag = onTargetUrl.match(regex);
          let renarration_onTargetDocument = onTargetUrlUntilTHeHashTag[0];
          return renarration_onTargetDocument;
        }

        renarrationList.forEach((renarrationItem, i) => {
          renarrationItemKeysArray = Object.keys(renarrationItem);
          renarrationItemValuesArray = Object.values(renarrationItem);
          let renarrationNotificationItems = "";



          renarrationItemValuesArray.forEach((renarrationItemValuesArrayItemInitial, i) => {
            let renarrationItemValuesArrayItem = renarrationItemValuesArrayItemInitial;
            if (renarrationItemKeysArray[i] === "onTargetDocument") {
              renarrationItemValuesArrayItem = onTargetDocumentBeforeTheLastHashSign(renarrationItemValuesArrayItemInitial);
            }
            let urlOrText = notification_mode.controller.valid_url_check(renarrationItemValuesArrayItem) ? `<a href="${renarrationItemValuesArrayItem}" target="_blank" class="general-clickable-url"><p  class="slide-to-right-hover">${renarrationItemValuesArrayItem}</p></a>` : `<p class="text-with-overflow">${renarrationItemValuesArrayItem}</p>`;

            renarrationNotificationItems += `<div class="rn-general-notification-content-box"><div class="rn-general-content-label-value-group rn-notification-content-label-value-group"><div class="rn-general-content-label-name rn-notification-label-width"><p>${renarrationItemKeysArray[i]}</p></div><div class="rn-general-content-value-name rn-notification-name-color">${urlOrText}</div></div></div>`;
          });

          let renarrationNotification = `<div class="rn-general-sliding-content-area-box rn-general-sliding-content-area-display-none"> <div class="rn-general-sliding-content-area-box-header"> <p class="rn-general-sliding-content-area-box-header-overflow  rn-notification-overflow-text">Renarration-${renarrationItem.renarratedAt}</p><div class="rn-updown-icon-group"> <i class="rn-icon-down"></i> <i class="rn-icon-up"></i> </div></div><div class="rn-general-sliding-content-area-box-body rn-general-sliding-content-area-display-slide-to-none">${renarrationNotificationItems}
          </div></div>`;

          allTheNotifications += renarrationNotification;

        });

        responseList.forEach((responseItem, i) => {
          responseItemKeysArray = Object.keys(responseItem);
          responseItemValuesArray = Object.values(responseItem);
          let responseNotificationItems = "";

          responseItemValuesArray.forEach((responseItemValuesArrayItemInitial, i) => {
            let responseItemValuesArrayItem = responseItemValuesArrayItemInitial;
            if (responseItemKeysArray[i] === "id" || responseItemKeysArray[i] === "responseOnRenarration") {
              responseItemValuesArrayItem = onTargetDocumentBeforeTheLastHashSign(responseItemValuesArrayItemInitial);
            }

            let urlOrText = notification_mode.controller.valid_url_check(responseItemValuesArrayItem) ? `<a href="${responseItemValuesArrayItem}" target="_blank" class="general-clickable-url"><p  class="slide-to-right-hover">${responseItemValuesArrayItem}</p></a>` : `<p class="text-with-overflow">${responseItemValuesArrayItem}</p>`;
            responseNotificationItems += `<div class="rn-general-notification-content-box"><div class="rn-general-content-label-value-group rn-notification-content-label-value-group"><div class="rn-general-content-label-name rn-notification-label-width"><p>${responseItemKeysArray[i]}</p></div><div class="rn-general-content-value-name rn-notification-name-color">${urlOrText}</div></div></div>`;
          });

          let responseNotification = `<div class="rn-general-sliding-content-area-box rn-general-sliding-content-area-display-none"> <div class="rn-general-sliding-content-area-box-header"> <p class="rn-general-sliding-content-area-box-header-overflow  rn-notification-overflow-text">Response-${responseItem.respondedAt}</p><div class="rn-updown-icon-group"> <i class="rn-icon-down"></i> <i class="rn-icon-up"></i> </div></div><div class="rn-general-sliding-content-area-box-body rn-general-sliding-content-area-display-slide-to-none">${responseNotificationItems}
          </div></div>`;

          allTheNotifications += responseNotification;

        });


        document.querySelector("#RNEx").shadowRoot.querySelector(".rn-general-response-container-body").innerHTML = allTheNotifications;


      }).catch(e => {
        console.log("some error occured getting the rn_get_the_response_mode_response_documents_array from  the chrome storage", e);
      });

    }
  },
  controller: {
    main: function() {
      chrome_storage.addListener(notification_mode.controller.storage_listener.main);

      chrome_storage.rn_get_the_notification_mode_values().then((notification_mode_result) => {
        console.log('notification_mode_result', notification_mode_result);
      });
      console.log('main');
      document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-notification").addEventListener("click", notification_mode.controller.main_operation_chain);

    },
    main_operation_chain: function(e) {
      console.log("event:", e.target);
      console.log('main_operation_chain');
      if (e.target.parentNode.className === "rn-updown-icon-group") {
        notification_mode.controller.toggle_the_general_sliding_content_area_box(e);
      }
    },
    toggle_the_general_sliding_content_area_box: function(e) {
      e.target.closest(".rn-general-sliding-content-area-box").classList.toggle("rn-general-sliding-content-area-display-none");
      e.target.closest(".rn-general-sliding-content-area-box").querySelector(".rn-general-sliding-content-area-box-body").classList.toggle("rn-general-sliding-content-area-display-slide-to-none");
    },
    valid_url_check: function(str) {
      let pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
        '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
      return !!pattern.test(str);
    },
    storage_listener: {
      main: function(changes, namespace) {
        console.log("changes", changes);
        // console.log("namespace",namespace);
        for (let key in changes) {
          let storageChange = changes[key];
          if (key == "notification_mode") {
            notification_mode.model.get_notification_items();
            //      profile_mode.model.get_renarration_items();
          }
        }

      }
    }
  }
};

recommending_mode = {
  view: {
    main: function(clicked_item_nodes_array) {

      let rn_mode_header = `<div class="rn-current-mode-content-box"><div><p>Recommendation & Search Mode</p></div></div>`;

      let rn_recommending_mode_renarrations_header = `<div class="rn-general-content-box"><div class="rn-general-header"><h4>Recommendations</h4></div></div>`;


      let rn_recommending_mode_renarrations_items = `<div  class="rn-general-table">
  <table class="rn-general-table-overflow rn-general-table-scroll"><thead class="rn-general-table-thead rn-table-column-size"><tr class="rn-renarration-list-items-info"><th><p>Renarrator</p></th><th><p>TargetURL</p></th><th><p>Date</p></th><th><p>SourceURL</p></th></tr></thead><tbody id="recommending-table" class="rn-general-table-tbody rn-table-column-size">` + `</tbody></table></div>`;

      let rn_recommending_mode_renarrations_buttons = `<div class="rn-general-content-box">
      <input type="button" id="go-and-apply-the-new-renarration" class="rn-general-searchbox-button-second-type" value="Apply">
    </div>`;

      let rn_recommending_mode_search_header = `<div class="rn-general-content-box"><div class="rn-general-header"><h4>Search</h4></div></div>`;

      let rn_recommending_mode_search = `<div class="rn-general-content-box"><div class=""><table class="rn-general-searchbox-table"><tbody><tr><td><div class="rn-general-searchbox-header"><p>Filter</p></div></td><td><input type="text" class="rn-general-searchbox-textbox" placeholder="Type here..."></td><td><input type="button" class="rn-general-searchbox-button" value="Submit" id='search-filter'></td></tr></tbody></table></div>` + `<div class="rn-general-content-radiobuttons-group"> <div class="rn-general-content-radiobutton"> <input type="radio" name="search-radiobuttons" id="search-radiobutton-date" value="Date"> <p>Date</p> </div> <div class="rn-general-content-radiobutton"> <input type="radio" name="search-radiobuttons" id="search-radiobutton-renarrator" value="Renarrator"> <p>Renarrator</p> </div> </div>` + `<div class="rn-general-content-radiobuttons-group"> <div class="rn-general-content-radiobutton"> <input type="radio" name="search-radiobuttons" id="search-radiobutton-document" value="Document"> <p>Document</p> </div> </div>` + `</div>`;
      let removed_part = `<div class="rn-general-content-radiobuttons-group"> <div class="rn-general-content-radiobutton"> <input type="radio" name="search-radiobuttons" id="search-radiobutton-motivation" value="Motivation"> <p>Motivation</p> </div> <div class="rn-general-content-radiobutton"> <input type="radio" name="search-radiobuttons" id="search-radiobutton-document" value="Document"> <p>Document</p> </div> </div>` + `<div class="rn-general-content-radiobuttons-group"> <div class="rn-general-content-radiobutton"> <input type="radio" name="search-radiobuttons" id="search-radiobutton-language" value="Language"> <p>Language</p> </div> <div class="rn-general-content-radiobutton"> <input type="radio" name="search-radiobuttons" id="search-radiobutton-contains" value="Contains"> <p>Contains</p> </div> </div>` + `</div>`;
      let rn_recommending_mode_search_items = `<div class="rn-general-table">
  <table class="rn-general-table-overflow rn-general-table-scroll"><thead class="rn-general-table-thead rn-table-column-size"><tr class="rn-renarration-list-items-info"><th><p>Renarrator</p></th><th><p>TargetURL</p></th><th><p>Date</p></th><th><p>SourceURL</p></th></tr></thead><tbody id="search-table" class="rn-general-table-tbody rn-table-column-size">` + `</tbody></table>`;
      let rn_recommending_mode_search_buttons = `<div class="rn-general-content-box">
  <input type="button" id="go-and-apply-the-searched-renarration" class="rn-general-searchbox-button-second-type" value="Apply"></div>`;

      // let rn_general_recommending_container = `<div class="rn-general-response-container"><div class="rn-general-response-container-header"><h4>Notifications</h4></div><div class="rn-general-response-container-body rn-notification-container-body">` + `</div></div>`;


      let rn_main_mode_recommending = document.createElement("div");
      rn_main_mode_recommending.setAttribute("class", "rn-main-mode-recommending");

      rn_main_mode_recommending.innerHTML = rn_mode_header + rn_recommending_mode_renarrations_header + rn_recommending_mode_renarrations_items + rn_recommending_mode_renarrations_buttons + rn_recommending_mode_search_header + rn_recommending_mode_search + rn_recommending_mode_search_items + rn_recommending_mode_search_buttons;

      recommending_mode.model.get_recommendation_items();

      return rn_main_mode_recommending;
    }
  },
  model: {
    get_recommendation_items: function() {

      let rn_profile_mode_renarrations_items_tbody = "";

      chrome_storage.rn_get_the_recommending_mode_values().then(recommending_document_obj => {
        let renarration_documents_array = recommending_document_obj.recommendation_second_degree_response_array;
        let rn_profile_renarrations_title = "Null";
        let rn_profile_renarrations_language = "Null";
        let rn_profile_renarrations_motivation = "Null";
        let rn_profile_renarrations_date = "Null";
        let rn_profile_renarrations_url = "Null";
        let renarration_documents_array_date_sorted = renarration_documents_array.slice();
        renarration_documents_array_date_sorted.sort(function(a, b) {
          return new Date(b.renarratedAt) - new Date(a.renarratedAt);
        });
        for (let i = 0; i < renarration_documents_array_date_sorted.length; i++) {
          let rn_target_url_and_unique_id_attribute = `rnuniqueid="${renarration_documents_array_date_sorted[i].onTargetDocument}"`;
          rn_profile_mode_renarrations_items_tbody += `<tr ${rn_target_url_and_unique_id_attribute}><td><p>${renarration_documents_array_date_sorted[i].renarratedBy}</p></td><td><p>${renarration_documents_array_date_sorted[i].onTargetDocument}</p></td><td><p>${renarration_documents_array_date_sorted[i].renarratedAt}</p></td><td><p>${renarration_documents_array_date_sorted[i].onSourceDocument}</p></td></tr>`;
        }

        document.querySelector("#RNEx").shadowRoot.querySelector("#recommending-table").innerHTML = rn_profile_mode_renarrations_items_tbody;

        let rn_profile_mode_renarrations_items = `<div class="rn-general-table">
              <table class="rn-general-table-overflow rn-general-table-scroll"><thead class="rn-general-table-thead"><tr class="rn-renarration-list-items-info"><th><p>Title</p></th><th><p>Language</p></th><th><p>Motivation</p></th><th><p>Date</p></th><th><p>URL</p></th></tr></thead><tbody id="recommending-table" class="rn-general-table-tbody">` + rn_profile_mode_renarrations_items_tbody + `</tbody></table></div>`;


        return rn_profile_mode_renarrations_items;
      }).catch(e => {
        console.log("some error occured getting the rn_get_the_reading_mode_renarration_documents_array from  the chrome storage", e);
      });

    },
    get_search_items: function() {

      let rn_profile_mode_renarrations_items_tbody = "";

      chrome_storage.rn_get_the_recommending_mode_values().then(recommending_document_obj => {
        let renarration_documents_array = Object.values(recommending_document_obj.search_operation_filtered_rnex_renarrations_array);
        let rn_profile_renarrations_title = "Null";
        let rn_profile_renarrations_language = "Null";
        let rn_profile_renarrations_motivation = "Null";
        let rn_profile_renarrations_date = "Null";
        let rn_profile_renarrations_url = "Null";
        let renarration_documents_array_date_sorted = renarration_documents_array.slice();
        renarration_documents_array_date_sorted.sort(function(a, b) {
          return new Date(b.publishDate) - new Date(a.publishDate);
        });
        for (let i = 0; i < renarration_documents_array_date_sorted.length; i++) {
          let rn_target_url_and_unique_id_attribute = `rnuniqueid="${renarration_documents_array_date_sorted[i].onTargetDocument}"`;
          rn_profile_mode_renarrations_items_tbody += `<tr ${rn_target_url_and_unique_id_attribute}><td><p>${renarration_documents_array_date_sorted[i].renarratedBy}</p></td><td><p>${renarration_documents_array_date_sorted[i].onTargetDocument}</p></td><td><p>${renarration_documents_array_date_sorted[i].renarratedAt}</p></td><td><p>${renarration_documents_array_date_sorted[i].onSourceDocument}</p></td></tr>`;
        }

        document.querySelector("#RNEx").shadowRoot.querySelector("#search-table").innerHTML = rn_profile_mode_renarrations_items_tbody;

      }).catch(e => {
        console.log("some error occured getting the rn_get_the_reading_mode_renarration_documents_array from  the chrome storage", e);
      });

    }
  },
  controller: {
    main: function() {
      chrome_storage.addListener(recommending_mode.controller.storage_listener.main);

      console.log('main');
      document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-recommending").addEventListener("click", recommending_mode.controller.main_operation_chain);

    },
    main_operation_chain: function(e) {
      console.log("event:", e.target);
      recommending_mode.controller.highlighting_the_clicked_renarration_row(e);
      if (e.target.id === "go-and-apply-the-new-renarration") {

        let clickedRow = document.querySelector("#RNEx").shadowRoot.querySelector("#recommending-table").querySelector(".rn-general-table-tbody-tr-clicked");
        if (clickedRow) {
          console.log("clickedRow", clickedRow);
          let renarrationUniqueUrl = clickedRow.getAttribute("rnuniqueid");
          console.log("renarrationUniqueUrl", renarrationUniqueUrl);
          if (renarrationUniqueUrl) {
            chrome_storage.rn_set_recommending_mode_chosen_renarration(null).then(s => {
              main_mode.model.get_the_renarration_from_the_url(renarrationUniqueUrl);
            });
          }
        }
      } else if (e.target.id === "go-and-apply-the-searched-renarration") {
        console.log('go-and-apply-the-searched-renarration');

        let clickedRow = document.querySelector("#RNEx").shadowRoot.querySelector("#search-table").querySelector(".rn-general-table-tbody-tr-clicked");
        if (clickedRow) {
          console.log("clickedRow", clickedRow);
          let renarrationUniqueUrl = clickedRow.getAttribute("rnuniqueid");
          console.log("renarrationUniqueUrl", renarrationUniqueUrl);
          if (renarrationUniqueUrl) {
            chrome_storage.rn_set_recommending_mode_chosen_renarration(null).then(s => {
              main_mode.model.get_the_renarration_from_the_url(renarrationUniqueUrl);
            });
          }
        }

      } else if (e.target.id === 'search-filter') {
        recommending_mode.controller.search_filter_operations.main_check_control();

      }
    },
    search_filter_operations: {
      main_check_control: function(e) {
        let text_value = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-recommending").querySelector(".rn-general-searchbox-textbox").value;

        let date_value = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-recommending").querySelector("#search-radiobutton-date").checked;
        let renarrator_value = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-recommending").querySelector("#search-radiobutton-renarrator").checked;
        let document_value = document.querySelector("#RNEx").shadowRoot.querySelector(".rn-main-mode-recommending").querySelector("#search-radiobutton-document").checked;
        let contains_value = false;
        let motivation_value = false;
        let language_value = false;

        if (text_value && (date_value || motivation_value || language_value || renarrator_value || document_value || contains_value)) {
          if (date_value) {
            recommending_mode.controller.search_filter_operations.apply_the_filter("date_value", text_value);
          } else if (motivation_value) {
            recommending_mode.controller.search_filter_operations.apply_the_filter("motivation_value", text_value);
          } else if (language_value) {
            recommending_mode.controller.search_filter_operations.apply_the_filter("language_value", text_value);
          } else if (renarrator_value) {
            recommending_mode.controller.search_filter_operations.apply_the_filter("renarrator_value", text_value);
          } else if (document_value) {
            recommending_mode.controller.search_filter_operations.apply_the_filter("document_value", text_value);
          } else if (contains_value) {
            recommending_mode.controller.search_filter_operations.apply_the_filter("contains_value", text_value);
          }

        }
      },
      apply_the_filter_OLD: function(operation_type, text_value) {
        let filter_text = text_value;

        chrome_storage.rn_get_the_recommending_mode_values().then(recommending_document_obj => {
          let resMinimized = recommending_document_obj.search_operation_all_the_renarrations_array;
          let resInitial = recommending_document_obj.search_operation_all_the_renarrations_initial_array;

          let date_value = "date_value";
          let motivation_value = "motivation_value";
          let language_value = "language_value";
          let renarrator_value = "renarrator_value";
          let document_value = "document_value";
          let contains_value = "contains_value";

          if (operation_type === motivation_value) {

            let searchMotivation = recommending_mode.controller.search_filter_operations.searchRenarration(resInitial, "motivation", text_value);

            let searchMotivationArray = recommending_mode.controller.search_filter_operations.searchAndBringRenarration(resMinimized, searchMotivation);

            console.log("motivation", searchMotivationArray);

            recommending_mode.controller.search_filter_operations.get_search_items(searchMotivationArray);

          } else if (operation_type === language_value) {

            let searchRenarrationTransformationLanguage = recommending_mode.controller.search_filter_operations.searchRenarration(resInitial, "renarrationTransformationLanguage", text_value);

            let searchRenarrationTransformationLanguageArray = recommending_mode.controller.search_filter_operations.searchAndBringRenarration(resMinimized, searchRenarrationTransformationLanguage);

            console.log("renarrationTransformationLanguage", searchRenarrationTransformationLanguageArray);

            recommending_mode.controller.search_filter_operations.get_search_items(searchRenarrationTransformationLanguageArray);
          } else if (operation_type === renarrator_value) {

            let searchActorRenarrator = recommending_mode.controller.search_filter_operations.searchRenarration(resInitial, "actorRenarrator", text_value);

            let searchActorRenarratorArray = recommending_mode.controller.search_filter_operations.searchAndBringRenarration(resMinimized, searchActorRenarrator);

            console.log("actorRenarrator", searchActorRenarratorArray);

            recommending_mode.controller.search_filter_operations.get_search_items(searchActorRenarratorArray);

          } else if (operation_type === document_value) {

            let searchSourceDocument = recommending_mode.controller.search_filter_operations.searchRenarration(resInitial, "sourceDocument", text_value);

            let searchSourceDocumentRenarrationArray = recommending_mode.controller.search_filter_operations.searchAndBringRenarration(resMinimized, searchSourceDocument);

            console.log("sourceDocument", searchSourceDocumentRenarrationArray);

            recommending_mode.controller.search_filter_operations.get_search_items(searchSourceDocumentRenarrationArray);
          } else if (operation_type === contains_value) {

          }


        }).catch(e => {
          console.log("some error occured getting the rn_get_the_reading_mode_renarration_documents_array from  the chrome storage", e);
        });


      },
      apply_the_filter: function(operation_type, text_value) {
        let filter_text = text_value;

        let date_value = "date_value";
        let motivation_value = "motivation_value";
        let language_value = "language_value";
        let renarrator_value = "renarrator_value";
        let document_value = "document_value";
        let contains_value = "contains_value";

        if (operation_type === renarrator_value) {
          recommending_mode.controller.search_filter_operations.getSearchedRenarration(renarrator_value, text_value);


        } else if (operation_type === document_value) {
          recommending_mode.controller.search_filter_operations.getSearchedRenarration(document_value, text_value);

        } else if (operation_type === date_value) {
          recommending_mode.controller.search_filter_operations.getSearchedRenarration(date_value, text_value);

        }
      },
      getSearchedRenarration: function(operation_type, text_value) {
        main_mode.model.get_the_searched_renarration_result_from_the_rnex_pod(operation_type, text_value);
      },
      getSearchedRenarrationFromStorageAndApply: function() {
        chrome_storage.rn_get_the_recommending_mode_values().then(recommending_document_obj => {
          main_mode.controller.rn_general_store_the_renarration_to_apply_the_page(recommending_document_obj.chosen_renarration[0]);
        });
      }
    },
    highlighting_the_clicked_renarration_row: function(e) {
      if (e.target.closest('#recommending-table') && e.target.closest("tr")) {
        e.target.closest("#recommending-table").childNodes.forEach(item => {
          item.classList.remove("rn-general-table-tbody-tr-clicked");
        });
        e.target.closest("tr").classList.add("rn-general-table-tbody-tr-clicked");

      } else if (e.target.closest('#search-table') && e.target.closest("tr")) {
        e.target.closest("#search-table").childNodes.forEach(item => {
          item.classList.remove("rn-general-table-tbody-tr-clicked");
        });
        e.target.closest("tr").classList.add("rn-general-table-tbody-tr-clicked");

      }
    },
    storage_listener: {
      main: function(changes, namespace) {
        console.log("changes", changes);
        console.log("namespace", namespace);
        for (let key in changes) {
          let storageChange = changes[key];
          if (key == "recommending_mode") {
            if (changes.recommending_mode.oldValue && changes.recommending_mode.oldValue.chosen_renarration === null && (changes.recommending_mode.newValue.chosen_renarration !== changes.recommending_mode.oldValue.chosen_renarration)) {
              recommending_mode.controller.search_filter_operations.getSearchedRenarrationFromStorageAndApply();
            } else {
              recommending_mode.model.get_recommendation_items();
              recommending_mode.model.get_search_items();
            }
          }
        }

      }
    }
  }
};

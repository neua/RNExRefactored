import Renarration from "../content/renarration_engine/renarrationClass";
import RenarrationTransformation from "../content/renarration_engine/renarrationTransformationClass";
import { selectedHTMLPropertiesInterface } from "../content/common_components/abstracts";


let A_RT_1_selectedHTMLProperties:selectedHTMLPropertiesInterface = {
  value: "<h1>Fibula</h1>",
  xpath: "/html[1]/body[1]/div[1]/h1[1]",
  xpathOfNextElementSibling: "/html[1]/body[1]/div[1]/img[1]",
  xpathOfPreviousElementSibling: null
}

let A_RT_1 = new RenarrationTransformation(0, "en", "text", "replace", "<p style='font-size:30px; color:#3402e8; font-weight:bold;' >Fibula</p>", A_RT_1_selectedHTMLProperties, new Date().toISOString());


let A_RT_2_selectedHTMLProperties:selectedHTMLPropertiesInterface = {
  value: "<p>The fibula is a piece of Phrygian jewelry that dates back to the 8th-7th century BC.</p>",
  xpath: "/html[1]/body[1]/div[1]/p[2]",
  xpathOfNextElementSibling: null,
  xpathOfPreviousElementSibling: "/html[1]/body[1]/div[1]/img[1]"
}

let A_RT_2 = new RenarrationTransformation(1, "en", "text", "replace", "<p style='font-size:17px; color:#02e812; font-weight:bold;  '>It is an ancient jewelry</p>", A_RT_2_selectedHTMLProperties as any, new Date().toISOString());

let A_RN_movivation = {
  alteration: true,
  elaboration: false,
  simplification: true,
  correction: false,
  translation: false
}

export const Test_A_RN = new Renarration("kpmz0ylc-69vrp9x6-1qgwthwt", new Date().toISOString(), "https://neua.gitlab.io/renarration-test-cases/fibula_example", "https://bob-therenarrator.inrupt.net", A_RN_movivation as any, [A_RT_1, A_RT_2])


//
// let jsonx: any = {
//   "renarrationUri": "kpmz0ylc-69vrp9x6-1qgwthwt",
//   "createdAt": "2021-06-07T18:52:07.584Z",
//   "onSourceDocument": "https://neua.gitlab.io/renarration-test-cases/fibula_example",
//   "renarratedBy": "https://bob-therenarrator.inrupt.net",
//   "motivation": {
//     "alteration": true,
//     "elaboration": false,
//     "simplification": true,
//     "correction": false,
//     "translation": false
//   },
//   "renarrationTransformations": [
//     {
//       "id": 0,
//       "language": "en",
//       "action": "replace",
//       "narration": "<p style='font-size:30px; color:#3402e8; font-weight:bold;  '>Fibula</p>",
//       "mediaType": "text",
//       "selectedHTMLProperties": {
//         "value": "<h1>Fibula</h1>",
//         "xpath": "/html[1]/body[1]/div[1]/h1[1]",
//         "xpathOfNextElementSibling": "/html[1]/body[1]/div[1]/img[1]",
//         "xpathOfPreviousElementSibling": null
//       },
//       "createdAt": "2021-06-07T18:51:21.265Z"
//     },
//     {
//       "id": 1,
//       "language": "en",
//       "action": "insertBefore",
//       "narration": "<p style='font-size:17px; color:#02e812; font-weight:bold;  '>It is an ancient jewelry</p>",
//       "mediaType": "text",
//       "selectedHTMLProperties": {
//         "value": "<p>The fibula is a piece of Phrygian jewelry that dates back to the 8th-7th century BC.</p>",
//         "xpath": "/html[1]/body[1]/div[1]/p[2]",
//         "xpathOfNextElementSibling": null,
//         "xpathOfPreviousElementSibling": "/html[1]/body[1]/div[1]/img[1]"
//       },
//       "createdAt": "2021-06-07T18:52:03.089Z"
//     }
//   ]
// };

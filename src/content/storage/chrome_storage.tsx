

class ChromeStorage {

  addListener = function(func) {
    chrome.storage.onChanged.addListener(func);
  }
  rn_get_the_session_info = async function() {
    let current_mode_promise = new Promise((resolve, reject) => {
      chrome.storage.local.get("rn_session_info", function(val) {
        if (val.rn_session_info === undefined || val.rn_session_info === null) {
          reject(null);
        } else {
          resolve(val.rn_session_info);
        }
      });
    });
    return await current_mode_promise;
  }
  rn_current_mode = function(val) {
    chrome.storage.local.set({
      rn_current_mode: val
    });
  }

  rn_remove_except_the_given_mode = function(given_mode) {

    if (given_mode !== 'rn-modes-profile') {
      chrome.storage.local.remove(['profile_mode'], () => { });
    }
    if (given_mode !== 'rn-modes-recommending') {
      chrome.storage.local.remove(['recommending_mode'], () => { });
    }
    if (given_mode !== 'rn-modes-response') {
      chrome.storage.local.remove(['rn-modes-response'], () => { });
    }
    if (given_mode !== 'rn-modes-notification') {
      chrome.storage.local.remove(['notification_mode'], () => { });
    }
    if (given_mode !== 'rn-modes-account') {
      chrome.storage.local.remove(['account_mode'], () => { });
    }
    if (given_mode !== 'rn-modes-reading') {
      chrome.storage.local.remove(['reading_mode'], () => { });
    }
    if (given_mode !== 'rn-modes-writing') {
    }

  }

  rn_navbar_toggle_clicked_and_open = function(val) {
    chrome.storage.local.set({
      rn_navbar_toggle_clicked_and_open: val
    });
  }

  rn_general_store_the_single_renarration_to_apply_the_page = async function(success) {
    let current_mode_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get(["rn_general_apply_the_renarration"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            rn_general_apply_the_renarration: {
              renarration_to_apply: success
            }
          }, () => {
            resolve(success);
          });
        } else {
          let createdNotificationObject = value.rn_general_apply_the_renarration;
          createdNotificationObject.renarration_to_apply = success;
          chrome.storage.local.set({
            rn_general_apply_the_renarration: createdNotificationObject
          }, () => {
            resolve(success);
          });
        }
      });

    });
    return await current_mode_promise;
  }

  rn_general_store_the_single_renarration_store_the_applied_renarration_on_the_page = async function(success) {
    let current_mode_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get(["rn_general_applied_renarration"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            rn_general_applied_renarration: {
              applied_renarration: success
            }
          }, () => {
            resolve(success);
          });
        } else {
          let createdNotificationObject = value.rn_general_applied_renarration;
          createdNotificationObject.applied_renarration = success;
          chrome.storage.local.set({
            rn_general_applied_renarration: createdNotificationObject
          }, () => {
            resolve(success);
          });
        }
      });

    });
    return await current_mode_promise;
  }

  rn_get_the_general_store_the_single_renarration_store_the_applied_renarration_on_the_page = async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("rn_general_applied_renarration", function(val) {
        if (val.rn_general_applied_renarration.applied_renarration) {
          resolve(val.rn_general_applied_renarration.applied_renarration);
        } else {
          reject(null);
        }
      });

    });

    return await a_promise;
  }

  rn_get_the_general_store_single_renarration_to_apply_the_page_values = async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("rn_general_apply_the_renarration", function(val) {
        if (val.rn_general_apply_the_renarration) {
          resolve(val.rn_general_apply_the_renarration);
        } else {
          reject(null);
        }
      });

    });

    return await a_promise;
  }

  rn_account_mode_clicked_row_before_the_go_button = async function(val) {
    let a_promise = new Promise((resolve, reject) => {
      chrome.storage.local.get(["account_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            account_mode: {
              clicked_row_before_the_go_button: val
            }
          }, function() {
            resolve("nice1 rn_account_mode_clicked_row_before_the_go_button");

          });
        } else {
          let createdRenarrationObject = value.account_mode;
          createdRenarrationObject.clicked_row_before_the_go_button = val;

          chrome.storage.local.set({
            account_mode: createdRenarrationObject
          }, function() {
            resolve("nice2 rn_account_mode_clicked_row_before_the_go_button");

          });
        }
      });

    });


    return await a_promise;
  }

  rn_account_mode_clicked_row_unique_id_before_the_go_button = async function(val) {
    let a_promise = new Promise((resolve, reject) => {
      chrome.storage.local.get(["account_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            account_mode: {
              clicked_row_unique_id_before_the_go_button: val
            }
          }, function() {

            resolve("nice1 rn_account_mode_clicked_row_unique_id_before_the_go_button");

          });
        } else {
          let createdRenarrationObject = value.account_mode;
          createdRenarrationObject.clicked_row_unique_id_before_the_go_button = val;

          chrome.storage.local.set({
            account_mode: createdRenarrationObject
          }, function() {
            resolve("nice2 rn_account_mode_clicked_row_unique_id_before_the_go_button");

          });
        }
      });
    });
    return await a_promise;
  }

  rn_reading_mode_clicked_row_before_the_go_button = async function(val) {
    let a_promise = new Promise((resolve, reject) => {
      chrome.storage.local.get(["reading_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            reading_mode: {
              clicked_row_before_the_go_button: val
            }
          }, function() {
            resolve("nice1 rn_reading_mode_clicked_row_before_the_go_button");

          });
        } else {
          let createdRenarrationObject = value.reading_mode;
          createdRenarrationObject.clicked_row_before_the_go_button = val;

          chrome.storage.local.set({
            reading_mode: createdRenarrationObject
          }, function() {
            resolve("nice2 rn_reading_mode_clicked_row_before_the_go_button");

          });
        }
      });

    });


    return await a_promise;
  }

  rn_reading_mode_clicked_row_unique_id_before_the_go_button = async function(val) {
    let a_promise = new Promise((resolve, reject) => {
      chrome.storage.local.get(["reading_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            reading_mode: {
              clicked_row_unique_id_before_the_go_button: val
            }
          }, function() {
            //    console.log('Value is set to ' + reading_mode);
            resolve("nice1 rn_reading_mode_clicked_row_unique_id_before_the_go_button");

          });
        } else {
          let createdRenarrationObject = value.reading_mode;
          createdRenarrationObject.clicked_row_unique_id_before_the_go_button = val;

          chrome.storage.local.set({
            reading_mode: createdRenarrationObject
          }, function() {
            //  console.log('Value is set to ' + reading_mode);
            resolve("nice2 rn_reading_mode_clicked_row_unique_id_before_the_go_button");

          });
        }
      });
    });
    return await a_promise;
  }

  rn_profile_mode_clicked_row_unique_id_before_the_go_button = async function(val) {
    let a_promise = new Promise((resolve, reject) => {
      chrome.storage.local.get(["profile_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            profile_mode: {
              clicked_row_unique_id_before_the_go_button: val
            }
          }, function() {
            //    console.log('Value is set to ' + profile_mode);
            resolve("nice1 rn_profile_mode_clicked_row_unique_id_before_the_go_button");

          });
        } else {
          let createdRenarrationObject = value.profile_mode;
          createdRenarrationObject.clicked_row_unique_id_before_the_go_button = val;

          chrome.storage.local.set({
            profile_mode: createdRenarrationObject
          }, function() {
            //  console.log('Value is set to ' + profile_mode);
            resolve("nice2 rn_profile_mode_clicked_row_unique_id_before_the_go_button");

          });
        }
      });
    });
    return await a_promise;
  }

  rn_profile_mode_clicked_row_before_the_go_button = async function(val) {
    let a_promise = new Promise((resolve, reject) => {
      chrome.storage.local.get(["profile_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            profile_mode: {
              clicked_row_before_the_go_button: val
            }
          }, function() {
            //  console.log('Value is set to ' + profile_mode);
            resolve("nice1 rn_profile_mode_clicked_row_before_the_go_button");

          });
        } else {
          let createdRenarrationObject = value.profile_mode;
          createdRenarrationObject.clicked_row_before_the_go_button = val;

          chrome.storage.local.set({
            profile_mode: createdRenarrationObject
          }, function() {
            //  console.log('Value is set to ' + profile_mode);
            resolve("nice2 rn_profile_mode_clicked_row_before_the_go_button");

          });

        }
      });

    });

    //    chrome_storage.rn_response_mode_renarration_value(val);

    return await a_promise;
  }

  rn_response_mode_comment_text = async function(val) {
    let a_promise = new Promise((resolve, reject) => {
      chrome.storage.local.get(["response_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            response_mode: {
              comment_text: val
            }
          }, function() {
            //  console.log('Value is set to ' + response_mode);
            resolve("nice1 rn_response_mode_comment_text");

          });
        } else {
          let createdRenarrationObject = value.response_mode;
          createdRenarrationObject.comment_text = val;

          chrome.storage.local.set({
            response_mode: createdRenarrationObject
          }, function() {
            //  console.log('Value is set to ' + response_mode);
            resolve("nice2 rn_response_mode_comment_text");

          });
        }
      });

    });
    return await a_promise;

  }

  rn_response_mode_rate_value = async function(val) {
    let a_promise = new Promise((resolve, reject) => {
      chrome.storage.local.get(["response_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            response_mode: {
              rate_value: val
            }
          }, function() {
            //  console.log('Value is set to ' + response_mode);
            resolve("nice1 rn_response_mode_comment_text");

          });
        } else {
          let createdRenarrationObject = value.response_mode;
          createdRenarrationObject.rate_value = val;

          chrome.storage.local.set({
            response_mode: createdRenarrationObject
          }, function() {
            //  console.log('Value is set to ' + response_mode);
            resolve("nice2 rn_response_mode_comment_text");

          });
        }
      });

    });
    return await a_promise;

  }

  rn_response_mode_renarration_value = async function(val) {
    let a_promise = new Promise((resolve, reject) => {
      chrome.storage.local.get(["response_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            response_mode: {
              renarration_value: val
            }
          }, function() {
            //  console.log('Value is set to ' + response_mode);
            resolve("nice1 rn_response_mode_comment_text");

          });
        } else {
          let createdRenarrationObject = value.response_mode;
          createdRenarrationObject.renarration_value = val;

          chrome.storage.local.set({
            response_mode: createdRenarrationObject
          }, function() {
            //  console.log('Value is set to ' + response_mode);
            resolve("nice2 rn_response_mode_comment_text");

          });
        }
      });

    });
    return await a_promise;

  }

  rn_get_the_response_mode_renarration_value = async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("response_mode", function(val) {
        if (val.response_mode) {
          if (val.response_mode.renarration_value === undefined || val.response_mode.renarration_value === null) {
            reject(null);
          } else {
            resolve(val.response_mode.renarration_value);
          }
        } else {
          console.log("chrome.storage.local.get response_mode doesn't exist");
          reject("chrome.storage.local.get response_mode doesn't exist");
        }
      });

    });

    return await a_promise;
  }

  rn_get_the_response_mode_response_documents_array = async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("response_mode", function(val) {
        if (val.response_mode === undefined || val.response_mode === null) {
          reject(null);
        } else {
          resolve(val.response_mode.response_documents_array);
        }
      });

    });

    return await a_promise;
  }

  rn_get_the_account_mode_clicked_row_unique_id_before_the_go_button = async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("account_mode", function(val) {
        if (val.account_mode) {
          if (val.account_mode.clicked_row_unique_id_before_the_go_button === undefined || val.account_mode.clicked_row_unique_id_before_the_go_button === null) {
            reject(null);
          } else {
            resolve(val.account_mode.clicked_row_unique_id_before_the_go_button);
          }
        } else {
          console.log("chrome.storage.local.get account_mode doesn't exist");
          reject("chrome.storage.local.get account_mode doesn't exist");
        }
      });

    });

    return await a_promise;
  }

  rn_get_the_reading_mode_clicked_row_unique_id_before_the_go_button = async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("reading_mode", function(val) {
        if (val.reading_mode) {
          if (val.reading_mode.clicked_row_unique_id_before_the_go_button === undefined || val.reading_mode.clicked_row_unique_id_before_the_go_button === null) {
            reject(null);
          } else {
            resolve(val.reading_mode.clicked_row_unique_id_before_the_go_button);
          }
        } else {
          console.log("chrome.storage.local.get reading_mode doesn't exist");
          reject("chrome.storage.local.get reading_mode doesn't exist");
        }
      });

    });

    return await a_promise;
  }

  rn_get_the_profile_mode_clicked_row_unique_id_before_the_go_button = async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("profile_mode", function(val) {
        if (val.profile_mode) {
          if (val.profile_mode.clicked_row_unique_id_before_the_go_button === undefined || val.profile_mode.clicked_row_unique_id_before_the_go_button === null) {
            reject(null);
          } else {
            resolve(val.profile_mode.clicked_row_unique_id_before_the_go_button);
          }
        } else {
          console.log("chrome.storage.local.get profile_mode doesn't exist");
          reject("chrome.storage.local.get profile_mode doesn't exist");
        }
      });

    });

    return await a_promise;
  }

  rn_get_the_account_mode_clicked_row_renarrations_value_before_the_go_button = async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("account_mode", function(val) {
        if (val.account_mode.clicked_row_before_the_go_button === undefined || val.account_mode.clicked_row_before_the_go_button === null) {
          reject(null);
        } else {
          resolve(val.account_mode.clicked_row_before_the_go_button);
        }
      });

    });

    return await a_promise;
  }

  rn_get_the_reading_mode_clicked_row_renarrations_value_before_the_go_button = async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("reading_mode", function(val) {
        if (val.reading_mode.clicked_row_before_the_go_button === undefined || val.reading_mode.clicked_row_before_the_go_button === null) {
          reject(null);
        } else {
          resolve(val.reading_mode.clicked_row_before_the_go_button);
        }
      });

    });

    return await a_promise;
  }

  rn_get_the_profile_mode_clicked_row_renarrations_value_before_the_go_button = async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("profile_mode", function(val) {
        if (val.profile_mode.clicked_row_before_the_go_button === undefined || val.profile_mode.clicked_row_before_the_go_button === null) {
          reject(null);
        } else {
          resolve(val.profile_mode.clicked_row_before_the_go_button);
        }
      });

    });

    return await a_promise;
  }

  rn_get_the_current_mode = async function() {
    let current_mode_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("rn_current_mode", function(val) {

        if (val.rn_current_mode === undefined || val.rn_current_mode === null) {
          reject(null);
        } else {
          resolve(val.rn_current_mode);
        }
      });

    });

    return await current_mode_promise;
  }

  rn_get_the_navbar_toggle_clicked_and_open = async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("rn_navbar_toggle_clicked_and_open", function(val) {

        if (val.rn_navbar_toggle_clicked_and_open === undefined || val.rn_navbar_toggle_clicked_and_open === null) {
          reject(null);
        } else {
          resolve(val.rn_navbar_toggle_clicked_and_open);
        }
      });

    });

    return await a_promise;
  }

  rn_get_the_account_mode_renarrations_and_the_table = async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("account_mode", function(val) {
        if (val.account_mode) {
          if (val.account_mode.renarrations_and_the_table === undefined || val.account_mode.renarrations_and_the_table === null) {
            reject(null);
          } else {
            resolve(val.account_mode.renarrations_and_the_table);
          }
        } else {
          console.log("chrome.storage.local.get account_mode doesn't exist");
          reject("chrome.storage.local.get account_mode doesn't exist");
        }
      });

    });

    return await a_promise;
  }

  rn_get_the_reading_mode_renarrations_and_the_table = async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("reading_mode", function(val) {
        if (val.reading_mode) {
          if (val.reading_mode.renarrations_and_the_table === undefined || val.reading_mode.renarrations_and_the_table === null) {
            reject(null);
          } else {
            resolve(val.reading_mode.renarrations_and_the_table);
          }
        } else {
          console.log("chrome.storage.local.get reading_mode doesn't exist");
          reject("chrome.storage.local.get reading_mode doesn't exist");
        }
      });

    });

    return await a_promise;
  }

  rn_get_the_profile_mode_renarrations_and_the_table = async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("profile_mode", function(val) {
        if (val.profile_mode) {
          if (val.profile_mode.renarrations_and_the_table === undefined || val.profile_mode.renarrations_and_the_table === null) {
            reject(null);
          } else {
            resolve(val.profile_mode.renarrations_and_the_table);
          }
        } else {
          console.log("chrome.storage.local.get profile_mode doesn't exist");
          reject("chrome.storage.local.get profile_mode doesn't exist");
        }
      });

    });

    return await a_promise;
  }

  rn_get_the_reading_mode_renarration_documents_array = async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("reading_mode", function(val) {
        if (val.reading_mode === undefined || val.reading_mode === null) {
          reject(null);
        } else {
          resolve(val.reading_mode.renarration_documents_array);
        }
      });

    });

    return await a_promise;
  }

  rn_get_the_profile_mode_renarration_documents_array = async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("profile_mode", function(val) {
        if (val.profile_mode === undefined || val.profile_mode === null) {
          reject(null);
        } else {
          resolve(val.profile_mode.renarration_documents_array);
        }
      });

    });

    return await a_promise;
  }

  rn_get_the_current_session_renarrators_info_object = async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("rn_session_renarrators_info", function(val) {
        if (val.rn_session_renarrators_info) {
          resolve(val.rn_session_renarrators_info);
        } else {
          reject(null);
        }
      });

    });

    return await a_promise;
  }

  rn_get_the_profile_mode_renarrators_info_object = async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("profile_mode", function(val) {
        if (val.profile_mode === undefined || val.profile_mode === null) {
          reject(null);
        } else {
          resolve(val.profile_mode.renarrators_profile_info);
        }
      });

    });

    return await a_promise;
  }

  rn_get_the_notification_mode_values = async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("notification_mode", function(val) {
        if (val.notification_mode) {
          resolve(val.notification_mode);
        } else {
          reject("notification_mode doesn't exist");
        }
      });

    });

    return await a_promise;
  }

  rn_get_the_recommending_mode_values = async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("recommending_mode", function(val) {
        if (val.recommending_mode) {
          resolve(val.recommending_mode);
        } else {
          reject("recommending_mode doesn't exist");
        }
      });

    });

    return await a_promise;
  }

  rn_get_the_extension_current_tab_url = async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("rn_extension_current_tab_url", function(val) {
        if (val.rn_extension_current_tab_url === undefined || val.rn_extension_current_tab_url === null) {
          reject(null);
        } else {
          resolve(val.rn_extension_current_tab_url);
        }
      });

    });

    return await a_promise;
  }

  rn_set_recommending_mode_chosen_renarration = async function(success) {
    let current_mode_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get(["recommending_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            recommending_mode: {
              chosen_renarration: success
            }
          }, (): void => {
            resolve(success);
          });
        } else {
          let createdRenarrationObject = value.recommending_mode;
          createdRenarrationObject.chosen_renarration = success;

          chrome.storage.local.set({
            recommending_mode: createdRenarrationObject
          }, () => {
            resolve(success);
          });

        }
      });

    });
    return await current_mode_promise;
  }

  rn_get_external_api_analytics_for_the_applied_renarration_on_the_page = async function() {
    let a_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get("rn_external_analytics_result_for_applied_renarration", function(val) {
        if (val.rn_external_analytics_result_for_applied_renarration) {
          resolve(val.rn_external_analytics_result_for_applied_renarration);
        } else {
          reject("rn_external_analytics_result_for_applied_renarration doesn't exist");
        }
      });

    });

    return await a_promise;
  }

}

import React, { FunctionComponent} from "react";


type ProfileInterface = {
  renarrator_picture?: string,
  renarrator_name?: string,
  renarrator_webId?: string,
  renarrator_role?: string,
  renarrator_mail?: string,
}

let profile_jpg = chrome.runtime.getURL("img/defaultprofile.svg");

export const Profile: FunctionComponent<ProfileInterface> = ({
  renarrator_picture = profile_jpg,
  renarrator_name,
  renarrator_webId,
  renarrator_role,
  renarrator_mail,
}) => {
  const ContentGroup = ({ keyName = "Default", value }: any) => {
    return <div className="rn-general-content-label-value-group">
      <div className="rn-general-content-label-name">
        <p id="label-name">{keyName}</p>
      </div>
      <div className="rn-general-content-value-name">
        <p className="slide-to-right-hover" id="value-name">{value}</p>
      </div>
    </div>
  };

  return <>
    <input type="image" src={renarrator_picture} className="rn-general-picture" />
    <div id="account-profile" className="rn-general-content-box">
      {renarrator_name ? <ContentGroup keyName="Name" value={renarrator_name} /> : null}
      {renarrator_webId ? <ContentGroup keyName="WebId" value={renarrator_webId} /> : null}
      {renarrator_role ? <ContentGroup keyName="Role" value={renarrator_role} /> : null}
      {renarrator_mail ? <ContentGroup keyName="Email" value={renarrator_mail} /> : null}
    </div>
  </>
}

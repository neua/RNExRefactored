export interface selectedHTMLPropertiesInterface {
  value: string | null;
  xpath: string | null;
  xpathOfNextElementSibling: string | null;
  xpathOfPreviousElementSibling: string | null;
}

export enum ModesEnum {
  menu_toggle_icon = "menu_toggle_icon",
  notification_mode_icon = "notification_mode_icon",
  account_mode_icon = "account_mode_icon",
  reading_mode_icon = "reading_mode_icon",
  writing_mode_icon = "writing_mode_icon",
  recommending_mode_icon = "recommending_mode_icon",
  information_box_icon = "information_box_icon",
  profile_mode_icon = "profile_mode_icon",
  response_mode_icon = "response_mode_icon"
}

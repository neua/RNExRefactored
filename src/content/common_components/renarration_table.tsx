import React from "react";
import RenarrationTableContext from "./renarrationTableContext";
import Renarration from "../renarration_engine/renarrationClass";
import RenarrationTransformation from "../renarration_engine/renarrationTransformationClass";





declare module 'react' {
  interface HTMLAttributes<T> extends AriaAttributes, DOMAttributes<T> {
    // extends React's HTMLAttributes
    rnuniqueid?: string;
  }
}

const GeneralTooltipRow = ({ labelName, valueName }: any): JSX.Element => {

  return (
    <div className="rn-general-content-tooltip-label-value-group">
      <div className="rn-general-content-tooltip-label-name">
        <p id="label-name">{labelName}</p>
      </div>
      <div className="rn-general-content-tooltip-value-name">
        <p id="value-name">{valueName}</p>
      </div>
    </div>
  )
}

const RenarrationRow = ({ rnUniqueId, language, motivation, date, source, renarrator }: any) => {

  return (
    <RenarrationTableContext.Consumer>
      {value => (
        <>
          <tr rnuniqueid={rnUniqueId} className={value.tableProperties && value.tableProperties.choosenRenarrationRow && rnUniqueId && value.tableProperties.choosenRenarrationRow === rnUniqueId ? "rn-general-table-tbody-tr-clicked" : ""} onClick={() => { value.onRenarrationRowClick(rnUniqueId) }}>
            <td>
              <p>{language}</p>
            </td>
            <td>
              <p>{motivation}</p>
            </td>
            <td>
              <p>{date}</p>
            </td>
            <td>
              <p>{source}</p>
            </td>
          </tr>
          <div id="" className="rn-general-content-tooltip-box rn-general-tooltip">
            <GeneralTooltipRow key="1" labelName="Language" valueName={language} />
            <GeneralTooltipRow key="2" labelName="Motivation" valueName={motivation} />
            <GeneralTooltipRow key="3" labelName="Date" valueName={date} />
            <GeneralTooltipRow key="4" labelName="Source" valueName={source} />
            <GeneralTooltipRow key="5" labelName="Renarrator" valueName={renarrator} />
          </div>
        </>)}
    </RenarrationTableContext.Consumer>)
};

const RenarrationTableBody = ({ renarrations }: { renarrations: Array<Renarration> }) => {


  return (<>
    <tbody id="account-table" className="rn-general-table-tbody rn-table-column-size">
      {renarrations ? renarrations.map((renarration: Renarration) => {
        let { renarratedBy, onSourceDocument, motivation, createdAt, renarrationUri, renarrationTransformations } = renarration;

        let motivationArray = [];
        let motivationText = "";
        
        if (motivation) {
          for (const [motivationType, booleanValue] of Object.entries(motivation)) {
            if (booleanValue) {
              motivationArray.push(motivationType);
            }
          }
          motivationText = motivationArray.join(",");
        }

        return <RenarrationRow key={renarrationUri} rnUniqueId={renarrationUri} language="En" motivation={motivationText} date={createdAt} source={onSourceDocument} renarrator={renarratedBy} />
      }) : ""}
    </tbody>
  </>);
};

const RenarrationTableHead = ({ }: {}) => {

  return (<>
    <thead className="rn-general-table-thead rn-table-column-size">
      <tr className="rn-renarration-list-items-info">
        <th>
          <p>Language</p>
        </th>
        <th>
          <p>Motivation</p>
        </th>
        <th>
          <p>Date</p>
        </th>
        <th>
          <p>Source</p>
        </th>
      </tr>
    </thead>
  </>);
};

const RenarrationTableLoading = ({ visible }: { visible: boolean }) => {

  return (<>
    <div id="loading-animation-table" className={`table-loading-animation ${visible ? "" : "table-loading-animation-display-none"}`}>
      <div className="circle-loader"></div>
    </div>
  </>);
};

const RenarrationTableFooter = ({ }: {}) => {

  //  const writingContext = useContext(WritingModeContext);

  return (<>
    <RenarrationTableContext.Consumer>
      {value => (
        <tfoot className="rn-general-table-tfooter">
          <tr>
            <td>
              <input type="button" className="rn-general-table-tfooter-buttons" id="go-to-previous-table-contents" defaultValue="<" />
            </td>
            <td>
              <input type="button" className="rn-general-table-tfooter-buttons" id="go-to-next-table-contents" defaultValue=">" />
            </td>
            <td>
              <input type="button" className="rn-general-table-tfooter-apply-button" id="go-and-apply-the-new-renarration" defaultValue="Apply" onClick={() => { value.onRenarrationApply("test") }} />
            </td>
            <td>
              <input type="text" id="table-current-page" defaultValue="1" className="rn-general-table-tfooter-number-box" />
              <input type="text" id="table-total-page" defaultValue="1" className="rn-general-table-tfooter-number-box" />
            </td>
          </tr>
        </tfoot>
      )}
    </RenarrationTableContext.Consumer>
  </>);
};


export const RenarrationTable = ({ renarrations,
  tableProperties, handleRenarrationApply, handlePageChange, handleRenarrationRowClick }: any) => {

  return (
    <RenarrationTableContext.Provider
      value={{
        renarrations: renarrations,
        tableProperties: tableProperties,
        onRenarrationApply: handleRenarrationApply,
        onPageChange: handlePageChange,
        onRenarrationRowClick: handleRenarrationRowClick
      }}
    >
      <div className="rn-general-table">
        <table className="rn-general-table-overflow rn-general-table-scroll">
          <RenarrationTableHead />
          <RenarrationTableBody renarrations={renarrations} />
          <RenarrationTableFooter />
        </table>
        <RenarrationTableLoading visible={false} />

      </div>

    </RenarrationTableContext.Provider>
  )

}

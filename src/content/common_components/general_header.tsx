import React from "react";



export const GeneralHeader = ({ header }: any) => {
  return (
    <div className="rn-general-content-box">
      <div className="rn-general-header">
        <h4>{header}</h4>
      </div>
    </div>
  )
}

export const CurrentMode = ({ currentMode }: any) => {
  return (
    <div className="rn-current-mode-content-box">
      <div>
        <p>{currentMode}</p>
      </div>
    </div>
  )
}

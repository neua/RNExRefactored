
import * as React from "react";
import Renarration from "../renarration_engine/renarrationClass";

interface IRenarrationTable {
  renarrations: Array<Renarration>,
  onRenarrationApply: (val: any) => void,
  onPageChange: (val: any) => void,
  onRenarrationRowClick: (val: any) => void,
  tableProperties: any
}

const RenarrationTableContext = React.createContext<IRenarrationTable>(null);
RenarrationTableContext.displayName = "RenarrationTableContext";

export default RenarrationTableContext;

import * as React from "react";
import { RenarrationTable } from "./common_components/renarration_table";
import { CurrentMode, GeneralHeader } from "./common_components/general_header";
import Renarration from "./renarration_engine/renarrationClass";
import { RenarrationExhibition } from "./renarration_engine/renarration_exhibition";
import { Test_A_RN } from "../test/renarration"
// interface IReadingMode {
//   renarrations: Array<Renarration>,
//   onRenarrationApply: (text: string) => void,
//   onPageChange: (text: string) => void
// }
//
// const ReadingModeContext = React.createContext<IReadingMode>(null);
// ReadingModeContext.displayName = "ReadingModeContext";

interface IProps {
}

export class ReadingMode extends React.Component<any, any> {
  engine: any;

  constructor(props: IProps) {
    super(props);
    this.engine = new RenarrationExhibition(document);
    //let selectedHtmlPart: string = "engine.selectedHtml";
    this.state = {
      renarrations: [Test_A_RN],
      tableProperties: {
        choosenRenarrationRow: null,
        currentPage: null,
        totalPage: null,
        numberOfRows: null
      }
    }
  }

  handleRenarrationApply = (val: any) => {
    console.log("handleRenarrationApply success", val);

    let selectedRenarration = this.state.renarrations.find((renarrationItem: any) => renarrationItem.renarrationUri === this.state.tableProperties.choosenRenarrationRow);

    this.engine.applyRenarrationToTheDom(selectedRenarration);
  };

  handlePageChange = (val: any) => {
    console.log("handlePageChange success", val);
  };
  handleRenarrationRowClick = (renarrationUniqueId: any) => {
    console.log("handleRenarrationRowClick success", renarrationUniqueId);
    let tableProperties: any = {
      choosenRenarrationRow: renarrationUniqueId,
      currentPage: null,
      totalPage: null,
      numberOfRows: null
    };

    this.setState({ ...this.state, tableProperties });
  };
  render() {
    let state: any = { ...this.state };

    return (
      <div className="rn-main-mode-reading">
        <CurrentMode currentMode={"My Profile"} />
        <GeneralHeader header={"Renarrations"} />
        <RenarrationTable
          renarrations={state.renarrations}
          handleRenarrationApply={this.handleRenarrationApply} handlePageChange={this.handlePageChange}
          handleRenarrationRowClick={this.handleRenarrationRowClick}
          tableProperties={state.tableProperties}
        />
      </div>
    )
  }
}

import * as React from "react";
import { useState } from "react";

const InformationBoxRow = ({ label, value, link = null }: { label: string, value: string, link: boolean }) => {
  return (
    <div className="rn-general-content-label-value-group">
      <div className="rn-general-content-label-name">
        <p>{label}</p>
      </div>
      {link ?
        <div className="rn-general-content-value-name">
          <a href={value} target="_blank" className="general-clickable-url">
            <p className="slide-to-right-hover">{value}</p>
          </a>
        </div>
        :
        <div className="rn-general-content-value-name">
          <p className="slide-to-right-hover">{value}</p>
        </div>
      }
    </div>)
}

export const InformationBox = ({ menuVisible }: { menuVisible: boolean }) => {
  const [active, setActive] = useState(false);
  return (

    <>
      <div id="information-of-current-renarration-box" className={`rn-renarration-information-box ${menuVisible ? '' : 'rn-information-box-menu-closed'}`}>
        <InformationBoxRow label="Renarrator" value="https://gwen-therenarrator.inrupt.net/profile/card#me" link={true} />
        <InformationBoxRow label="Date" value="2021-02-11T05:47:06.389Z" link={true} />
        <InformationBoxRow label="Source" value="https://neua.gitlab.io/renarration-test-cases/multiple_actions_example" link={false} />
        <InformationBoxRow label="Motivation" value="Alteration" link={false} />
        <InformationBoxRow label="Target" value="https://gwen-therenarrator.inrupt.net/renarration/RenarrationFiles/RNCNT-kkkheiqs-5f4jb4vq-498xobja.ttl" link={true} />
        <input type="button" className="rn-general-searchbox-button" id="show_more_information" value="More Information" onClick={() => setActive(prevActive => !prevActive)} />
      </div>
      {active ? <MoreInformationBox menuVisible={menuVisible} /> : null}
    </>
  )
}


const MoreInformationBoxRow = ({ label, value }: { label: string, value: string }) => {
  return (
    <div className="rn-general-content-label-value-group">
      <div className="rn-general-content-label-name">
        <p>{label}</p>
      </div>
      <div className="rn-general-content-value-name">
        <a target="_blank" className="general-clickable-url">
          <p>{value}</p>
        </a>
      </div>
    </div>

  )
}

const MoreInformationBox = ({menuVisible}: { menuVisible: boolean }) => {
  return (
    <div id="more-information-for-current-renarration-box" className={`rn-renarration-more-information-box ${menuVisible ? '' : 'rn-information-box-menu-closed'}`}>
      <div className="rn-general-centered-content-label-name">
        <p>Concepts</p>
      </div>
      <MoreInformationBoxRow label="Top&gt;LivingThing&gt;Animal&gt;Vertebrate&gt;Mammal" value="dog" />
      <div className="rn-general-centered-content-label-name">
        <p>Entities</p>
      </div>
      <MoreInformationBoxRow label="Top" value="Foobar Municipality" />
    </div>
  )
}

import * as React from "react";
import { useState } from "react";
import { CurrentMode, GeneralHeader } from "./common_components/general_header"
import { uparrow_icon, downarrow_icon, triangle_icon } from './icons';


const GeneralResponseBox = ({ comment, date, userWebID }: { comment: string, date: string, userWebID: string }) => {

  const [hide, setHide] = useState(false);

  return (
    <div className={`rn-general-response-box ${hide ? 'rn-response-display-none' : ''}`}>
      <div className="rn-general-response-box-header">
        <p className="rn-general-response-box-header-overflow">{userWebID}</p>
        <p className="rn-general-response-box-header-overflow rn-general-response-rate-box-body-dark-color-time-text">{date}</p>
        <div className="rn-updown-group" onClick={() => setHide(prevHide => !prevHide)}>
          {hide ? <input type="image" src={uparrow_icon} className="" /> :
            <input type="image" src={downarrow_icon} className="" />}
        </div>
      </div>
      <div className={`rn-general-response-box-body ${hide ? 'rn-response-display-slide-to-none' : ''}`}>
        <p>{comment}</p>
      </div>
    </div>
  )
}

const RateBoxRow = ({ userWebID, rate }: { userWebID: string, rate: number }) => {

  return (
    <div className="rn-general-response-rate-box-body-text-group">
      <p className="rn-general-response-rate-box-header-overflow rn-general-response-rate-box-body-dark-color-text">{userWebID}</p>
      <p className="rn-general-response-rate-box-header-overflow rn-general-response-rate-box-body-light-color-text">{rate}</p>
    </div>
  )
}

const ResponseRateBox = ({ amountValue, meanValue }: { amountValue: number, meanValue: number }) => {

  //const [hide, setHide] = useState(false);

  return (
    <div className="rn-general-response-rate-box">
      <div className="rn-general-response-rate-box-header">
        <p className="rn-general-response-rate-box-header-title">Rate</p>
        <p className="rn-general-response-rate-box-header-overflow rn-general-response-rate-box-header-dark-color-text">Amount</p>
        <p className="rn-general-response-rate-box-header-overflow rn-general-response-rate-box-header-light-color-text" id="number_of_created_rates">{amountValue}</p>
        <p className="rn-general-response-rate-box-header-overflow rn-general-response-rate-box-header-dark-color-text">Mean</p>
        <p className="rn-general-response-rate-box-header-overflow rn-general-response-rate-box-header-light-color-text" id="rate_mean_value">{meanValue}</p>
        {/* <div className="rn-updown-group" >
          {hide ? <input type="image" src={uparrow_icon} className=""  /> :
            <input type="image" src={downarrow_icon} className="" />}
        </div> */}
        <div className="rn-updown-rate-icon-group">
          <input type="image" src={uparrow_icon} className="rn-rate-icon-down" />
          <input type="image" src={downarrow_icon} className="rn-rate-icon-up" />
        </div>
      </div>
      <div className="rn-general-response-rate-box-body">
        <RateBoxRow userWebID="https://gwen-therenarrator.inrupt.net/profile/card#me" rate={5} />
        <RateBoxRow userWebID="https://gwen-therenarrator.inrupt.net/profile/card#me" rate={2} />
      </div>
    </div>
  )
}


const RateTheCurrent = ({ }: any) => {
  return (
    <div className="rn-general-content-box">
      <div className="rn-rate-group">
        <input type="image" src={triangle_icon} />
        <input type="image" src={triangle_icon} />
        <input type="image" src={triangle_icon} />
        <input type="image" src={triangle_icon} />
        <input type="image" src={triangle_icon} />
      </div>
      <input type="button" className="rn-general-searchbox-button" id="save_the_rate" value="Rate" />
    </div>
  )
}
const CommentTheCurrent = ({ }: any) => {
  return (
    <div className="rn-general-content-box">
      <div className="rn-general-header">
        <h4>Response</h4>
      </div>
      <textarea id="rn_transformation_text" className="rn-general-textarea"></textarea>
      <input type="button" className="rn-general-searchbox-button" id="save_the_comment" value="Comment" />
    </div>
  )
}

export class ResponseMode extends React.Component {
  render() {
    return (
      <div className="rn-main-mode-response">
        <CurrentMode currentMode={"Response Mode"} />
        <div className="rn-general-response-container">
          <div className="rn-general-response-container-header">
            <h4>Response</h4>
          </div>
          <ResponseRateBox amountValue={3} meanValue={5.00} />
          <div className="rn-general-response-container-body">
            <div className="rn-general-response-container-body-comments">
              <GeneralResponseBox comment="Hey" date="2021-01-31T01:50:26.980Z" userWebID="https://mete-therenarrator.inrupt.net/profile/card#me" />
              <GeneralResponseBox comment="Hey" date="2021-01-31T01:50:26.980Z" userWebID="https://jane-therenarrator.inrupt.net/profile/card#me" />
              <GeneralResponseBox comment="Hey" date="2021-01-31T01:50:26.980Z" userWebID="https://gwen-therenarrator.inrupt.net/profile/card#me" />
            </div>
          </div>
        </div>
        <RateTheCurrent />
        <CommentTheCurrent />
      </div>
    )
  }
}

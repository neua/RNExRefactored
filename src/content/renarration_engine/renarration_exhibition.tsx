
import Renarration from "./renarrationClass";

import RenarrationTransformation from "./renarrationTransformationClass";


export class RenarrationExhibition {

  document: any;

  constructor(document: any) {
    this.document = document;
  }


  applyRenarrationToTheDom = (renarration: any) => {
    let renarrationTransformations = renarration.renarrationTransformations;
    renarrationTransformations.map((rt: any) => {
      this.xpathEvaluation(rt);
    })
  }

  documentEvaluateXpath = (xpath_string: string) => {
    let document = this.document;
    let node_of_xpath_target = null;
    let xpath_evaluation = document.evaluate(xpath_string, document, null, XPathResult.ANY_TYPE, null);

    let xpath_iteration_this_node = xpath_evaluation.iterateNext();
    while (xpath_iteration_this_node) {

      node_of_xpath_target = xpath_iteration_this_node;

      xpath_iteration_this_node = xpath_evaluation.iterateNext();
    }

    return node_of_xpath_target;
  }

  xpathEvaluation = (renarrationTransformation: RenarrationTransformation) => {
    let action = renarrationTransformation.action;
    let selectedHTMLProperties = renarrationTransformation.selectedHTMLProperties;
    let documentEvaluateXpath = this.documentEvaluateXpath;
    let content = renarrationTransformation.narration;

    if (action === "insertBefore" || action === "insertAfter") {
      let rangeSelectorXpathStartSelector = selectedHTMLProperties.xpathOfPreviousElementSibling;
      let rangeSelectorXpathEndSelector = selectedHTMLProperties.xpath;

      if (action === "insertBefore") {
        rangeSelectorXpathStartSelector = selectedHTMLProperties.xpathOfPreviousElementSibling;
        rangeSelectorXpathEndSelector = selectedHTMLProperties.xpath;
      } if (action === "insertAfter") {
        rangeSelectorXpathStartSelector = selectedHTMLProperties.xpath;
        rangeSelectorXpathEndSelector = selectedHTMLProperties.xpathOfNextElementSibling;
      }


      if (rangeSelectorXpathStartSelector) {
        let refCurrentChosenNode = documentEvaluateXpath(rangeSelectorXpathStartSelector);

        if (refCurrentChosenNode) {
          let refCurrentChosenNodeOuterHTML = refCurrentChosenNode.outerHTML;
          refCurrentChosenNode.outerHTML = refCurrentChosenNodeOuterHTML + content;
        } else {
          console.log("error, renarrationTransformation", renarrationTransformation);
        }

      } else if (rangeSelectorXpathEndSelector) {
        let refCurrentChosenNode = documentEvaluateXpath(rangeSelectorXpathEndSelector);

        if (refCurrentChosenNode) {
          let refCurrentChosenNodeOuterHTML = refCurrentChosenNode.outerHTML;
          refCurrentChosenNode.outerHTML = content + refCurrentChosenNodeOuterHTML;
        } else {
          console.log("error, renarrationTransformation", renarrationTransformation);
        }

      }

    } else if (action === "replace") {
      let xpath = selectedHTMLProperties.xpath;
      let xpath_string = xpath;
      let xpath_content = content;

      if (documentEvaluateXpath(xpath_string)) {
        documentEvaluateXpath(xpath_string).outerHTML = xpath_content;

      } else {
        console.log("error in documentEvaluateXpath for the hasAction replace xpath string:", xpath_string, "renarrationTransformation", renarrationTransformation);
      }

    } else if (action === "remove") {
      let xpath = selectedHTMLProperties.xpath;
      let xpath_string = xpath;

      if (documentEvaluateXpath(xpath_string)) {
        documentEvaluateXpath(xpath_string).remove();
      } else {
        console.log("error in documentEvaluateXpath for the hasAction remove xpath string:", xpath_string, "renarrationTransformation", renarrationTransformation);
      }
    }

  }

}

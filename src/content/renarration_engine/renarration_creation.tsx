import * as React from 'react';
import { RichTextAreaModeEnum } from "../writing_mode/abstract/richTextAreaModeEnum";
import { selectedHTMLPropertiesInterface } from "../common_components/abstracts";

import { IWritingModeState } from "../writing_mode/abstract/writingModeStateInterface";

import { ActionTypeEnum } from "../writing_mode/abstract/ActionTypeEnum";

import Renarration from "./renarrationClass";

import RenarrationTransformation from "./renarrationTransformationClass";

export const EventEmitter = {
  _events: {},
  dispatch: function(event: any, data: any) {
    if (!this._events[event]) return;
    this._events[event].forEach((callback: any) => callback(data))
  },
  subscribe: function(event: any, callback: any) {
    if (!this._events[event]) this._events[event] = [];
    this._events[event].push(callback);
  },
  unsubscribe(event: any) {
    if (!this._events[event]) return;
    delete this._events[event];
  }
}


export class ModificationOfDom {

  document: any;

  constructor(document: any) {
    this.document = document;

  }

  hovered_target_html_node: any;
  clicked_target_html_node: any;
  pressedKeys: any = "";
  renarrationTransformations: Array<RenarrationTransformation> = [];

  main = () => {
    this.document.querySelector("body").setAttribute("rnex_body", "rn_writing_mode");
    this.document.querySelector("[rnex_body=rn_writing_mode]").addEventListener("mouseover", this.mouseover_operations);
    this.document.querySelector("[rnex_body=rn_writing_mode]").addEventListener("click", this.click_operations);
    // this.document.addEventListener("selectionchange", this.clickandchane(this.clicked_target_html_node));
  }


  unsubscribeEvents = () => {
    this.document.querySelector("[rnex_body=rn_writing_mode]").removeEventListener("mouseover", this.mouseover_operations);
    this.document.querySelector("[rnex_body=rn_writing_mode]").removeEventListener("click", this.click_operations);

    this.document.querySelector("body").removeAttribute("rnex_body", "rn_writing_mode");

    this.mutateAndSanitizeTheChosenHTML(this.hovered_target_html_node);
    this.mutateAndSanitizeTheChosenHTML(this.clicked_target_html_node);
  }

  mouseover_operations = (e: any) => {
    if (e.target.id !== "RNEx" && e.target.tagName !== "BODY") {
      if (this.hovered_target_html_node) {
        this.hovered_target_html_node.removeAttribute("rn-hovered-class", "rn-hovered-html-node");
      }
      this.hovered_target_html_node = e.target;
      this.hovered_target_html_node.setAttribute("rn-hovered-class", "rn-hovered-html-node");
    } else if (e.target.tagName === "BODY") {
      // INFO - If user hovers a empty area chosen element is removed
      if (this.hovered_target_html_node) {
        this.hovered_target_html_node.removeAttribute("rn-hovered-class", "rn-hovered-html-node");
      }
      this.hovered_target_html_node = null;
    }
  }

  click_operations = (e: any) => {
    if (e.target.id !== "RNEx" && e.target.tagName !== "BODY") {
      if (this.clicked_target_html_node) {
        this.clicked_target_html_node.removeAttribute("rn-class", "rn-chosen-html-node");
      }
      this.clicked_target_html_node = e.target;
      this.clicked_target_html_node.setAttribute("rn-class", "rn-chosen-html-node");

      if (e.target.hasAttribute("href")) {
        e.preventDefault();
      }
      EventEmitter.dispatch("click_operation", this.selectedHtml());
    }
    else if (e.target.tagName === "BODY") {
      // INFO - If user clicks a empty area chosen element is removed
      if (this.clicked_target_html_node) {
        this.clicked_target_html_node.removeAttribute("rn-class", "rn-chosen-html-node");
      }
      this.clicked_target_html_node = null;
      let sendEmptySelectedHTMLProperties = true;
      EventEmitter.dispatch("click_operation", this.selectedHtml(sendEmptySelectedHTMLProperties));
    }
  }

  mutateAndSanitizeTheChosenHTML = (currentChosenTarget: any) => {
    if (currentChosenTarget) {
      currentChosenTarget.removeAttribute("rn-class", "rn-chosen-html-node");
      currentChosenTarget.removeAttribute("rn-hovered-class", "rn-hovered-html-node");
    }
    return currentChosenTarget;
  }

  sanitizeTheChosenHTML = (currentChosenTarget: any) => {
    let clonedCurrentChosenTargetNode = currentChosenTarget.cloneNode();
    clonedCurrentChosenTargetNode.removeAttribute("rn-class", "rn-chosen-html-node");
    clonedCurrentChosenTargetNode.removeAttribute("rn-hovered-class", "rn-hovered-html-node");
    let clonedCurrentInnerHTML = currentChosenTarget.innerHTML;
    clonedCurrentChosenTargetNode.innerHTML = clonedCurrentInnerHTML;

    return clonedCurrentChosenTargetNode;
  }

  selectedHtml = (sendEmptySelectedHTMLProperties?: boolean) => {

    let selectedHTMLProperties: selectedHTMLPropertiesInterface = {
      value: null,
      xpath: null,
      xpathOfNextElementSibling: null,
      xpathOfPreviousElementSibling: null
    };

    let current_chosen_target = this.clicked_target_html_node;

    if (sendEmptySelectedHTMLProperties || !current_chosen_target) return selectedHTMLProperties;

    let sanitizedCurrentChosenTargetNode = this.sanitizeTheChosenHTML(current_chosen_target);

    selectedHTMLProperties.value = sanitizedCurrentChosenTargetNode.outerHTML;
    selectedHTMLProperties.xpath = this.xpathGeneratorRenarrationContent(current_chosen_target);
    if (current_chosen_target.nextElementSibling) {
      selectedHTMLProperties.xpathOfNextElementSibling =
        this.xpathGeneratorRenarrationContent(current_chosen_target.nextElementSibling);
    }
    if (current_chosen_target.previousElementSibling) {
      selectedHTMLProperties.xpathOfPreviousElementSibling =
        this.xpathGeneratorRenarrationContent(current_chosen_target.previousElementSibling);
    }

    return selectedHTMLProperties;
  }

  click_and_change = () => {

    this.document.addEventListener('keypress', (e: any) => {

      this.pressedKeys += e.key;
      //  node.textContent = sadfsdf;
      console.log("pressedKeys", this.pressedKeys);
    });


    this.document.addEventListener('selectionchange', () => {
      let focusOffset = this.document.getSelection().focusOffset;
      let anchorOffset = this.document.getSelection().anchorOffset;
      let textFromBeginningToAnchor = this.document.getSelection().focusNode.textContent.substring(0, anchorOffset);

      let textFromAnchorToEnd = this.document.getSelection().focusNode.textContent.substring(focusOffset);
      let selectedText = this.document.getSelection().toString();
      let allTheTextofTheNode = textFromBeginningToAnchor + selectedText + textFromAnchorToEnd;
      let allTheTextofTheNodeWithThePressedKeys = textFromBeginningToAnchor + this.pressedKeys + textFromAnchorToEnd;
      //console.log("allTheTextofTheNode", allTheTextofTheNode);

      //console.log("allTheTextofTheNodeWithThePressedKeys", allTheTextofTheNodeWithThePressedKeys);
      this.document.getSelection().focusNode.textContent = this.pressedKeys
    });
  }

  xpathGeneratorRenarrationContent = (element: any): string => {
    if (element === this.document.body)
      return '/html[1]/' + element.tagName.toLowerCase() + '[1]';

    var ix = 0;
    var siblings = element.parentNode.childNodes;

    for (var i = 0; i < siblings.length; i++) {
      var sibling = siblings[i];
      if (sibling === element)
        return this.xpathGeneratorRenarrationContent(element.parentNode) + '/' + element.tagName.toLowerCase() + '[' + (ix + 1) + ']';
      if (sibling.nodeType === 1 && sibling.tagName === element.tagName)
        ix++;
    }
  }

  createHtmlForRenarrationTransformation = (mode: any, text: string, contentProperties: any) => {
    let savedHtmlForRenarrationTransformation = null;
    if (mode === RichTextAreaModeEnum.text) {
      savedHtmlForRenarrationTransformation = this.textHtmlCreator(text, contentProperties.size, contentProperties.color, contentProperties.bold, contentProperties.underline, contentProperties.italic);
    }
    else if (mode === RichTextAreaModeEnum.image) {
      savedHtmlForRenarrationTransformation = this.imageHtmlCreator(text, contentProperties.width, contentProperties.height);
    }
    else if (mode === RichTextAreaModeEnum.audio) {
      savedHtmlForRenarrationTransformation = this.audioHtmlCreator(text);
    }
    else if (mode === RichTextAreaModeEnum.video) {
      savedHtmlForRenarrationTransformation = this.videoHtmlCreator(text, contentProperties.width, contentProperties.height);
    }
    else if (mode === RichTextAreaModeEnum.html) {
      savedHtmlForRenarrationTransformation = text;
    }
    return savedHtmlForRenarrationTransformation;
  }

  modifyDomDependingOnActionType = (actionType: ActionTypeEnum, selectedHTMLProperties: selectedHTMLPropertiesInterface, narrationHtml: string) => {
    let clickedOuterHtml = selectedHTMLProperties.value;

    if (actionType === ActionTypeEnum.replace) {
      clickedOuterHtml = narrationHtml;

    } else if (actionType === ActionTypeEnum.remove) {
      clickedOuterHtml = "";

    } else if (actionType === ActionTypeEnum.insertBefore) {
      clickedOuterHtml = narrationHtml + clickedOuterHtml;

    } else if (actionType === ActionTypeEnum.insertAfter) {
      clickedOuterHtml = clickedOuterHtml + narrationHtml;
    }
    return clickedOuterHtml;
  }

  createRenarrationTransformation = (rt: any) => {
    console.log("createRenarrationTransformation", rt);
    let { mode, text, contentProperties } = rt;
    let clickedOuterNode = this.clicked_target_html_node;
    if (clickedOuterNode && clickedOuterNode.outerHTML) {
      let narrationHtml = this.createHtmlForRenarrationTransformation(mode, text, contentProperties);

      clickedOuterNode.outerHTML = this.modifyDomDependingOnActionType(rt.action, rt.selectedHTMLProperties, narrationHtml);

      let RT = new RenarrationTransformation(this.renarrationTransformations.length, rt.language, rt.mode, rt.action, narrationHtml, rt.selectedHTMLProperties, new Date().toISOString());

      this.renarrationTransformations.push(RT);
      console.log("RT", RT);

      //INFO - after the DOM HTML change applied null the clicked node
      clickedOuterNode = null;
    } else {
      console.log("ERROR this.clicked_target_html_node.outerHTML");
    }

  }

  createRenarration = (rn: IWritingModeState) => {
    let renarrationTransformations = this.renarrationTransformations;
    let renarrationCreationDate = new Date();
    let renarrationCreationDateToISOString = renarrationCreationDate.toISOString()
    let timeToString36 = renarrationCreationDate.getTime().toString(36)
    let renarrationUri = this.renarrationUniqueIdGenerator(timeToString36);
    let onSourceDocument = window.location.href;
    let motivation = rn.motivation;
    let RN = new Renarration(renarrationUri, renarrationCreationDateToISOString, onSourceDocument, "https://bob-therenarrator.inrupt.net", motivation, renarrationTransformations);
    console.log("RN", RN);
  }

  renarrationUniqueIdGenerator = (timeToString36: string) => {
    let randomNoToString36First = Math.round((Math.random() * 36 ** 8)).toString(36);
    let randomNoToString36Second = Math.round((Math.random() * 36 ** 8)).toString(36);
    let uniqueIdString36 = timeToString36 + "-" + randomNoToString36First + "-" + randomNoToString36Second;
    return uniqueIdString36;
  }

  textHtmlCreator = (text: string = "", size?: string, color?: string, bold?: boolean, underline?: boolean, italic?: boolean): string => {
    let textHTML = `<p style='${size ? "font-size:" + size + "px;" : ""} ${color ? "color:" + color + ";" : ""} ${bold ? "font-weight:bold;" : ""} ${underline ? "text-decoration-line:underline;" : ""} ${italic ? "font-style:italic;" : ""}'>${text}</p>`;
    return textHTML;
  }

  audioHtmlCreator = (audioLink: string): string => {
    let audioHTML = `<audio controls><source src=${audioLink} type="audio/ogg"><source src=${audioLink} type="audio/mpeg">Your browser does not support the audio element.</audio>`;
    return audioHTML;
  }

  imageHtmlCreator = (imageLink: string, width?: string, height?: string): string => {
    let imageHTML = `
    <img  src=${imageLink} ${width ? "width=" + width + ";" : ""} ${height ? "height=" + height + ";" : ""}>`;
    return imageHTML;
  }

  videoHtmlCreator = (videoLink: string, width?: string, height?: string): string => {
    let regex;
    let val;
    if (videoLink.includes("watch")) {
      regex = /[^\?=\v]+$/g;
      val = videoLink.match(regex);
    } else {
      regex = /[^\/]+$/g;
      val = videoLink.match(regex);
    }

    let video = `https://www.youtube.com/embed/` + val;
    let videoHTML = `
      <iframe  src=${video} ${width ? "width=" + width + ";" : ""} ${height ? "height=" + height + ";" : ""}
      frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;
    return videoHTML;
  }
}

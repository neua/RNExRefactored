import { IMotivationType } from "../writing_mode/abstract/motivationTypeInterface";

import RenarrationTransformation from "./renarrationTransformationClass";


export default class Renarration {
  renarrationUri: string;
  createdAt: string;
  onSourceDocument: string;
  renarratedBy: string;
  motivation: IMotivationType;
  renarrationTransformations: Array<RenarrationTransformation>;

  constructor(renarrationUri: string, createdAt: string, onSourceDocument: string, renarratedBy: string, motivation: IMotivationType, renarrationTransformations: Array<RenarrationTransformation>) {
    this.renarrationUri = renarrationUri;
    this.createdAt = createdAt;
    this.onSourceDocument = onSourceDocument;
    this.renarratedBy = renarratedBy;
    this.motivation = motivation;
    this.renarrationTransformations = renarrationTransformations;
  }

}

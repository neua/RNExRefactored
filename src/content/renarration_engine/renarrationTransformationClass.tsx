
import { selectedHTMLPropertiesInterface } from "../common_components/abstracts";

export default class RenarrationTransformation {

  id: number;
  language: string;
  action: string;
  mediaType: string;
  narration: string;
  selectedHTMLProperties: selectedHTMLPropertiesInterface;
  createdAt: string;

  constructor(id: number, language: string, mediaType: string, action: string, narration: string, selectedHTMLProperties: selectedHTMLPropertiesInterface, createdAt: string) {
    this.id = id;
    this.language = language;
    this.action = action;
    this.narration = narration;
    this.mediaType = mediaType;
    this.selectedHTMLProperties = selectedHTMLProperties;
    this.createdAt = createdAt;
  }

}

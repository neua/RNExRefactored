import 'react-devtools'  //Remove this after erything is done
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { AccountMode } from './account_mode';
import { ProfileMode } from './profile_mode';
import { NotificationMode } from './notification_mode';
import { ResponseMode } from './response_mode';
import { SearchAndRecommendingMode } from './search_and_recommending_mode';
import { WritingMode } from './writing_mode/writing_mode';
import { ReadingMode } from './reading_mode';
import { InformationBox } from './information_part';
import { RnWholeWindowWrap } from './renarration_engine/whole_window_wrap';
import {
  menu_toggle_icon,
  notification_mode_icon,
  account_mode_icon,
  reading_mode_icon,
  writing_mode_icon,
  recommending_mode_icon,
  information_box_icon,
  profile_mode_icon,
  response_mode_icon
} from './icons';
import { ModesEnum } from './common_components/abstracts';

import { RnCss } from './rncss'

export class Login extends React.Component {
  render() {
    return (

      <div className="rn-login-outer-main-wrap">
        <div className="rn-login-general-content-box">
          <h4>Login RNEx with a Solid account </h4>
        </div>
        <div className="rn-login-general-content-box">
          <input onClick={() => { window.open('https://inrupt.net/register') }} type="button" className="rn-general-searchbox-button-second-type" value="Register" />
          <input id="rn-login-button" type="button" className="rn-general-searchbox-button-second-type" value="Login" />
        </div>
      </div>

    )
  }
}



export class RnOuterWrap extends React.Component<{}, any>{
  constructor(props: any) {
    super(props);
    this.state = {
      currentMode: null,
      isInformationBoxActive: false,
      isMenuVisible: true
    }
  }
  setVisibilityOfMenu: any = (visible: boolean) => {
    this.setState({ isMenuVisible: visible });
  }
  setCurrentMode: any = (mode: ModesEnum) => {
    this.setState({ currentMode: mode });
  }
  setInformationBox = (active: boolean) => {
    this.setState({ isInformationBoxActive: active });
  }
  renderCurrentMode = (expression: ModesEnum) => {
    switch (expression) {
      case ModesEnum.account_mode_icon:
        return <AccountMode />
      case ModesEnum.reading_mode_icon:
        return <ReadingMode />
      case ModesEnum.writing_mode_icon:
        return <WritingMode />
      case ModesEnum.recommending_mode_icon:
        return <SearchAndRecommendingMode />
      case ModesEnum.notification_mode_icon:
        return <NotificationMode />
      case ModesEnum.menu_toggle_icon:
        return "test";

      case ModesEnum.profile_mode_icon:
        return <ProfileMode />
      case ModesEnum.response_mode_icon:
        return <ResponseMode />
      default:
        return <AccountMode />;
    }
  }

  render() {
    // let menu_toggle_icon = chrome.runtime.getURL("img/OwnedIcons/menuicon.svg");
    // let recommending_mode_icon = chrome.runtime.getURL("img/OwnedIcons/search.svg");
    // let notification_mode_icon = chrome.runtime.getURL("img/OwnedIcons/notification.svg");
    // let account_mode_icon = chrome.runtime.getURL("img/OwnedIcons/myprofile.svg");
    // let reading_mode_icon = chrome.runtime.getURL("img/OwnedIcons/readingbook.svg");
    // let writing_mode_icon = chrome.runtime.getURL("img/OwnedIcons/pencil.svg");
    //
    // let information_box_icon = chrome.runtime.getURL("img/OwnedIcons/information.svg");
    // let response_mode_icon = chrome.runtime.getURL("img/OwnedIcons/response.svg");
    // let profile_mode_icon = chrome.runtime.getURL("img/OwnedIcons/otherprofile.svg");
    //


    return (
      <React.Fragment>
        <div className={`rn-outer-main-wrap ${this.state.isMenuVisible ? '' : 'rn-outer-main-wrap-toggle'}`}>
          <div className="rn-navbar">
            <input type="image" src={menu_toggle_icon} className="rn-navbar-toggle" onClick={() => this.setVisibilityOfMenu(!this.state.isMenuVisible)} />
            <input type="image" src={notification_mode_icon} className="rn-modes-notification" onClick={() => this.setCurrentMode(ModesEnum.notification_mode_icon)} />
            <input type="image" src={account_mode_icon} className="rn-modes-account" onClick={() => this.setCurrentMode(ModesEnum.account_mode_icon)} />
            <input type="image" src={reading_mode_icon} className="rn-modes-reading" onClick={() => this.setCurrentMode(ModesEnum.reading_mode_icon)} />
            <input type="image" src={writing_mode_icon} className="rn-modes-writing" onClick={() => this.setCurrentMode(ModesEnum.writing_mode_icon)} />
            <input type="image" src={recommending_mode_icon} className="rn-modes-recommending" onClick={() => this.setCurrentMode(ModesEnum.recommending_mode_icon)} />
          </div>

          <div className="rn-navbar-vertical">
            <input type="image" src={information_box_icon} className="rn-information-of-current-renarration" rn-navbar-mode-chosen="rn-navbar-mode-selection" onClick={() => this.setInformationBox(!this.state.isInformationBoxActive)} />
            <input type="image" src={profile_mode_icon} className="rn-modes-profile" onClick={() => this.setCurrentMode(ModesEnum.profile_mode_icon)} />
            <input type="image" src={response_mode_icon} className="rn-modes-response" onClick={() => this.setCurrentMode(ModesEnum.response_mode_icon)} />
          </div>

          <div className={`rn-main-wrap ${this.state.isMenuVisible ? '' : 'rn-display-none'}`}>
            {this.renderCurrentMode(this.state.currentMode)}
          </div>
        </div>
        {this.state.isInformationBoxActive ? <InformationBox menuVisible={this.state.isMenuVisible} /> : null}
      </React.Fragment>
    )
  }
}


export class Extension extends React.Component {
  render() {
    return (
      <React.Fragment>
        <script src="http://localhost:8097"></script>
        {/* <link rel="stylesheet" href={chrome.runtime.getURL('rn-design.css')} /> */}
        {/* <div id="rn_whole_window_wrap"></div> */}
        <style>{RnCss}</style>
        <RnWholeWindowWrap />
        <RnOuterWrap />
      </React.Fragment>
    )
  }
}

let div = document.getElementById('RNEx');
if (!div) {
  let shadow = document.createElement('div');
  shadow.setAttribute('id', 'RNEx');

  document.body.appendChild(shadow);
  shadow.attachShadow({
    mode: "open"
  });
  ReactDOM.render(<Extension />, shadow.shadowRoot);
}

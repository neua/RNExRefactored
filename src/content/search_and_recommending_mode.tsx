import * as React from "react";
import { CurrentMode, GeneralHeader } from "./common_components/general_header"
import { RenarrationTable } from "./common_components/renarration_table"

export class SearchAndRecommendingMode extends React.Component {
  render() {
    return (
      <div className="rn-main-mode-recommending">
        <CurrentMode currentMode={"Recommendation & Search Mode"} />
        <GeneralHeader header={"Recommendations"} />
        <RenarrationTable renarrations={[1, 2, 3, 4, 5]} />
        <GeneralHeader header={"Search"} />
        <FilterArea />
        <RenarrationTable renarrations={[1, 2, 3, 4, 5]} />
      </div>

    )
  }
}

const FilterArea = () => {
  return (
    <div className="rn-general-content-box">
      <div >
        <table className="rn-general-searchbox-table">
          <tbody>
            <tr>
              <td>
                <div className="rn-general-searchbox-header">
                  <p>Filter</p>
                </div>
              </td>
              <td><input type="text" className="rn-general-searchbox-textbox" placeholder="Type here..." /></td>
              <td><input type="button" className="rn-general-searchbox-button" defaultValue="Submit" id="search-filter" /></td>
            </tr>
          </tbody>
        </table>
      </div>
      <div className="rn-general-content-radiobuttons-group">
        <div className="rn-general-content-radiobutton"> <input type="radio" name="search-radiobuttons" id="search-radiobutton-date" defaultValue="Date" />
          <p>Date</p>
        </div>
        <div className="rn-general-content-radiobutton"> <input type="radio" name="search-radiobuttons" id="search-radiobutton-renarrator" defaultValue="Renarrator" />
          <p>Renarrator</p>
        </div>
      </div>
      <div className="rn-general-content-radiobuttons-group">
        <div className="rn-general-content-radiobutton"> <input type="radio" name="search-radiobuttons" id="search-radiobutton-document" defaultValue="Document" />
          <p>Document</p>
        </div>
      </div>
    </div>
  )
}

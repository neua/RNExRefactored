
//
// export const Icons ={
//   uparrow
// }
//
// export cos
//

const getURL = (val: string) => {
  return chrome.runtime.getURL(val);
}
//---horizontal menu icons---
export const menu_toggle_icon = getURL("img/OwnedIcons/menuicon.svg");
export const recommending_mode_icon = getURL("img/OwnedIcons/search.svg");
export const notification_mode_icon = getURL("img/OwnedIcons/notification.svg");
export const account_mode_icon = getURL("img/OwnedIcons/myprofile.svg");
export const reading_mode_icon = getURL("img/OwnedIcons/readingbook.svg");
export const writing_mode_icon = getURL("img/OwnedIcons/pencil.svg");

//---vertical menu icons---
export const information_box_icon = getURL("img/OwnedIcons/information.svg");
export const response_mode_icon = getURL("img/OwnedIcons/response.svg");
export const profile_mode_icon = getURL("img/OwnedIcons/otherprofile.svg");

//----up and down arow and triangle icons----
export const uparrow_icon = getURL("img/OwnedIcons/uparrow.svg");
export const  downarrow_icon = getURL("img/OwnedIcons/downarrow.svg");

export const  triangle_icon = getURL("img/OwnedIcons/triangle.svg");

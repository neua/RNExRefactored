import React, { Component } from "react";
import { RenarrationTable } from "./common_components/renarration_table"
import { Profile } from "./common_components/profile_part"
import { CurrentMode, GeneralHeader } from "./common_components/general_header"

export class AccountMode extends Component {
  render(): JSX.Element {
    return (
      <React.Fragment>
        <div className="rn-main-mode-account">

          <CurrentMode currentMode={"My Profile"} />
          <Profile
            //renarrator_picture="profile_jpg"
            renarrator_name="Joe"
            renarrator_webId="https://bob-therenarrator.inrupt.net"
            //  renarrator_role="Student"
            renarrator_mail="Boe@gmail.com"
          />

          <GeneralHeader header={"Renarrations"} />
          <RenarrationTable renarrations={[1, 2, 3, 4, 5]} />
          <div className="rn-general-content-box">
            <input type="button" className="rn-general-searchbox-button-second-type" id="logout" defaultValue="Logout" />
          </div>
        </div>
      </React.Fragment>
    )
  }
}

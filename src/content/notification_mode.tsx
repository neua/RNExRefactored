import React, { useState } from "react";
import { CurrentMode } from "./common_components/general_header"
import { uparrow_icon, downarrow_icon } from './icons';

const NotificationContentRow = ({ label, value, link = null }: { label: string, value: string, link: boolean }) => {
  return (
    <div className="rn-general-notification-content-box">
      <div className="rn-general-content-label-value-group rn-notification-content-label-value-group">
        <div className="rn-general-content-label-name rn-notification-label-width">
          <p>{label}</p>
        </div>
        {link ?
          <div className="rn-general-content-value-name rn-notification-name-color">
            <a href={value} target="_blank" className="general-clickable-url">
              <p className="slide-to-right-hover">{value}</p>
            </a>
          </div>
          :
          <div className="rn-general-content-value-name rn-notification-name-color">
            <p className="text-with-overflow">{value}</p>
          </div>
        }
      </div>
    </div>)
}

const NotificationBox = ({ }: any) => {

  const [hide, setHide] = useState(false);

  return (
    <div className={`rn-general-sliding-content-area-box ${hide ? 'rn-general-sliding-content-area-display-none' : ''}`}>
      <div className="rn-general-sliding-content-area-box-header">
        <p className="rn-general-sliding-content-area-box-header-overflow  rn-notification-overflow-text">Renarration-2021-02-11T05:47:06.389Z</p>

        {hide ? <input type="image" src={uparrow_icon} className="" onClick={() => setHide(prevHide => !prevHide)} /> :
          <input type="image" src={downarrow_icon} className="" onClick={() => setHide(prevHide => !prevHide)} />}

      </div>
      <div className={`rn-general-sliding-content-area-box-body ${hide ? 'rn-general-sliding-content-area-display-slide-to-none' : ''}`}>
        <NotificationContentRow label="onSourceDocument" value="https://neua.gitlab.io/renarration-test-cases/fibula_example" link={true} />
        <NotificationContentRow label="onTargetDocument" value="https://jane-therenarrator.inrupt.net/renarration/RenarrationFiles/RNCNT-kl0fwlqt-jmla7w11-5qlijtu4.ttl" link={true} />
        <NotificationContentRow label="renarratedAt" value="2021-02-11T05:47:06.389Z" link={false} />
        <NotificationContentRow label="renarratedBy" value="https://jane-therenarrator.inrupt.net/profile/card#me" link={true} />
      </div>
    </div>

  )
}


export class NotificationMode extends React.Component {
  render() {
    return (
      <div className="rn-main-mode-notification">

        <CurrentMode currentMode="Notification Mode" />
        <div className="rn-general-response-container">
          <div className="rn-general-response-container-header">
            <h4>Notifications</h4>
          </div>
          <div className="rn-general-response-container-body rn-notification-container-body">
            <NotificationBox />
            <NotificationBox />
            <NotificationBox />
          </div>
        </div>
      </div>
    )
  }
}

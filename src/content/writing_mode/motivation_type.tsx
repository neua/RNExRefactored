import * as React from "react";
import { useState, useEffect } from "react";


export const MotivationType = ({ onMode, name, id, value }: { onMode: any, name: string, id: string, value: string }) => {
  const [checked, setCheck] = useState({ checked: false });

  const setRadioCheck = (e: any, oldVal: any): any => {
    setCheck({ checked: !oldVal });
    e.currentTarget.checked = !oldVal;
  }
  useEffect(() => {
    onMode({ checked, name, id, value });
  }, [checked])

  return (
    <div className="rn-general-content-radiobutton">
      <input type="radio" name={name} id={id} value={value} onClick={(e) => setRadioCheck(e, checked.checked)} />
      <p>{value} </p>
    </div>
  )
}

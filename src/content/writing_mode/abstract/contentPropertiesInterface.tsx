export interface IContentProperties {
  size?: string,
  color?: string,
  bold?: boolean,
  italic?: boolean,
  underline?: boolean,
  width?: string,
  height?: string
}

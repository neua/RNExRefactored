
import { ActionTypeEnum } from "./ActionTypeEnum";
import { RichTextAreaModeEnum } from "./RichTextAreaModeEnum";

import { IContentProperties } from "./contentPropertiesInterface";

import { IMotivationType } from "./motivationTypeInterface";

import { selectedHTMLPropertiesInterface } from "../../common_components/abstracts";

export interface IWritingModeState {
  action: ActionTypeEnum,
  text: string,
  contentProperties: IContentProperties,
  language: string,
  mode: RichTextAreaModeEnum,
  motivation: IMotivationType,
  selectedHTMLProperties: selectedHTMLPropertiesInterface
}

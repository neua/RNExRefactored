export const enum ActionTypeEnum {
  remove="remove",
  replace="replace",
  insertAfter="insertAfter",
  insertBefore="insertBefore",
}

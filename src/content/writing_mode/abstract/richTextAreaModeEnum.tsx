export const enum RichTextAreaModeEnum {
  html="html",
  text="text",
  image="image",
  audio="audio",
  video="video",
  none="none"
}

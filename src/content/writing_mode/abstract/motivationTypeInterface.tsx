export interface IMotivationType {
  alteration?: string,
  elaboration?: string,
  simplification?: string,
  correction?: string,
  translation?: string
}

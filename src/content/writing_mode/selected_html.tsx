import * as React from "react";
import { useState } from "react";
import { uparrow_icon, downarrow_icon } from '../icons';


export const SelectedHtml = ({ onSelectedHTML }: { onSelectedHTML: any }) => {
  const [hide, setHide] = useState(false);

  return (
    <div className={`rn-general-sliding-content-area-box ${hide ? 'rn-general-sliding-content-area-display-none' : ''}`}>
      <div className="rn-general-sliding-content-area-box-header">
        <input type="image" src="chrome-extension://nkdhfjnoihpegfhdnkghpmblbniejmpm/img/OwnedIcons/copy.svg" className="copy-the-content-icon" id="rn-copy-content-icon" rn-class-visibility="rn-visibility-hidden" />

        <p className="rn-general-sliding-content-area-box-header-overflow">Selected HTML Part</p>

        {hide ? <input type="image" src={uparrow_icon} className="" onClick={() => setHide(prevHide => !prevHide)} /> :
          <input type="image" src={downarrow_icon} className="" onClick={() => setHide(prevHide => !prevHide)} />}
      </div>
      <div className={`rn-general-sliding-content-area-box-body ${hide ? 'rn-general-sliding-content-area-display-slide-to-none' : ''}`}>
        <p className="rn-general-sliding-content-area-text-place"> {onSelectedHTML ? onSelectedHTML : ""}</p>
      </div>
    </div>
  )
}

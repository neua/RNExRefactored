import React, { useState,useEffect } from "react";
import { MotivationType } from "./motivation_type";

export const SaveRenarration = ({ onMotivationChange,onSaveRenarration }: { onMotivationChange:any, onSaveRenarration: any }) => {
  const [motivation, setMotivation] = useState({
      alteration: false,
      elaboration: false,
      simplification: false,
      correction: false,
      translation: false

  });

  const handleMode = (obj: any): void => {

    let motivationKeyValue: any = {};
    let motivationKey = obj["value"].toLowerCase();
    let motivationValue = obj["checked"].checked;
    motivationKeyValue[motivationKey] = motivationValue;

    setMotivation({ ...motivation, ...motivationKeyValue });

  }

  const saveRenarration = (value: any): void => {
    onSaveRenarration(motivation);
  }

  useEffect(() => {
    onMotivationChange(motivation);
  }, [motivation])

  return (
    <div className="rn-general-content-box">
      <div className="rn-general-header">
        <h4>RENARRATION</h4>
      </div>
      <div className="rn-general-content-header">
        <p>Motivation</p>
      </div>
      <div className="rn-general-content-radiobuttons-group">
        <MotivationType onMode={handleMode} name="motivation-radiobuttons-1" id="motivation-radiobutton-simplification" value="Simplification" />
        <MotivationType onMode={handleMode} name="motivation-radiobuttons-2" id="motivation-radiobutton-alteration" value="Alteration" />
      </div>
      <div className="rn-general-content-radiobuttons-group">
        <MotivationType onMode={handleMode} name="motivation-radiobuttons-3" id="motivation-radiobutton-correction" value="Correction" />
        <MotivationType onMode={handleMode} name="motivation-radiobuttons-4" id="motivation-radiobutton-elaboration" value="Elaboration" />
      </div>
      <div className="rn-general-content-radiobuttons-group">
        <MotivationType onMode={handleMode} name="motivation-radiobuttons-5" id="motivation-radiobutton-translation" value="Translation" />
      </div>
      <input type="button" className="rn-general-searchbox-button" id="save_the_rn" value="Save Renarration" onClick={saveRenarration} />
    </div>
  )
}

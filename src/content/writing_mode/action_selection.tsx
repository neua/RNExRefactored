import * as React from "react";
import { useState, useEffect } from "react";
import { ActionTypeEnum } from "./abstract/ActionTypeEnum";

export const ActionSelection = ({ onActionChange }: { onActionChange: any }) => {

  const [checkedItems, setCheckedItems] = useState({ actionEnumType: ActionTypeEnum.replace });

  const handleChange = (actionType: ActionTypeEnum) => {
    setCheckedItems({ actionEnumType: actionType });
  }

  useEffect(() => {
    onActionChange(checkedItems.actionEnumType);
  }, [checkedItems])

  return (
    <>
      <div className="rn-general-content-header">
        <p>Action</p>
      </div>
      <div className="rn-general-content-radiobuttons-group">
        <div className="rn-general-content-radiobutton">
          <input type="radio" name="action-radiobuttons" id="remove" value="Remove" checked={checkedItems.actionEnumType === ActionTypeEnum.remove} onChange={() => { handleChange(ActionTypeEnum.remove) }} />
          <p>Remove</p>
        </div>
        <div className="rn-general-content-radiobutton">
          <input type="radio" name="action-radiobuttons" id="replace" value="Replace" checked={checkedItems.actionEnumType === ActionTypeEnum.replace} onChange={() => { handleChange(ActionTypeEnum.replace) }} />
          <p>Replace</p>
        </div>
      </div>
      <div className="rn-general-content-radiobuttons-group">
        <div className="rn-general-content-radiobutton">
          <input type="radio" name="action-radiobuttons" id="insertbefore" value="Insert-Before" onChange={() => { handleChange(ActionTypeEnum.insertBefore) }} />
          <p>Insert Before</p>
        </div>
        <div className="rn-general-content-radiobutton">
          <input type="radio" name="action-radiobuttons" id="insertafter" value="Insert-After" onChange={() => { handleChange(ActionTypeEnum.insertAfter) }} />
          <p>Insert After</p>
        </div>
      </div>


    </>
  )
}

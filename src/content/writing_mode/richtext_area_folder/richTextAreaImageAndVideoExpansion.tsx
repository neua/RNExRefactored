import React, { useState, useContext } from "react";
import WritingModeContext from "../context/writingModeContext";

export const RichTextAreaImageAndVideoExpansion = () => {


  const writingContext = useContext(WritingModeContext);

  const [contentProperties, setContentProperties] = useState(
    {
      width: null,
      height: null
    }
  );

  const setSizeValue = (e: React.FormEvent<HTMLInputElement>, mode: string): void => {
    let value = e.currentTarget.value;
    setContentProperties({ ...contentProperties, [mode]: value });
    writingContext.onContentProperties({ ...contentProperties, [mode]: value });
  }

  return (
    <div className="rich-textarea-header-expansion">
      <p className="rich-textarea-label" >Width</p>
      <input type="number" min="256" max="1024" className="rich-textarea-image-size-input" id="richtext-media-image-width-input"
        onChange={(e) => setSizeValue(e, "width")}
      />
      <p className="rich-textarea-label" >Height</p>
      <input type="number" min="256" max="1024" className="rich-textarea-image-size-input" id="richtext-media-image-height-input"
        onChange={(e) => setSizeValue(e, "height")}
      />
    </div>
  )
}

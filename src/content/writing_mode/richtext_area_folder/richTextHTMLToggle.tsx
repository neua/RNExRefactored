import React from "react";

export const RichTextHTMLToggle = ({ toggle, toggleStatus }: { toggle: any, toggleStatus: boolean }) => {

  return (
    <div className="rich-textarea-toggle-group">
      <p className="rich-textarea-toggle-text">Type HTML</p>
      <label className="switch" id="richtext-area-toggle-switch">
        <input type="checkbox" id="toggle-richtext-area" defaultChecked={toggleStatus} onClick={() => toggle(!toggleStatus)} />
        <span className="slider round"></span>
      </label>
    </div>
  )
}

import React from "react";
import {RichTextAreaModeEnum} from "../abstract/RichTextAreaModeEnum";

export const RichTextAreaMode = ({ select, idValue, value, enumIdValue }: { select: any, idValue: string, value: string, enumIdValue: RichTextAreaModeEnum }) => {

  return (
    <label className="rich-textarea-header-media-radiobutton-group">
      <input type="radio" name="media-radiobuttons" id={idValue} value={value} onClick={() => select(enumIdValue)} />
      <span className="rich-textarea-header-media-radiobutton-group-text">{value}</span>
    </label>
  )
}

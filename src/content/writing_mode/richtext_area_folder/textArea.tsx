import React, { useState, useContext, useEffect } from "react";
import WritingModeContext from "../context/writingModeContext";

export interface ITextAndStyle {
  text: string,
  style: React.CSSProperties
}

export const TextArea = ({ mode, disabled, placeholder }: { mode: any, disabled: any, placeholder: any }) => {
  const writingContext = useContext(WritingModeContext);

  const [textAndStyle, setTextAndStyle] = useState<ITextAndStyle | undefined>({
    text: "", style: {
      fontWeight: "bold",
      textDecoration: "underline",
      fontStyle: "italic",
      color: "#1fb244",
      fontSize: "75px",
    }
  });

  const setTextAreaValue = (e: any): void => {
    let content = e.target.value;
    setTextAndStyle({ ...textAndStyle, text: content })

    writingContext.onContent(content);
  }

  useEffect(() => {
    let { bold, color, italic, size, underline } = writingContext.contentProperties;
    let style: React.CSSProperties = {
      fontWeight: `${bold ? "bold" : ""}` as any,
      textDecoration: `${underline ? "underline" : ""}`,
      fontStyle: `${italic ? "italic" : ""}`,
      color: `${color ? color : ""}`,
      fontSize: `${size ? `${size}px` : ""}`,
    }
    setTextAndStyle({ ...textAndStyle, style })

  }, [writingContext.contentProperties]);
  //
  // const stylse: React.CSSProperties = {
  //   fontWeight: `bold`,
  //   textDecoration: "underline",
  //   fontStyle: "italic",
  //   color: "#1fb244",
  //   fontSize: "75px",
  // }

  return (
    <textarea id="richtext-textarea" style={textAndStyle.style} className="rich-textarea-text" placeholder={placeholder()} disabled={disabled} value={textAndStyle.text} onChange={setTextAreaValue}></textarea>
  )
}

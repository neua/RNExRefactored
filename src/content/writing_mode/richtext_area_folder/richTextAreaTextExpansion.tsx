import React, { useState, useContext } from "react";
import WritingModeContext from "../context/writingModeContext";



export const RichTextAreaTextExpansion =() => {

  const writingContext = useContext(WritingModeContext);

  const [contentProperties, setContentProperties] = useState(
    {
      size: null,
      color: null,
      bold: false,
      italic: false,
      underline: false
    }
  );

  const setSizeValue = (e: React.FormEvent<HTMLInputElement>): void => {
    let size = e.currentTarget.value;
    setContentProperties({ ...contentProperties, size });
    writingContext.onContentProperties({ ...contentProperties, size });
  }

  const setColorValue = (e: React.FormEvent<HTMLInputElement>): void => {
    let color = e.currentTarget.value;
    setContentProperties({ ...contentProperties, color });
    writingContext.onContentProperties({ ...contentProperties, color });
  }

  const setBoldValue = (e: React.FormEvent<HTMLInputElement>): void => {
    let bold = e.currentTarget.checked;
    setContentProperties({ ...contentProperties, bold });
    writingContext.onContentProperties({ ...contentProperties, bold });
  }

  const setItalicValue = (e: React.FormEvent<HTMLInputElement>): void => {
    let italic = e.currentTarget.checked;
    setContentProperties({ ...contentProperties, italic });
    writingContext.onContentProperties({ ...contentProperties, italic });
  }

  const setUnderlineValue = (e: React.FormEvent<HTMLInputElement>): void => {
    let underline = e.currentTarget.checked;
    setContentProperties({ ...contentProperties, underline });
    writingContext.onContentProperties({ ...contentProperties, underline });
  }

  return (
    <div className="rich-textarea-header-expansion">
      <p className="rich-textarea-label" >Size</p>
      <input type="number" min="8" max="96" className="rich-textarea-font-size-input" id="richtext-font-size-input" onChange={setSizeValue} />
      <p className="rich-textarea-label" >Color</p>
      <input type="color" className="rich-textarea-color-input" id="richtext-color-input" onChange={setColorValue} />
      <label className="rn-checkbox-container">
        <input type="checkbox" id="richtext-font-prop-bold" onChange={setBoldValue} />
        <span className="rn-checkbox-checkmark" style={{ fontWeight: "bold" }} >B</span>
      </label>
      <label className="rn-checkbox-container">
        <input type="checkbox" id="richtext-font-prop-italic" onChange={setItalicValue}  />
        <span className="rn-checkbox-checkmark" style={{ fontStyle: "italic" }}>I</span>
      </label>
      <label className="rn-checkbox-container">
        <input type="checkbox" id="richtext-font-prop-underline" onChange={setUnderlineValue} />
        <span className="rn-checkbox-checkmark" style={{ textDecorationLine: "underline" }}>U</span>
      </label>
    </div>
  )
}

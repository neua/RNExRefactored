import React from "react";
import {RichTextAreaModeEnum} from "../abstract/richTextAreaModeEnum";
import {IContentProperties} from "../abstract/contentPropertiesInterface";

interface IWritingMode {
  content: string,
  contentType:RichTextAreaModeEnum,
  onContent: (text: string) => void,
  onContentProperties: (contentProperties: IContentProperties)=> void,
  contentProperties:IContentProperties
}

const WritingModeContext = React.createContext<IWritingMode>(null);
WritingModeContext.displayName = "WritingModeContext";

export default WritingModeContext;

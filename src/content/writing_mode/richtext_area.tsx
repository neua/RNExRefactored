import * as React from "react";
import { useState } from "react";
import { RichTextAreaImageAndVideoExpansion } from "./richtext_area_folder/richTextAreaImageAndVideoExpansion";
import { RichTextAreaTextExpansion } from "./richtext_area_folder/richTextAreaTextExpansion";
import { TextArea } from "./richtext_area_folder/textArea";
import { RichTextAreaMode } from "./richtext_area_folder/richTextAreaMode";
import { RichTextHTMLToggle } from "./richtext_area_folder/richTextHTMLToggle";
import { RichTextAreaModeEnum } from "./abstract/RichTextAreaModeEnum";







export const RichTextArea = ({ onModeChange }: any) => {
  const [mode, setMode] = useState({ mode: RichTextAreaModeEnum.none });
  const [toogle, setToggle] = useState({ toggle: false });

  const handleSetMode = (value:any) => {
    setMode({ mode: value });
    onModeChange({ mode: value });
  }

  const handleSelect = (enumIdValue: RichTextAreaModeEnum) => {
    handleSetMode(enumIdValue);
  }
  const handleToggle = (isToggled: any) => {
    setToggle({ toggle: isToggled });
    if (isToggled) {
      handleSetMode(RichTextAreaModeEnum.html);
    } else {
      handleSetMode(RichTextAreaModeEnum.none);
    }
  }

  const setPlaceHolderValue = (): string => {
    if (toogle.toggle) {
      return "Type HTML"
    }
    else if (mode.mode !== RichTextAreaModeEnum.none) {
      switch (mode.mode) {
        case RichTextAreaModeEnum.text:
          return "Type some text"
        case RichTextAreaModeEnum.image:
          return "Type a url of an image"
        case RichTextAreaModeEnum.audio:
          return "Type a url of an audio"
        case RichTextAreaModeEnum.video:
          return "Type a url of a video"
      }
    }
    return "Choose a media type to create a renarration transformation"
  }


  return (
    <React.Fragment>
      <RichTextHTMLToggle toggle={handleToggle} toggleStatus={toogle.toggle} />
      <div className="richtext-and-advancedtext-area-toggle">
        <div className="rich-textarea-box">
          <div className="rich-textarea-header">
            {
              toogle.toggle ? <p className="rich-textarea-label" style={{ margin: "0 calc(50% - 22px)" }}>HTML</p> :
                <>
                  <p className="rich-textarea-label" >Type</p>
                  <RichTextAreaMode idValue={"media-radiobutton-text"} value={"Text"} select={handleSelect} enumIdValue={RichTextAreaModeEnum.text} />
                  <RichTextAreaMode idValue={"media-radiobutton-image"} value={"Image"} select={handleSelect} enumIdValue={RichTextAreaModeEnum.image} />
                  <RichTextAreaMode idValue={"media-radiobutton-audio"} value={"Audio"} select={handleSelect} enumIdValue={RichTextAreaModeEnum.audio} />
                  <RichTextAreaMode idValue={"media-radiobutton-video"} value={"Video"} select={handleSelect} enumIdValue={RichTextAreaModeEnum.video} />
                </>
            }
          </div>
          <div className="rich-textarea-header-expansion-group">
            {mode.mode === RichTextAreaModeEnum.text ? <RichTextAreaTextExpansion /> : null}
            {mode.mode === RichTextAreaModeEnum.image ? <RichTextAreaImageAndVideoExpansion /> : null}
            {mode.mode === RichTextAreaModeEnum.video ? <RichTextAreaImageAndVideoExpansion /> : null}
            {mode.mode === RichTextAreaModeEnum.audio ? null : null}
          </div>
          <TextArea disabled={mode.mode !== RichTextAreaModeEnum.none ? false : true} placeholder={setPlaceHolderValue} mode={mode.mode} />
        </div>
      </div>
    </React.Fragment>
  )
}

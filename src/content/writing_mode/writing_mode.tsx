import * as React from "react";
import { CurrentMode } from "../common_components/general_header"
import { ModificationOfDom, EventEmitter } from "../renarration_engine/renarration_creation";
import { ActionSelection } from "./action_selection";
import { SelectedHtml } from "./selected_html";
import { RichTextArea } from "./richtext_area";
import { LanguageSelection } from "./language_selection";
import { SaveRenarration } from "./save_renarration";
import WritingModeContext from "./context/writingModeContext";
import { selectedHTMLPropertiesInterface } from "../common_components/abstracts";

import { ActionTypeEnum } from "./abstract/ActionTypeEnum";
import { RichTextAreaModeEnum } from "./abstract/RichTextAreaModeEnum";

import { IContentProperties } from "./abstract/contentPropertiesInterface";

import { IMotivationType } from "./abstract/motivationTypeInterface";
import {IWritingModeState} from "./abstract/writingModeStateInterface";

interface IProps {
}



export class WritingMode extends React.Component<IProps, IWritingModeState> {
  engine: any;
  constructor(props: IProps) {
    super(props);
    this.engine = new ModificationOfDom(document);
    this.engine.main();
    //let selectedHtmlPart: string = "engine.selectedHtml";
    this.state = {
      action: ActionTypeEnum.replace,
      text: null,
      contentProperties: {
        size: "",
        color: "",
        bold: false,
        underline: false,
        italic: false,
        width: null,
        height: null
      },
      language: null,
      mode: null,
      motivation: null,
      selectedHTMLProperties: {
        value: null,
        xpath: null,
        xpathOfNextElementSibling: null,
        xpathOfPreviousElementSibling: null
      }
    }
  }

  componentDidMount() {
    EventEmitter.subscribe("click_operation", (selectedHTMLProperties: any) => {
      this.handleSelectedHtml(selectedHTMLProperties);
    });
  }

  componentWillUnmount() {
    EventEmitter.unsubscribe("click_operation");
    this.engine.unsubscribeEvents();
  }
  handleContent = (text: string) => {
    this.setState({ ...this.state, text });
  };

  handleContentProperties = (contentProperties: IContentProperties) => {
    this.setState({ ...this.state, contentProperties });
  };

  handleLanguage = (language: any) => {
    this.setState({ ...this.state, language });
  };



  handleModeChange = (mode: any) => {
    let contentProperties: IContentProperties = {
      size: "",
      color: "",
      bold: false,
      underline: false,
      italic: false,
      width: null,
      height: null
    };
    this.setState({ ...this.state, mode: mode.mode, contentProperties });
  };
  handleActionChange = (action: any) => {
    this.setState({ ...this.state, action });
  };

  handleMotivationChange = (motivation: any) => {
    this.setState({ ...this.state, motivation });
  };

  handleSaveRenarration = (motivation: any) => {
    console.log("handleSaveRenarration", motivation);

    this.engine.createRenarration(this.state);
  };

  handleSaveRenarrationTransformation = () => {
    let state = this.state;
    console.log("handleSaveRenarrationTransformation", state);
    if (state.selectedHTMLProperties.value) {
      this.engine.createRenarrationTransformation(state);

      let selectedHTMLProperties: selectedHTMLPropertiesInterface = {
        value: null,
        xpath: null,
        xpathOfNextElementSibling: null,
        xpathOfPreviousElementSibling: null
      }
      this.setState({
        ...this.state, selectedHTMLProperties
      });

    } else {
      console.log("Warning you should choose some text");
    }

  };

  handleSelectedHtml = (valSelectedHTMLProperties: any) => {

    let selectedHTMLProperties = valSelectedHTMLProperties;

    this.setState({ ...this.state, selectedHTMLProperties });
  };

  render() {
    let state: any = { ...this.state };

    return (
      <WritingModeContext.Provider
        value={{
          content: null,
          onContent: this.handleContent,
          contentType: null,
          onContentProperties: this.handleContentProperties,
          contentProperties: state.contentProperties
        }}
      >
        <div className="rn-main-mode-writing">
          <CurrentMode currentMode={"Create Renarration"} />
          <div className="rn-general-content-box">
            <div className="rn-general-header">
              <h4>Renarration Transformation</h4>
            </div>
            <ActionSelection onActionChange={this.handleActionChange} />
            <SelectedHtml onSelectedHTML={state.selectedHTMLProperties.value} />

            <RichTextArea onModeChange={this.handleModeChange} />
            <LanguageSelection onLanguage={this.handleLanguage} />
            <input type="button" className="rn-general-searchbox-longer-button" id="save_the_rt" value="Save Renarration Transformation" disabled={false} onClick={this.handleSaveRenarrationTransformation} />
          </div>
          <SaveRenarration onMotivationChange={this.handleMotivationChange} onSaveRenarration={this.handleSaveRenarration} />
        </div>
      </WritingModeContext.Provider>
    )
  }
}

import React, { useState } from "react";

// interface ILanguage {
//   language: any
// }

export const LanguageSelection = ({ onLanguage }: { onLanguage: any }) => {

  const [language, setLanguage] = useState({ language: null });

  const setLanguageValue = (value: any): void => {
    onLanguage(value);
    setLanguage({ language:value });
  }

  return (
    <div className="rn-general-content-label-value-group" id="rn_language_group">
      <div className="rn-general-searchbox-header">
        <p>Language</p>
      </div>
      <input type="text" className="rn-general-searchbox-textbox" id="rn_transformation_language" placeholder="Type here..." onChange={(e) => setLanguageValue(e.target.value)} />
    </div>
  )
}

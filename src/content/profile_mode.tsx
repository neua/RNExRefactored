import * as React from "react";
// import "./rn-design.css";
import { RenarrationTable } from "./common_components/renarration_table"
import { Profile } from "./common_components/profile_part"
import { CurrentMode, GeneralHeader } from "./common_components/general_header"


export class ProfileMode extends React.Component {
    render() {
        return (
          <div className="rn-main-mode-profile">
            <CurrentMode currentMode="Visited Profile"/>

          <div className="rn-follow-button-box"><input type="button" className="rn-follow-button" id="follow-the-profile" defaultValue="Follow" /></div>
          <Profile
            //renarrator_picture="profile_jpg"
            renarrator_name="Gwen"
            renarrator_webId="https://gwen-therenarrator.inrupt.net"
           renarrator_role="student"
            renarrator_mail="Boe@gmail.com"
          />

          <GeneralHeader header="Renarrations"/>
          <RenarrationTable renarrations={[1,3,4]}/>
        </div>
    )
  }
}

import {
  uparrow_icon,
  downarrow_icon,
  triangle_icon
} from './icons';

import styles from '../../rndesignprimary.css'

const css = styles.toString();
export const RnCss = css;

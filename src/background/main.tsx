import { login, onSession, getSession } from "solid-auth-fetcher"

getSession().then(async (session) => {
  if (!session) {
    const session = await login({
      // You could provide either a "webId" field, or an "oidcIssuer" field if
      // you know who the user's OIDC issuer is. If neither are provided, a
      // pop-up will ask the user to provide one. If it is running on the server
      // the popup parameter will be ingored.
      //    oidcIssuer: 'https://solid.inrupt.net',
      webId: 'https://bob-therenarrator.inrupt.net/profile/card#me',
      // Note that when 'popUp' is false, the browser will redirect the
      // current page thus stopping the current flow (default is false).
      popUp: false,

      // The page that that should be queried once login is complete
      redirect: 'https://google.com',
    })
    // Chances are that this session will not be logged in. Instead, your browser
    // window will be redirected and you'll be able to get the logged in session
    // via `getSession` or `onSession` after the redirect.
    console.log(session);
  }
  // if (!session.loggedIn) {
  //   // const session = await login({
  //   //   oidcIssuer: "https://inrupt.net",
  //   //   popUp: false,
  //   //   redirect: 'https://google.com'
  //   // });
  //   console.log("User is not logged in");
  //   console.log(session);
  // } else {
  //   console.log("User is logged in");
  // }
})


onSession((session) => {
  if (session.loggedIn) {
    console.log(session);
    session.fetch('https://example.com/resource');
  }
})
